import csv
import numpy as np

def normalize(grades):
    for key in grades.keys():
        if isinstance(grades[key][0], float):
            grades[key] = grades[key] / max(grades[key])

def droplowhmwk(grades):
    numstu = len(grades['Student ID'])
    karr = np.array([key for key in grades.keys()])
    rarr = np.array(['Homework' in s for s in karr])
    hkeys = karr[rarr]
    numhw = len(hkeys)
    havg = np.zeros(numstu)
    skp = np.zeros(numstu, dtype=bool)
    for stu in range(numstu):
        hgr = [grades[hk][stu] for hk in hkeys]
        havg[stu] = (sum(hgr) - min(hgr)) / (numhw - 1)
        skp[stu] = (min(hgr) <= 0.25)
    grades['Hmwk Avg'] = havg
    grades['Skipped'] = skp

def applincomb(grades, weights):
    if 'Hmwk Avg' not in grades.keys():
        droplowhmwk(grades)
    numstu = len(grades['Student ID'])
    avg = np.zeros(numstu)
    for stu in range(numstu):
        avg[stu] = sum(grades[key][stu] * weights[key] for key in weights.keys())
    return avg

def getgrades(grades):
    normalize(grades)
    droplowhmwk(grades)
    weights = [{'Final':0.4, 'Hmwk Avg':0.2, 'Midterm 1':0.2, 'Midterm 2 After correction':0.2}]
    weights += [{'Final':0.6, 'Hmwk Avg':0.2, 'Midterm 1':0.0, 'Midterm 2 After correction':0.2}]
    weights += [{'Final':0.6, 'Hmwk Avg':0.2, 'Midterm 1':0.2, 'Midterm 2 After correction':0.0}]
    labs = np.array(['Avg {}'.format(i) for i in range(len(weights))])
    for i in range(len(weights)):
        grades[labs[i]] = applincomb(grades, weights[i])
    numstu = len(grades['Student ID'])
    maxav = np.zeros(numstu)
    for stu in range(numstu):
        maxav[stu] = max(grades[k][stu] for k in labs)
    grades['Max Avg'] = maxav

def makecsv(grades, filnam):
    fields = ['Last Name', 'First Name', 'Student ID', 'Hmwk Avg', 'Skipped',
              'Midterm 1', 'Midterm 2 After correction', 'Final', 'Avg 0', 
              'Avg 1', 'Avg 2', 'Max Avg']
    numstu = len(grades['Student ID'])
    
    with open(filnam, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fields)
        writer.writeheader()
        for stu in range(numstu):
            wd = {k:str(grades[k][stu]) for k in fields}
            writer.writerow(wd)
            
            
        
