import csv
import pickle
import numpy as np

with open('grades.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    grades = []
    for row in reader:
        grades += [row]

gDict = dict()

for key in grades[0].keys():
    num = 'Homework' in key or 'Midterm' in key or 'Final' in key
    if not num:
        gDict[key] = []
        for k in range(len(grades)):
            gDict[key] += [grades[k][key]]
        gDict[key] = np.array(gDict[key])
    else:
        gDict[key] = np.array([])
        for k in range(len(grades)):
            if grades[k][key]:
                gDict[key] = np.hstack([gDict[key], float(grades[k][key])])
            else:
                gDict[key] = np.hstack([gDict[key], 0])

with open('gradesall.dat', 'wb') as file:
    pickle.dump(gDict, file)

ind = np.where(gDict['Status'] == 'Enrolled')
for key in gDict.keys():
    if key != 'Status':
        gDict[key] = gDict[key][ind]
gDict.pop('Status')

with open('gradesdropped.dat', 'wb') as file:
    pickle.dump(gDict, file)
