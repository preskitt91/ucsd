%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathbf{F}}}
\renewcommand{\S}{\ensuremath{\mathbf{S}}}
\newcommand{\s}{\ensuremath{\mathbf{s}}}



%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{}
\author{}
\date{}

\begin{document}
\newlength{\probskip}
\setlength{\probskip}{0.6cm}
\addtolength{\baselineskip}{3pt}
% \maketitle

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

\textbf{\large University of California, San Diego \\ Math 20E, Summer Session II, 2016 \\ Instructor:  Brian Preskitt \\ Practice Final \\ September 3, 2016 \\ $\cdots$ \\ Version P for Practice}

\textbf{Instructions}
\begin{enumerate}
  \addtolength{\baselineskip}{-3pt}
  \bfseries
  \item No calculators are allowed on this exam, nor any other electronic device.
    \item You may use one hand-written $3'' \times 5''$ index card of notes, but no other notes or books are permitted.
      \item Clearly indicate all question numbers (and letters).
      \item Please start each problem (not problem section) on a new side of a page.
      \item Show all necessary work -- unsupported answers will earn no credit.
\end{enumerate}

\textbf{Problem 1}
Let $W$ be the cube $[-1, 1] \times [-1, 1] \times [-1, 1]$ and let $\F$ be the vector field \[\F(x, y, z) = (x + y, x^2 + y^2, x^{2016} + y^{2016}).\]  Using Gauss's theorem or otherwise, find $\iint_{\partial W} \F \cdot d\S$, the flux of $\F$ through $\partial W$.
\vspace{\probskip}

\textbf{Solution}  Well, the divergence theorem tells us that \[\iint_{\partial W} \F \cdot d\S = \iiint_W (\nabla \cdot \F) \, dV,\] so we calculate $\nabla \cdot \F = 1 + 2y$.  Since $W$ is symmetric about the $xz$-plane, $\iiint_W 2y \, dV = 0$, so we just have $\iiint_W 1 \, dV = \mathrm{Vol}(W) = 2^3 = 8$.

\textbf{Problem 2}
Change the order of integration to evaluate \[\int_{1/2}^1 \int_{1/y}^2 \sin (\ln x - x) \, dx \, dy.\]
\vspace{\probskip}

\textbf{Solution} We consider the region $D$ where $1/2 \le y \le 1, 1/y \le x \le 2$; with a quick sketch-n-stare, we turn this into $1 \le x \le 2, 1/x \le y \le 1$, giving \[\int_1^2 \int_{1/x}^1 \sin(\ln x - x) \, dy \, dx = \int_1^2 (1 - 1/x) \sin(\ln x - x) \, dx = \cos(\ln 2 - 2) - \cos 1.\]

\textbf{Problem 3}
Take $S$ to be the surface $x^2 + y^2 = (1 - z)^2, 0 \le z \le 1$ (just the cone, without the bottom).  Find \[\iint_S (\nabla \times \F) \cdot d\S,\] where $F = (0, x\ee^z, 0)$.
\vspace{\probskip}

\textbf{Solution}  We use Stokes's Theorem to observe that \[\iint_S (\nabla \times \F) \cdot d\S = \iint_D (\nabla \times \F) \cdot d\S,\] where $D$ is the circle $x^2 + y^2 = 1, z = 0$ (we make this switch by observing that $D$ and $S$ share the same boundary).  Now we remark $\nabla \times \F = (-x \ee^z, 0, \ee^z);$ since $D$ has a constant unit normal vector of $\hat{n} = (0, 0, 1)$, we finish by finding \[\iint_D (\nabla \times \F) \cdot d\S = \iint_D \begin{bmatrix} x \ee^0 \\ 0 \\ \ee^0 \end{bmatrix} \cdot \begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix} \, dS = \iint_D 1 \, dS = \mathrm{Area}(D) = \pi.\]

\textbf{Problem 4}
The torus of major radius $4$ and minor radius $2$ is a \emph{closed surface} parametrized by \[\Phi(\phi, \theta) = \begin{bmatrix} \cos \theta (4 + 2 \sin \phi) \\ 4 \sin \theta (4 + 2 \sin \phi) \\ 2 \cos \phi \end{bmatrix}.\]  Let $S$ be this surface and let $\F$ be the vector field \[\F = \begin{bmatrix} \ee^{xyz} \\ \sin^2(xyz) \\ (x^2 y^2 z^2 + 1)^{-1} \end{bmatrix}.\] Using Gauss's Theorem, Stokes's Theorem, or otherwise, calculate \[\iint_S (\nabla \times \F) \cdot d\S.\]
\vspace{\probskip}

\textbf{Solution}  Welp, $S$ is a closed surface; if we let $W$ denote the solid enclosed by $S$, then Gauss's Theorem tells us that \[\iint_S (\nabla \times \F) \cdot d\S = \iiint_W \nabla \cdot (\nabla \times \F) \, dV = \iiint_W 0 \, dV = 0,\] where we have also used the identity $\nabla \cdot (\nabla \times \F) = 0$, which holds for any vector field $\F$.

\textbf{Problem 5}
Let $\F = (\frac{1}{z^2 + 1}, 0, \frac{-2xz}{(z^2 + 1)^2})$.
\begin{enumerate}[a)]
  \item Decide if $\F$ is a conservative vector field.  If so, find a potential function $f$ for $\F$.
    \item Calculate $\int_{\c} \F \cdot d\s$, where $\c(t) = (t, 2^t, 4^t), t \in [0, 1]$.
\end{enumerate}
\vspace{\probskip}

\textbf{Solution}
\begin{enumerate}[a)]
  \item Indeed, $\F = \nabla\left( \dfrac{x}{z^2 + 1} \right)$, so $\F$ is conservative and $f = \dfrac{x}{z^2 + 1}$ is a potential function for it (I discovered $\F$ is conservative by just guess-and-checking for a potential function; you could just as well calculate $\nabla \times \F = 0$).
    \item Well, hooray, $\F$ is a gradient field, so \[\begin{array}{rcl}
      \int_{\c} \F \cdot d\s & = & f(\c(1)) - f(\c(0)) \\
      & = & f(1, 2, 4) - f(0, 1, 1) \\
      & = & \dfrac{1}{4^2 + 1} - \dfrac{0}{1^2 + 1} \\
      & = & \dfrac{1}{17}.\end{array}\]
\end{enumerate}

\textbf{Problem 6}
Let $C$ be the curve parametrized by $\c(t) = (\cos^2 t, \sin^2 t), 0 \le t \le 2\pi$.  Using Green's Theorem or otherwise, find the area bounded by $C$.
\vspace{\probskip}

\textbf{Solution}  Well... geometrically, we may observe that $x(t) + y(t) = 1$ for all $t$, so we're taking the area of a line, which is $0$ :(

Just to see if we're crazy, we might use Green's theorem to calculate the area as \[\int_{\c} x \, dy = \int_0^{2\pi} \cos^2 t \cdot 2 \sin t \cos t \, dt = -1/2 \cos^4 t|_{t = 0}^{2 \pi} = 0.\]

\textbf{Problem 7}
Let $S$ be the ellipse $2x^2 + 4y^2 + 8z^2 = 16$ and let $\F$ be the vector field $\F = (2x + \ee^z, \sin x - y, y^3 - z)$.  Calculate \[\iint_S \F \cdot d\S,\] the flux of $\F$ through $S$, by any means necessary.
\vspace{\probskip}

\textbf{Solution}  Well, this is a closed surface, so we may quote the Divergence Theorem; we immediately observe that $\nabla \cdot \F = 0$, so $\iint_S \F \cdot d\S = 0$!

\textbf{Problem 8}
Consider the mappings $\c(t) = (\pi \cdot \cos t, 2\pi \cdot \sin t)$ and $\Phi(u, v) = (\cos u \sin v, \sin u \sin v, \cos v)$.  Compute $D(\Phi \circ \c)(\pi)$, the derivative of $\Phi \circ \c$ evaluated at $t = \pi$.
\vspace{\probskip}

\textbf{Solution}  My preference is to use the chain rule.  We find \[D\Phi = \begin{bmatrix} -\sin u \sin v & \cos u \cos v \\ \cos u \sin v & \sin u \cos v \\ 0 & -\sin v \end{bmatrix}, D\c = \begin{bmatrix} -\pi \sin t \\ 2 \pi \cos t \end{bmatrix}.\]  Well, \[D\c(\pi) = \begin{bmatrix} 0 \\ - 2 \pi \end{bmatrix},\] and \[D\Phi(\c(\pi)) = D \Phi(-\pi, 0) = \begin{bmatrix} 0 & -1 \\ 0 & 0 \\ 0 & 0 \end{bmatrix},\] so \[D(\Phi \circ \c)(\pi) = D\Phi (\c(\pi)) \cdot D \c (\pi) = \begin{bmatrix} 0 & -1 \\ 0 & 0 \\ 0 & 0 \end{bmatrix} \cdot \begin{bmatrix} 0 \\ - 2 \pi \end{bmatrix} = \begin{bmatrix} 2 \pi \\ 0 \\ 0 \end{bmatrix}.\]

\end{document}

