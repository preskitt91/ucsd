%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathbf{F}}}
\renewcommand{\S}{\ensuremath{\mathbf{S}}}
\newcommand{\s}{\ensuremath{\mathbf{s}}}



%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{}
\author{}
\date{}

\begin{document}
\newlength{\probskip}
\setlength{\probskip}{0.6cm}
\addtolength{\baselineskip}{3pt}
% \maketitle

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

\begin{multicols}{2}
\textbf{\large University of California, San Diego \\ Math 20E, Summer Session II, 2016 \\ Instructor:  Brian Preskitt \\ Final Exam \\ September 3, 2016 \\ $\cdots$ \\ Version B}

\columnbreak
\hfill\includegraphics[height=1.2in]{circumference_formula}
\end{multicols}

\textbf{Instructions}
\begin{enumerate}
  \addtolength{\baselineskip}{-3pt}
  \bfseries
  \item No calculators are allowed on this exam, nor any other electronic device.
    \item You may use two hand-written $3'' \times 5''$ index cards of notes, but no other notes or books are permitted.
      \item Clearly indicate all question numbers (and letters).
      \item Please start each problem (not problem section) on a new side of a page.
      \item Show all necessary work -- unsupported answers will earn no credit.
        \item No need to simplify fractions or other ``messy'' expressions in your final answer, just make it as presentable as is convenient.
\end{enumerate}

\textbf{Problem 1}
Let $C$ be the curve parametrized by $\c(t) = \langle \cos t, t \sin t \rangle, t \in [0, 2\pi]$.  Let $D$ be the region bounded by $C$.
\begin{enumerate}[a)]
  \item Using Green's Theorem or otherwise, set up \emph{but do not evaluate} an integral that will give the area of $D$. [\emph{10 points}]
    \item Evaluate this integral to find the area of $D$. [\emph{2 points}]
\end{enumerate}
\vspace{\probskip}

\textbf{Problem 2}
Let $S$ be the surface of the cylinder $y^2 + z^2 = 9, -4 \le x \le 4$ \emph{including its top and bottom} (so that this is a closed surface).  Let $\F = \langle z, x, y \rangle$.
\begin{enumerate}[a)]
  \item Calculate $\nabla \times \F$ and $\nabla \cdot \F$. [\emph{6 points}]
    \item Calculate $\iint_S (\nabla \times \F) \cdot d\S$, the flux of the curl of $\F$ through $S$. [\emph{6 points}]
      \item Calculate $\iint_S \F \cdot d\S$, the flux of $\F$ through $S$. [\emph{6 points}]
\end{enumerate}
\vspace{\probskip}

\textbf{Problem 3}
Let $\F$ be the vector field $\F(x, y, z) = \langle z, 2y, x \rangle$.
\begin{enumerate}[a)]
  \item Decide if $\F$ is a conservative vector field.  If it is, find a function $f$ such that $\F = \nabla f$. [\emph{6 points}]
    \item Let $\c$ be the path \[\c(t) = \begin{bmatrix} t^3 \cos(\dfrac{\pi}{t}) \\ t^3 \sin (\dfrac{\pi}{t}) \\ t \end{bmatrix}, t \in [-1, 1].\]  Compute the line integral $\int_{\c} \F \cdot d\s$. [\emph{6 points}]
\end{enumerate}
\vspace{\probskip}

\textbf{Problem 4}
Consider the path $\c(t) = (t, \sin \pi t)$ and the mapping $b(x, y) = (x + y, 2y, \ee^{-x})$.  Calculate $D(b \circ \c)|_{t = 1}$, the derivative of $b \circ \c$ at time $t = 1$. [\emph{12 points}]
\vspace{\probskip}

\textbf{Problem 5}
Let $S$ be the section of the sphere of radius $2$ with $x \ge 1$ (that is, $S$ has $x^2 + y^2 + z^2 = 4, x \ge 1$) and let $\F$ be the vector field $\F = \langle y, -z \cos^2 x, y \sin^2 x \rangle$.
\begin{enumerate}[a)]
  \item Calculate $\nabla \times \F$, the curl of $\F$. [\emph{6 points}]
    \item Calculate $\iint_S (\nabla \times \F) \cdot d\S$ (Hint: use Stokes's Theorem, but not to trade for a line integral) [\emph{12 points}]
\end{enumerate}
\vspace{\probskip}

\textbf{Problem 6}
  Let $S$ be the surface of the triangle with vertices $(1, 1, 0), (1, -1, 0)$, and $(0, 0, 1)$.  Parametrize $S$ and compute $\iint_S z \, dS$. [\emph{12 points}]
\vspace{\probskip}

\textbf{Problem 7}
Let $W$ be the region in $\R^3$ given by $x \ge 0, 0 \le z \le 1 - (x^2 + y^2)$.
\begin{enumerate}[a)]
  \item Use cylindrical coordinates to show $\iiint_W y^2 \, dV = \pi / 24$ [\emph{12 points}]
    \item Let $\F = (xy^2\sin^2 y, z^2, zy^2 \cos^2 y)$ and use your answer from part (a) to find $\iint_{\partial W} \F \cdot d\S$.  Please specify if you use any theorems from the class. [\emph{6 points}]
\end{enumerate}
\vspace{\probskip}

\textbf{Problem 8}
\begin{enumerate}[a)]
\item Use a change of variables to compute \[\iint_D \dfrac{(2x + 2y)^2}{1 + 3x + 4y} \, dA,\] where $D$ is the region where $1 \le 2x + 2y \le 2, 1 \le 3x + 4y \le 3$. [\emph{9 points}]
  \item Change the order of integration to compute $\int_1^4 \int_1^{\sqrt{y}} \sin(4x - x^3) \, dx \, dy.$ [\emph{9 points}]
\end{enumerate}
\vspace{\probskip}

\end{document}

