%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, color}
\usepackage{graphics, parskip}
\usepackage[margin=1in]{geometry}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{Gentle Intro to Parametrization}
\author{Math 20E, Summer Session II \\ Instr. Brian Preskitt}
\date{{\color{red} LAST UPDATED \today}}

\begin{document}
\maketitle

This document contains a series of examples of parametrizations of curves that will hopefully help you build a methodic understanding of how to navigate parametrization in this course.  Parametrization of paths and surfaces is an indispensable skill -- it's how we're able to mathematically describe the geometry on which we perform our calculations, and as such, it constitutes a major component of the language we're trying to learn in this class.

Unlike other parts of typical lower-division math courses, parametrization does \emph{not} admit a single step-by-step process.  Instead, parametrizing geometric objects requires us to build a steady foundation of \emph{intuitive familiarity} with simple cases that will allow us to make \emph{educated guesses} as to how we can solve new problems.  Eventually, we'll learn how to ``try things'' intelligently until we can find suitable answers for previously unseen scenarios.

\section*{How to study parametrization}

My advice is that you study the examples by asking not \emph{``how did they get the answer?''} but rather \emph{``how can I check that the answer is correct?''}  In my experience (and this applies for many types of ``creative'' problems in math), once you know how to check that an answer is correct, you're 50\% or more of the way towards learning how to solve the problem!

To that end, I will present these examples mostly by way of throwing out a problem and a solution, and then retroactively explaining \emph{why} the answer works with a few comments on \emph{how} I came up with it.

\section*{Line Segments}

Let's start with the absolutely most basic case of a path -- a line segment.  Of course, we define the \textbf{line segment} between points $a = (x_1, y_1, z_1)$ and $b = (x_2, y_2, z_2)$ to be the set of all points directly between $a$ and $b$.  If we want to parametrize this line segment, we can do so with the path $\c : [0, 1] \to \R^3$ given by \[\c(t) = a + (b - a)t.\]

At first, this answer seems to come from nowhere!  How did we get it, or at least how can I tell that it's correct?  Well...  to check whether it's correct, let's just try plugging in the endpoints $\c(0)$ and $\c(1)$ to see if we get the right things.  Indeed, \[\c(0) = a + (b - a) \cdot 0 = a \ \text{and} \ \c(1) = a + (b - a) \cdot 1 = a + (b - a) = b,\] so at the very least, \c\ does seem to go between $a$ and $b$.  Is \c\ a line segment?  Well... yes, basically by definition :)  another way to see this is that $\c'(t) = (b - a)$, so it has a constant derivative which means it's a straight-looking object.

To do a specific example, you might see that the line segment between $a = (10, -5, 5)$ and $b = (6, 0, 16)$ is given by $\c : [0, 1] \to \R^3$ where \[\c(t) = \begin{bmatrix} 10 \\ -5 \\ 5 \end{bmatrix} + \left(\begin{bmatrix} 6 \\ 0 \\ 16 \end{bmatrix} - \begin{bmatrix} 10 \\ -5 \\ 5 \end{bmatrix}\right) t = \begin{bmatrix} 10 - 4t \\ -5 + 5t \\ 5 + 11 t \end{bmatrix}.\]  Indeed, $\c(0) = (10, -5, 5) = a$ and $\c(1) = (6, 0, 16) = b$ and \c\ goes straight from one to the other, so it works!

What if I took $a = (0, 0, 0)$ and $b = (10, 20, -15)$ and told you that $\c : [-6, -1] \to \R^3$ (\emph{notice the domain of} \c!!) parametrized the line segment from $a$ to $b$, where \[\c(t) = \begin{bmatrix} 12 + 2t \\ 24 + 4t \\ -18 - 3t \end{bmatrix}?\]  Well... this looks a bit weird, but we can still check the endpoints $\c(-6) = (12 + 2 \cdot (-6), 24 + 4 \cdot (-6), -18 - 3 \cdot (-6)) = (0, 0, 0)$ and $\c(-1) = (12 + 2 \cdot (-1), 24 + 4 \cdot (-1), -18 - 3 \cdot (-1)) = (10, 20, -15)$.  So... this parametrization works, but the ``usual one'' would be to take $\mathbf{p} : [0, 1] \to \R^3$ where $\mathbf{p}(t) = (10t, 20t, -15t)$ (check this one, too).

\section*{Circular Segments}

Another super-common type of shape that we see is circular segments -- by this, I mean actual circles, semicircles, quarter circles, or other fractional bits of circles.  At this time, we'll stay in $\R^2$.  

Circular segments are fairly easy to handle by way of polar coordinates.  For example, I may parametrize the semicircle of radius 4 going counterclockwise from $(4, 0)$ to $(-4, 0)$ with $\c : [0, \pi] \to \R^2$ given by $\c(t) = (4 \cos t, 4 \sin t)$.  Indeed, $\c(0) = (4 \cos(0), 4 \sin(0)) = (4, 0)$ and $\c(\pi) = (4 \cos \pi, 4 \sin \pi) = (-4, 0)$, so the endpoints are correct.

But is this a semi-circle?  Well, we have $(4 \cos t)^2 + (4 \sin t)^2 = 4^2$ at each $t$, so at the very least, \c\ \emph{lies within} a circle, but how do we know that \c\ doesn't, say, wrap around the circle one and a half times before landing at $(-4, 0)$?  We can check by seeing that \emph{the largest number} ever plugged in to $\cos$ or $\sin$ in \c\ is $\pi$, so it only runs around half a circle.

A \textbf{non-example} of a parametrization would be $\mathbf{p} : [0, \pi] \to \R^2$ where $\mathbf{p}(t) = (4 \cos(3t), 4\sin(3t))$.  You can check that $\mathbf{p}$ has the correct endpoints, but $\mathbf{p}(\pi / 2) = (0, -4)$, which is not on the correct semicircle!  In fact, $\mathbf{p}$ runs around a full circle \emph{plus} a semicircle on its way to $(-4, 0)$!  A quick way to have discovered this bug would be to notice that $\mathbf{p}(\pi) = (4 \cos(3\pi), 4 \sin(3\pi))$; while $\cos(3\pi)$ is physically the same number as $\cos(\pi)$, the fact that $3 \pi$ is being plugged into $\cos$ suggests that we actually went around a circle and a half!  (Q: Is there a way to ``fix'' $\mathbf{p}$ by changing one of the endpoints of its domain?  A: Yes -- you could either restrict $\mathbf{p}$ to $[0, \pi / 3]$ or $[2 \pi / 3, \pi]$.  Check that these work!)

This example raises a question -- why was the domain $[0, \pi]$ instead of $[0, 1]$ like before?  The answer to this question is mostly a matter of taste, since, as we saw before, there are \emph{always multiple ways to parametrize any curve}.  In my opinion, the quickest and easiest way to parametrize a line segment is to jus go \[\c = \left(\text{starting point}\right) + \left(\text{distance to finish point}\right) t, t \in [0, 1]\] and be done with it, whereas for circle-y objects, my preference is to picture the endpoints of the domain $[a, b]$ as two points on a unit circle; in this case, I wanted to go from the right hand side ($a = 0 \ \mathrm{rad}$) and end at the left hand side ($b = \pi \ \mathrm{rad}$), which lead to the endpoints $[0, \pi]$ used here for $\mathbf{p}$.


\section*{Intersections of surfaces}

For example, if I say "find a parametrization for the intersection of the surfaces $x = y^2$ and $z = x^2$ from $(1, -1, 1)$ to $(4, 2, 16)$," and then just throw out there that the answer is $\mathbf{c}(t) = \begin{bmatrix} t^2 \\ t \\ t^4 \end{bmatrix}, t \in [-1, 2]$, how can you work backwards and just check that this answer is correct?


Well...  all you have to check here is that the image of $\mathbf{c}$ lies within the surfaces $x = y^2$ and $z = x^2$ and that its endpoints are $(-1, 1, 1)$ and $(2, 4, 16)$.  The endpoints are easy -- literally just plug in $-1$ and $2$ to $\mathbf{c}$ and you get


$\mathbf{c}(-1) = (1, -1, 1)$ and $\mathbf{c}(2) = (4, 2, 16)$


as required.  To check whether $\mathbf{c}$ is in the surfaces given, you can literally just plug $\mathbf{c}$ into the equations and verify that they are true!  In particular, to check $x = y^2$, we just plug in the $x$ and $y$ coordinates of $\mathbf{c}$ to the LHS and RHS and make sure we get a true equation -- here, this gives us $x = y^2 \iff (t^2) = (t)^2$, which of course is true.


Similarly, to test whether $\mathbf{c}$ lies in the curve $z = x^2$, we just plug in $t^2$ for $x$ and $t^4$ for $z$ (as these are the component functions of $\mathbf{c}$) and observe that this gives $(t^4) = (t^2)^2$, which again is a true equation.


At this point...  $\mathbf{c}$ lies in the required surfaces and has the correct endpoints, so $\mathbf{c}$ is a correct parametrization of the path required!


After doing this, you might deconstruct the solution and see that the constraint $x = y^2$ poses $x$ as dependent on $y$, and $z = x^2$ poses $z$ as dependent on $x$ -- from this observation, you might notice that the solution starts by setting the "least dependent" variable $y$ as $t$ and then just solving for $x$ and $z$ from there. 


In other words, start by just setting $y = t$ -- the first equation ($x = y^2$) forces $x = t^2$, and then the second equation ($z = x^2$) forces you to have $z = (t^2)^2 = t^4$.  Overall, that gives $\mathbf{c}(t) = (t^2, t, t^4)$, and now we just have to find the proper domain interval $[a, b]$.


Luckily for us, this just means solving two (systems of) old-fashioned equations -- namely $\mathbf{c}(a) = (a^2, a, a^4) = (1, -1, 1)$ and $\mathbf{c}(b) = (b^2, b, b^4) = (4, 2, 16)$.  Even more luckily for us, the equation for the $y$ coordinate in both of these equations immediately yields $a = -1$ and $b = 2$. \vspace{1cm}

Again, try to verify the following before reading the discussion that follows: the intersection between the sphere of radius $1$ and the plane $x = y$ may be parametrized by \[\c(t) = \begin{bmatrix} \frac{1}{\sqrt{2}} \cos t \\ \frac{1}{\sqrt{2}} \cos t \\ \sin t \end{bmatrix}, t \in [0, 2 \pi].\]

This was solved by using the equation $x = y$ in the equation describing the sphere; namely, we took \[x^2 + y^2 + z^2 = 1 \to y^2 + y^2 + z^2 = 2y^2 + z^2 = 1,\] and we parametrized this last bit with the usual ellipse method -- we rewrite $2y^2 + z^2 = 1$ as $(\sqrt{2} y)^2 + z^2 = 1$ so we can set $\sqrt{2} y = \cos t$ and $z = \sin t$.  Combining this with the plane $x = y$ finally gives us \c as above! \vspace{1cm}

How about the intersection of the plane $x + 2y + z = 2$ with the cylinder $(x - 1)^2 + (y + 3)^2 = 1$?  Well, one parametrization is \[\c(t) = \begin{bmatrix} \cos t + 1 \\ \sin t - 3 \\ 7 - \cos t - 2 \sin t \end{bmatrix}, t \in [0, 2\pi] .\]

This was built by just starting with the cylinder part: $(x - 1)^2 + (y + 3)^2 = 1$ immediately lends itself to $(x - 1) = \cos t$ and $(y + 3) = \sin t$, which gives the $x$ and $y$ used above.  Finally, using the equation for the plane $x + 2y + z = 2$ gives $z = 2 - x - 2y = 2 - (\cos t + 1) - 2 (\sin t - 3) = 7 - \cos t - 2 \sin t$.

\section*{Parametrizing surfaces}

Some of the standard ``geometrics'' examples are cylinders, spheres, cones, planes, and subsets of each of these.  Let's get started!

The unit sphere is most easily parametrized using spherical coordinates; namely, \[\Phi(u, v) = \begin{bmatrix} \sin v \cos u \\ \sin v \sin u \\ \cos v \end{bmatrix}, u \in [0, 2\pi], v \in [0, \pi]\] which, as you can see, is simply the usual spherical coordinates transformation with $\rho$ frozen at $\rho = 1$ and the letters $\theta$ and $\phi$ replaced with $u$ and $v$, respectively.  In fact, it's equally valid to write this as \[\Phi(\theta, \phi) = \begin{bmatrix} \sin \phi \cos \theta \\ \sin \phi \sin \theta \\ \cos \phi \end{bmatrix},\] which may feel more intuitive.

From this, we can figure out how to parametrize a number of objects by just adjusting the domain of this parametrization!  For example, the southern hemisphere may be parametrized by requiring $\phi \ge \pi / 2$ and taking $(\theta, \phi) \in [0, 2\pi] \times [\pi / 2, \pi]$.  Alternatively, the band $-1/2 \le z \le 1/2, x + y \ge 0$ can be obtained by taking $\phi \in [\pi / 3, 2\pi / 3], \theta \in [-\pi / 4, 3 \pi / 4]$ (try drawing the $xy$ portion by itself in the $xy$ plane and analyzing it in polar coordinates).

Another way we can use spherical coordinates is to actually parametrize an ellipsoid by simply stretching the three coordinates -- for example, if we take \[\Phi(\theta, \phi) = \begin{bmatrix} 4 \cos \theta \sin \phi \\ 1/2 \sin \theta \sin \phi \\ 3 \cos \phi \end{bmatrix},\] it's not too far to see that this parametrizes the ellipse $\left(\dfrac{x}{4}\right)^2 + (2y)^2 + \left(\dfrac{z}{3}\right)^2 = 1$, which is a sphere that has been stretched in the $x$ and $z$ directions and squashed in the $y$ direction.

Next, we have cylinders, which are defined by taking two of the coordinates $(x, y, z)$ and fixing them on a circle while the other coordinate runs free.  For example, we might have $x^2 + y^2 = 4, -1 \le z \le 1$, which is a cylinder of radius $2$ and height $2$ that goes along the $z$-axis.  A cylinder is (somewhat unsurprisingly) well parametrized using cylindrical coordinates, i.e. \[\Phi(u, v) = \begin{bmatrix} 2 \cos u \\ 2 \sin u \\ v \end{bmatrix}, u \in [0, 2\pi], v \in [-1, 1]\] in this case.  Again, we can substitute the letters $(u, v)$ with something ``more familiar'' for our geometric setting: \[\Phi(\theta, z) = \begin{bmatrix} 2 \cos \theta \\ 2 \sin \theta \\ z \end{bmatrix}, (\theta, z) \in [0, 2 \pi] \times [-1, 1].\]  As was described earlier, precisely what's happening here is that we're making $x$ and $y$ run around a circle, and $z$ is allowed to run around independently.

For another example, we can just as easily take a cylinder that goes along the $x$-axis by putting $y$ and $z$ on a circle; it could even be a squashed cylinder!  Say we have $y^2 + 4z^2 = 9, 1 \le x \le 5$.  This can be parametrized with \[\Phi(x, \theta) = \begin{bmatrix} x \\ 3 \cos \theta \\ \frac{3}{2} \sin \theta \end{bmatrix}, (x, \theta) \in [1, 5] \times [0, 2\pi].\]  We got $y$ and $z$ with our usual ellipse routine: \[y^2 + 4z^2 = 9 \to y^2 + (2z)^2 = 3^2 \implies y = 3\cos \theta, 2z = 3\sin \theta \implies y = 3 \cos \theta, z = \frac{3}{2} \sin \theta,\] and $x$ (being the ``axial variable'') is its own, independent variable.

Now we can consider cones of several different flavors: there's the basic cone, which goes up the $z$-axis and we have $x$ and $y$ always living on a circle of radius $z$.  We can describe this surface with the equation $x^2 + y^2 = z^2$, and we can parametrize it by doing what we always do when we see something like ``$x^2 + y^2$'' -- we put them on a circle with $\cos \theta$ and $\sin \theta$!  Namely, we recognize (again) that $x^2 + y^2 = z^2$ means that $(x, y)$ is on a circle of radius $z$, so we can straightforwardly set \[\Phi(\theta, z) = \begin{bmatrix} z \cos \theta \\ z \sin \theta \\ z \end{bmatrix},\] and this is the pattern that all ``cone-like'' things will follow!  As a further example, consider the cone going along the $x$-axis given by $y^2 + z^2 = \frac{x^2}{4} = (x / 2)^2$.  It almost parametrizes itself as \[\Phi(x, \theta) = \begin{bmatrix} x \\ \frac{x}{2} \cos \theta \\ \frac{x}{2} \sin \theta \end{bmatrix}.\]

A great extension of this discussion is ``surfaces of rotation,'' which is addressed in your textbook, pp.~ 387-389.



%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

\end{document}

