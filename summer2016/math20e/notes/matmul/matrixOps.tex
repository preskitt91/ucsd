%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}

\renewcommand{\u}{\ensuremath{\mathbf{u}}}
\renewcommand{\v}{\ensuremath{\mathbf{v}}}
\newcommand{\w}{\ensuremath{\mathbf{w}}}
\newcommand{\x}{\ensuremath{\mathbf{x}}}
\newcommand{\y}{\ensuremath{\mathbf{y}}}
\newcommand{\z}{\ensuremath{\mathbf{z}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{Dot Products and Matrix Multiplication}
\author{Math 20E, Summer Session II}
\date{}

\begin{document}
\maketitle

For vector calculus, there is a little we need to know about matrices at various points throughout the course.  If you haven't encountered these calculations in a previous course, we'll lay them out right here.

\section*{Dot Products}

Given two vectors $\mathbf{u}, \mathbf{v} \in \Rn$, there is a calculation we may perform on them called the \textbf{dot product} or \textbf{inner product} of $\mathbf{u}$ and $\mathbf{v}$.  Namely, if \[\u = \begin{bmatrix} u_1 \\ u_2 \\ \vdots \\ u_n \end{bmatrix} \ \text{and} \ \v = \begin{bmatrix} v_1 \\ v_2 \\ \vdots \\ v_n \end{bmatrix},\] then the dot product, written $\u \cdot \v$, is given by \[\u \cdot \v = u_1 v_1 + u_1 v_2 + \cdots + u_n v_n,\] namely it is just the sum of the products of the entries of the two vectors!

For example, if we have \[\u = \begin{bmatrix} 3 \\ -6 \\ -1 \end{bmatrix}, \v = \begin{bmatrix} 2 \\ 0 \\ 2 \end{bmatrix},\] then \[\u \cdot \v = (3 \cdot 2) + (-6 \cdot 0) + (-1 \cdot 2) = 4.\]  If \[\v_1 = \begin{bmatrix} -1 \\ 1 \\ 5 \\ -1 \end{bmatrix}, \v_2 = \begin{bmatrix} 5 \\ -3 \\ 12 \\ -3 \end{bmatrix},\] then \[\v_1 \cdot \v_2 = (-1 \cdot 5) + (1 \cdot -3) + (5 \cdot 12) + (-1 \cdot -3) = 55.\]

Additionally, the \textbf{norm}, \textbf{length}, or \textbf{magnitude} of a vector $\v \in \Rn$ is written $|| \v ||$ and is given by \[|| \v || = \sqrt{\v \cdot \v}.\]  For example, if we have $\w = \langle -4, 3 \rangle$, then $|| \w || = \sqrt{(-4 \cdot -4) + (3 \cdot 3)} = \sqrt{(-4)^2 + (3)^2} = 5$.  Notice that this is the ``square root of the sum of the squares'' of the components of a vector, so this definition of length coincides with the usual distance formula!

\textbf{Remark:}  Different writers may use different notation for the inner product.  You may see $\langle \u, \v \rangle, \u \cdot \v,$ or $\u^T \v$.  The first two are just ``conventions,'' while the last one comes from matrix multiplication, as you'll see below.  In this class, I anticipate mainly using $\u \cdot \v$ to denote the dot product of $\u$ and $\v$.

\section*{Matrix Multiplication}

Given two matrices $A \in \R^{m \times n}$ and $B \in \R^{k \times p}$, $A$ and $B$ may be multiplied when their sizes are compatible.  In particular, $AB$ exists \emph{only when $n = k$}; that is, when $A$ has as many columns as $B$ has rows.  In other words, we will actually need to have $A \in \R^{m \times n}$ and $B \in \R^{n \times k}$ in order to multiply $AB$.  On the other hand, we would need $A \in \R^{p \times n}$ and $B \in \R^{k \times p}$ to multiply $BA$ (it's not the \emph{letters} here that matter -- just the pattern of which numbers match).

The best way to remember what sizes two matrices must have to be compatible for multiplication is to picture it this way: if $A \in \R^{m \times n}$ and $B \in \R^{n \times k}$, then $AB \in \R^{m \times k}$.  In other words, \[(m \times n)(n \times k) \to (m \times k).\]  You can multiply when the \emph{adjacent dimensions} match, and the size of the product is given by the \emph{outside dimensions}.

This size requirement comes from how the calculation is carried out -- namely, the product $AB$ (for eligible matrices $A$ and $B$) has entries given by the \emph{dot products of the rows of $A$ with the columns of $B$}.  Specifically, if we label the rows of $A \in \Rmxn$ and $B \in \R^{n \times k}$ as \[A = \begin{bmatrix} a_1 \\ a_2 \\ \vdots \\ a_m \end{bmatrix}, B = \begin{bmatrix} b_1 & b_2 & \cdots & b_k \end{bmatrix},\] then $(AB)_{ij} = a_i \cdot b_j$.  In other words, the $(i, j)$th entry of $AB$ is the dot product between the $i$th row of $A$ and the $j$th row of $B$.  For example, if we have \[A = \begin{bmatrix} 1 & 2 \\ 3 & 4 \end{bmatrix}, B = \begin{bmatrix} 5 & 6 \\ 7 & 8 \end{bmatrix},\] then \[AB = \begin{bmatrix} (1, 2) \cdot (5, 7) & (1, 2) \cdot (6, 8) \\ (3, 4) \cdot (5, 7) & (3, 4) \cdot (6, 8) \end{bmatrix} = \begin{bmatrix} 1 \cdot 5 + 2 \cdot 7 & 1 \cdot 6 + 2 \cdot 8 \\ 3 \cdot 5 + 4 \cdot 7 & 3 \cdot 6 + 4 \cdot 8 \end{bmatrix} = \begin{bmatrix} 19 & 22 \\ 43 & 50 \end{bmatrix}.\]  As you can see, both $A$ and $B$ were $2 \times 2$ matrices, so their product $AB$ was of size $(2 \times 2)(2 \times 2) \to (2 \times 2)$.  Additionally, it would be ``legal'' to multiply $BA$ here because the sizes still match correctly.  Indeed, if we do the whole calculation, \[BA = \begin{bmatrix} (5, 6) \cdot (1, 3) & (5, 6) \cdot (2, 4) \\ (7, 8) \cdot (1, 3) & (7, 8) \cdot (2, 4) \end{bmatrix} = \begin{bmatrix} 23 & 34 \\ 31 & 46 \end{bmatrix}.\]  \emph{It is important to observe that $BA \neq AB$!!}  In general, matrix multiplication is \emph{not} commutative, meaning you cannot switch two matrices and get the same answer!  As long as you follow the order of matrix multiplication as it is, you'll have no problem here.

For another example, see if you can multiply \[A = \begin{bmatrix} 3 & 0 & 3 & -1 \\ 0 & -2 & 2 & -2 \\ 6 & 4 & 3 & 5 \end{bmatrix}, B = \begin{bmatrix} 0 & -2 \\ 1 & -2 \\ 5 & 5 \\ -4 & 0 \end{bmatrix}\] and get \[AB = \begin{bmatrix} 19 & 9 \\ 16 & 14 \\ -1 & -5 \end{bmatrix}.\]  Does it make sense to multiply $BA$?

\section*{Transpose}

A small note -- for any matrix $A \in \Rmxn$, we can turn this matrix ``on its side'' to get another matrix, which we call the \textbf{transpose} of $A$, written $A^T \in \R^{n \times m}$.  More specifically, $A^T$ is the matrix whose columns are the rows of $A$ (equivalently, its rows are taken from the columns of $A$).  For example, if we have \[A = \begin{bmatrix} 3 & 0 & 3 & -1 \\ 0 & -2 & 2 & -2 \\ 6 & 4 & 3 & 5 \end{bmatrix},\] then \[A^T = \begin{bmatrix} 3 & 0 & 6 \\ 0 & -2 & 4 \\ 3 & 2 & 3 \\ -1 & -2 & 5 \end{bmatrix}.\]  This concept has already been seen in our class; for example, when we defined the gradient of a function $f : \Rn \to \R$, we wrote \[\nabla f = (D f)^T;\] as $D f \in \R^{1 \times n}$, we will have $\nabla f \in \R^{n \times 1} = \Rn$ (a matrix in $\R^{n \times 1}$ has one column with $n$ entries, so we consider it to be equivalent to a vector in $\Rn$ with $n$ entries).

Additionally, this makes sense of why the dot product $\u \cdot \v$ may be rewritten as $\u^T \v$; indeed, if we take \[\u = \begin{bmatrix} 3 \\ -6 \\ -1 \end{bmatrix}, \v = \begin{bmatrix} 2 \\ 0 \\ 2 \end{bmatrix},\] then \[\u^T \v = \begin{bmatrix} 3 & -6 & -1 \end{bmatrix} \begin{bmatrix} 2 \\ 0 \\ 2 \end{bmatrix},\] which, when viewed as a product of two matrices (compatible because we have $(1 \times 3)(3 \times 1) \to (1 \times 1)$, and we consider $1 \times 1$ matrices to just be scalars), gives us $4$ as before!

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

\end{document}

