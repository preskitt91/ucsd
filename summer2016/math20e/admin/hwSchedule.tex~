%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm}
\usepackage{graphics, parskip, color}
\usepackage{listings, soul}
\usepackage{appendix}
\usepackage{afterpage}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{Math 20E Homework/Exam Schedule}
\author{Instructor:  Brian Preskitt}
\date{\color{red} \Large LAST UPDATED: \today}

\begin{document}
\maketitle

Homework is due by 3:00PM on the due date, in the dropbox for your
section in the Basement of AP\&M. Late homework will not be accepted.

Please observe the following neatness guidelines for homework that you
turn in to be graded; homework not conforming to these guidelines will
not receive full credit and may not be graded at all.

\begin{enumerate}
    \item Use clean, white paper (preferably ruled) that is not torn
      from a spiral notebook.
    \item Write your Name, PID, and Section clearly on the front page
      of your completed assignment.
      \item Write clearly and legibly.
        \item Clearly number each solution and present them in
          numerical order.
\end{enumerate}

The following homework assignments are subject to change as the term proceeds.

\pagebreak
{\large \textbf{Homework 0}}
Review (not to be turned in):  The corresponding topics were covered in Math 20C and you will be expected to be familiar with them.

\begin{itemize}
    \item Section 1.1:  (pg 18)  1, 4, 7, 11, 17
    \item Section 1.2:  (pg 29)  3, 7, 12, 22
    \item Section 1.3:  (pg 49)  2b, 5, 11, 15ad, 16b, 30
    \item Section 1.4:  (pg 58)  1, 3ab, 9, 10, 11
    \item Section 2.1:  (pg 85)  2, 9, 10b, 30, 40
    \item Section 2.2:  (pg 103)  2, 6, 9c, 10b, 16
    \item Section 2.4:  (pg 123)  1, 3, 9, 14, 17
    \item Section 2.6:  (pg 142)  2b, 3b, 9b, 10c, 20
    \item Section 3.1:  (pg 156)  2, 9, 10, 25
    \item Section 4.1:  (pg 227)  2, 5, 11, 13, 19
    \item Section 4.2:  (pg 234)  3, 6, 7, 9, 13
\end{itemize}

{\large \textbf{Homework 1}}
Due Friday, August 5

\begin{itemize}
    \item Section 2.3:  (pg 115)  5, 6, 10, 12, 19, 20, 28
    \item Section 2.5:  (pg 132)  6, 8, 11, 14, 32, 34
    \item Section 3.2:  (pg 165)  3, 4, 6, 12 (only for ex. 3 and 4)
    \item Section 4.3:  (pg 243)  4, 5, 9, 10, 12, 15, 16
\end{itemize}

{\large \textbf{Midterm 1} on  August 10 covers sections 2.3, 2.5, 3.2, 4.3, 4.4, 5.1, 5.2, 5.3}

{\large \textbf{Homework 2}}
Due Friday, August 12

\begin{itemize}
    \item Section 4.4:  (pg 258)  3, 9, 15, 19, 34, 36, 38, 39
    \item Section 5.1:  (pg 269)  3ac, 7, 11, 14
    \item Section 5.2:  (pg 282)  1d, 2c, 7, 8, 9, 17
    \item Section 5.3:  (pg 288)  4ad, 7, 8, 11, 15
    \item Section 5.4:  (pg 293)  3ac, 4ac, 7, 10, 14, 15
    \item Section 5.5:  (pg 302)  5, 13, 17, 30
\end{itemize}

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

\end{document}

