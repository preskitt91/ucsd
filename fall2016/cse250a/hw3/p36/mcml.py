import numpy as np

Z = 128
numbits = 10
alpha = 0.2
numsams = 32

p = np.zeros(numbits)

for i in range(numbits):
    num = 0
    den = 0
    for sam in range(numsams):
        B = ranbits(numbits - 1)
        bi = drawbit(i, B, Z, alpha)
        q  = probpex(i, B, Z, alpha)
        if bi:
            num = num + q
        den = den + q
    p[i] = num / den

def ranbits(bits, i):
    B = np.zeros(k)
    for j in range(k):
        if np.random.random() < 0.5 and j != i:
            B[j] = 1
    return B
    
def drawbit(i, B, Z, alpha):
    b0 = debin(B)
    b1 = b0 + pow(2, i)
    p = pow(alpha, abs(Z - b1)) / (pow(alpha, abs(Z - b0)) +
                                         pow(alpha, abs(Z - b1)))
    if np.random.random() < p:
        return 1
    else:
        return 0

def probpex(i, B, Z, alpha):
    b0 = debin(B)
    b1 = b0 + pow(2, i)
    return (pow(alpha, abs(Z - b0)) + pow(alpha, abs(Z - b1)))

def debin(bits):
    tot = 0
    for k in range(len(bits)):
        tot += pow(2, k) * bits[k]
    return tot
    
    
