import numpy as np
from matplotlib import pyplot as plt

tr3fn = 'train3.txt'
tr5fn = 'train5.txt'
te3fn = 'test3.txt'
te5fn = 'test5.txt'

tr3 = np.loadtxt(tr3fn)
tr5 = np.loadtxt(tr5fn)
te3 = np.loadtxt(te3fn)
te5 = np.loadtxt(te5fn)

N = tr3.shape[1]

def initWeights():
    w = sum(tr5, 1) - sum(tr3, 1)
    w = w / sum(np.abs(w))

    return w

def update(w, eta, dat3=tr3, dat5=tr5):
    w = w - sum(eta * sgm(np.dot(dat3, w))[:, None] * dat3, 0)
    w = w + sum(eta * sgm(-np.dot(dat5, w))[:, None] * dat5, 0)
    return w

def logLike(w, dat3, dat5):
    return sum(np.log(sgm(-np.dot(dat3, w)))) + sum(np.log(sgm(np.dot(dat5, w))))

def sgm(z):
    return 1 / (1 + np.exp(-z))

def test(w, dat3=te3, dat5=te5):
    res3 = (np.dot(dat3, w) >= 0).astype(float)
    res5 = (np.dot(dat5, w) >= 0).astype(float)
    err3 = sum(res3) / dat3.shape[0]
    err5 = sum(1 - res5) / dat5.shape[0]

    err = (sum(res3) + sum(1 - res5)) / (dat3.shape[0] + dat5.shape[0])

    return err3, err5, err

def plotDat(numiter, eta):
    w = initWeights()
    
    ll = np.array([logLike(w, tr3, tr5)])
    e3, e5, __ = test(w, tr3, tr5)
    err3 = np.array(e3)
    err5 = np.array(e5)

    for i in range(numiter):
        w = update(w, eta, tr3, tr5)
        ll = np.hstack([ll, logLike(w, tr3, tr5)])
        e3, e5, __ = test(w, tr3, tr5)
        err3 = np.hstack([err3, e3])
        err5 = np.hstack([err5, e5])
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.plot(range(ll.size), ll)
    ax1.set_title('Log-Likelihood over gradient iterations')
    ax2 = fig.add_subplot(212)
    ax2.plot(range(err3.size), err3, 'b', label="% Err, 3's")
    ax2.plot(range(err5.size), err5, 'g', label="% Err, 5's")
    ax2.legend()
    ax2.set_title('Classification error rates over iterations')
    fig.show()
