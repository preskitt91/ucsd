%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol, minted}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathcal{F}}}
\newcommand{\mg}{martingale}
\newcommand{\E}{\ensuremath{\mathbb{E}}}
\newcommand{\rv}{random variable}


%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{}
\author{Brian Preskitt}
\date{\today}

\begin{document}
\textbf{Problem 5.5}

\begin{enumerate}[(a)]
  \item We recall the derivation from class that \[\frac{\partial L}{\partial a_i} = \sum_{t = 5}^T \sum_{j = 1}^4 a_j x_{t - j} x_{t - i} - x_t x_{t - i},\] so to minimize $L$, we want $\partial L / \partial a_i$ to simultaneously be $0$; namely, we rearrange the above and solve $Aw = b$ where $w = (a_1, a_2, a_3, a_4)^T$, $A_{ij} = \sum_{t = 5}^T x_{t - j} x_{t - i}$, and $b_i = \sum_{t = 5}^T x_t x_{t - i}$.  

    We run the linear solver in \texttt{python3} and determine the weights:
\begin{minted}{python}
>>> import cse250ahw5_5 as p5
>>> import numpy as np
>>> from matplotlib import pyplot as plt
>>> 
>>> trainDat, a = p5.trainSystem()
>>> a
array([ 0.94520006,  0.01974237, -0.01364498,  0.04678134])
\end{minted}

\item For the squared error, we run the predictor on the training data (from 2000) and calculate the squared error on both the test data (from 2001) and training data:
\begin{minted}{python}
>>> trPred, tePred, trErr, trRel, teErr, teRel = p5.testSystem()
>>> trErr, trRel
(3410065.0656060893, 0.03073539587961796)
>>> teErr, teRel
(736457.353186002, 0.026788245430859756)
\end{minted}
This doesn't look so bad!  We only have a $2-3\%$ relative squared error (\texttt{trRel} is training relative error and \texttt{teRel} is test relative error) on both data sets (using the same linear coefficients for each)!  However... the weight vector we calculated weights the immediate predecessor of any data point very heavily ($a_1 = 0.945$), so we ask if our model is really much better than the trivial model of guessing that the next day's index price will be exactly equal to today's:
\begin{minted}{python}
>>> testDat = np.loadtxt('nasdaq01.txt')
>>> triv = [1.0, 0.0, 0.0, 0.0]
>>> trivPred = p5.modPred(testDat, triv)
>>> trivErr, trivRel = p5.sqErr(testDat, trivPred)
>>> trivErr, trivRel
(737895.47700000065, 0.026814388169772774)
\end{minted}
We still get a roughly $2.5\%$ error here.  It would appear that the accuracy of our model is really an artifact of the slow-moving market instead of an actual triumph of mathematics -- I would \emph{not recommend} this model, since it can only guess one day in the future, and basically just always guesses that the price won't change much.

\item The source code is printed below:
\inputminted[linenos]{python}{cse250ahw5_5.py}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{nasdaqlinear.png}
  \end{center}
  \caption{Predictions and truth for linear model of stock prices}
\end{figure}
\end{enumerate}

\textbf{Problem 5.6}

\begin{enumerate}[(a)]
\item For my work, I used gradient ascent for the log-likelihood maximization, with the observation that \[\left.\nabla L\right|_{w} = -\sum_{t = 1}^{T_3} p_t x_t + \sum_{t = 1}^{T_5} (1 - p_{T_3 + t}) x_{T_3 + t} = -\sum_{v \in A_3} \sigma(w \cdot v) v + \sum_{v \in A_5} \sigma(-w \cdot v) v,\] where $v \in A_3$ refers to summing over all the images in the training set of 3's and $v \in A_5$ refers to summing over all the training images of 5's.  I used an $\eta$ value of $0.001$ (because it was the largest value that worked -- quick convergence). The convergence of the log-likeliheed and the classification error rates are illustrated in Figure~\ref{fig:p6train}.  You can see that I let the algorithm run for 2048 iterations, but could have gotten away with only running 256.  Performance results for the final weight vector are shown in Table~\ref{tab:p6train}.  As you can see, the model works quite well, yielding a $<4\%$ error rate, performing slightly better on 5's than on 3's.  The numerical values of the final weight vector are displayed below:

  \begin{minted}{python}
[[-1.077 -2.005 -1.392 -1.541 -0.779 -1.213  1.057  2.143]
 [-0.222 -0.241  0.287 -0.031 -0.633  1.001 -1.626 -1.692]
 [ 4.984  1.640  1.860  0.165  1.099 -2.751 -2.961 -3.388]
 [ 0.966  0.426  0.885 -0.364 -0.604 -3.012  0.694 -0.005]
 [ 0.813  1.373  0.105 -0.526 -0.771 -0.074 -0.729 -0.312]
 [ 1.496 -0.256 -0.609 -0.121 -0.095 -1.164  1.174 -1.954]
 [ 2.005 -0.888  1.695  0.805  0.679 -0.541  0.137 -1.752]
 [ 0.800  0.377  1.091  2.876  0.515  0.846  1.021 -0.658]]    
  \end{minted}

  \begin{figure}
    \begin{center}
      \includegraphics[width=0.8\textwidth]{display_2048}
      \includegraphics[width=0.8\textwidth]{display_256}
    \end{center}
    \caption{Convergence of Log-Likelihood and Classification Error Rates}    
    \label{fig:p6train}
  \end{figure}

  \begin{table}
    \begin{center}
    \begin{tabular}{|c|c|c|c|}
      \hline
      Log-Likelihood & Err Rate on 3's & Err Rate on 5's & Total Err Rate \\ \hline
      -170.1 & 5.14\% & 2.43\% & 3.79\% \\\hline
    \end{tabular}
    \end{center}

    \caption{Performance on training data}
    \label{tab:p6train}
  \end{table}
\pagebreak
  \item The same model was applied to the test data, with performance results listed in Table~\ref{tab:p6test}.  It worked better on the training data, but overall it worked on the test data!  It had a $<7\%$ total error rate, performing better on the 5's than on the 3's.

    \begin{table}
      \begin{center}
        \begin{tabular}{|c|c|c|c|}
\hline
Log-Likelihood & Err Rate on 3's & Err Rate on 5's & Total Err Rate \\ \hline
-137.4 & 9.00\% & 4.25\% & 6.63\% \\\hline
        \end{tabular}
      \end{center}

      \caption{Performance results of trained model on test data}
      \label{tab:p6test}      
    \end{table}

\item  The source code is displayed below:

\inputminted[linenos]{python}{cse250ahw5_6.py}
\end{enumerate}




\end{document}
