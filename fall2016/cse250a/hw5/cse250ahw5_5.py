import numpy as np
from numpy import linalg as la
from matplotlib import pyplot as plt

order = 4
trainFile = 'nasdaq00.txt'
testFile = 'nasdaq01.txt'

def trainSystem():
    trainDat = np.loadtxt(trainFile)
    T = len(trainDat)
    A = np.zeros([order, order])
    b = np.zeros(order)
    for i in range(order):
        for j in range(order):
            A[i, j] = sum(trainDat[(order - (j + 1)):T-(j + 1)] * 
                          trainDat[(order - (i + 1)):T-(i + 1)])
        b[i] = sum(trainDat[order:] * trainDat[(order - (i + 1)):T-(i + 1)])
    a = la.solve(A, b)

    return trainDat, a

def testSystem():
    trainDat, a = trainSystem()
    T0 = trainDat.size
    testDat = np.loadtxt(testFile)
    T1 = testDat.size

    trPred = modPred(trainDat, a)
    tePred = modPred(testDat, a)
    trErr, trRel = sqErr(trainDat, trPred)
    teErr, teRel = sqErr(testDat, tePred)
    
    return trPred, tePred, trErr, trRel, teErr, teRel

def modPred(dat, weights):
    order = len(weights)
    T = len(dat)
    t = range(order, T)
    pred = np.zeros(T - order)
    for i in t:
        pred[i - order] = sum(dat[(i - order):i] * weights[::-1])
    return pred

def sqErr(dat, pred):
    T = len(pred)
    err = sum((dat[-T:] - pred) * (dat[-T:] - pred))
    rel = np.sqrt(err / sum(dat[-T:] * dat[-T:]))

    return err, rel

def plotStuff():
    trainDat, a = trainSystem()
    testDat = np.loadtxt(testFile)
    trp, tep, __, __, __, __ = testSystem()

    order = a.size
    T0 = trainDat.size
    T1 = testDat.size
    t0 = np.arange(T0 - order)
    t1 = np.arange(T1 - order)

    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.plot(t0, trainDat[order:], 'g', label='True Data') 
    ax1.plot(t0, trp, 'r', label='Model Predictions')
    ax1.legend()
    ax1.set_title('2000')
    ax2 = fig.add_subplot(212)
    ax2.plot(t1, testDat[order:], 'g', label='True Data')
    ax2.plot(t1, tep, 'r', label='Model Predictions')
    ax2.legend()
    ax2.set_title('2001')

    fig.show()
    
