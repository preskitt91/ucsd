import numpy as np
from matplotlib import pyplot as plt

vocab = np.loadtxt('vocab.txt', dtype='str')
for i in range(len(vocab)):
    vocab[i] = vocab[i][2:-1].upper()

uni = np.loadtxt('unigram.txt', dtype='int')
totWords = sum(uni)
bigram = np.loadtxt('bigram.txt', dtype='int')

bidict = dict()
for bi in bigram:
    bidict[(vocab[bi[0] - 1], vocab[bi[1] - 1])] = bi[2]

wti = dict()
for i in range(len(vocab)):
    wti[vocab[i]] = i

uprob = dict()
for i in range(len(vocab)):
    uprob[vocab[i]] = uni[i] / totWords

bprob = dict()
for k in bidict.keys():
    bprob[k] = bidict[k] / uni[wti[k[0]]]

def uniprob(sentence):
    prob = 1
    for tok in sentence:
        tok = tok.upper()
        prob = prob * uprob[tok]
    return prob

def biprob(sentence):
    prob = 1
    t = '<S>'
    for tok in sentence:
        tok = tok.upper()
        if not tok in wti.keys():
            tok = '<UNK>'
        if (t, tok) in bprob.keys():
            prob = prob * bprob[(t, tok)]
        else:
            return 0
        t = tok
    return prob

def mixprob(sentence, lamb):
    prob = 1
    t = '<S>'
    for tok in sentence:
        tok = tok.upper()
        if not tok in wti.keys():
            tok = '<UNK>'
        if (t, tok) in bprob.keys():
            itprob = lamb * bprob[(t, tok)] + (1 - lamb) * uprob[tok]
        else:
            itprob = (1 - lamb) * uprob[tok]
        prob = prob * itprob
        t = tok
    return prob
