from hw4 import *

lam = np.arange(0, 1, 0.001)
sentence = 'The nineteen officials sold fire insurance'.split(' ')
a = np.array([])
for l in lam:
    a = np.hstack([a, [np.log(mixprob(sentence, l))]])
    
optlam = lam[np.argmax(a)]
print([optlam, max(a)])

plt.plot(lam, a)
plt.title('Log Likelihood, Mixed Probability vs. Lambda')
plt.show()
