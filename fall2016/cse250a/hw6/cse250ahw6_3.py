import numpy as np
from matplotlib import pyplot as plt

def partAits(n, x0):
    x = x0
    xn = x0
    for i in range(n):
        xn1 = xn - np.tanh(xn)
        x = np.hstack([x, xn1])
        xn = xn1

    plt.plot(x)
    plt.title('{0} iterations, x0 = {1}'.format(n, x0))
    plt.show()
    return x

def partAnewt(n, x0):
    x = x0
    xn = x0
    for i in range(n):
        xn1 = xn - np.tanh(xn) / (1 - np.tanh(xn)**2)
        x = np.hstack([x, xn1])
        xn = xn1

    plt.plot(x)
    plt.title('''{0} iterations, x0 = {1}, Newton's'''.format(n, x0))
    plt.show()
    return x
    
def partBits(n, x0):
    x = x0
    xn = x0
    N = 10
    k = np.arange(1, N + 1)
    
    for i in range(n):
        xn1 = xn - 1 / N * sum(np.tanh(xn + 1 / k))
        x = np.hstack([x, xn1])
        xn = xn1

    plt.plot(x)
    plt.title('{0} iterations, x0 = {1}'.format(n, x0))
    plt.show()
    return x
    
