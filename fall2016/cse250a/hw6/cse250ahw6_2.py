import numpy as np

def logLike(x, y, p):
    '''
    '''
    T = x.shape[0]
    P = np.prod(1 - x * p, 1) * (1 - 2 * y) + y
    
    return sum(np.log(P)) / T

def probXYP(x, y, p):
    '''
    '''
    return 1 - np.prod(1 - x * p, 1)

def posteriors(x, y, p):
    '''
    '''
    return (y[:, np.newaxis] * x * p / probXYP(x, y, p)[:, np.newaxis])

def EMupdate(x, y, p):
    '''
    '''
    post = posteriors(x, y, p)
    T = np.sum(x, 0)
    return np.sum(post, 0) / T
    
def mistakes(x, y, p):
    '''
    '''
    k = np.floor(probXYP(x, y, p) * 2)
    return sum(k != y)

def fullRun():
    '''
    '''
    x = np.loadtxt('spectX.txt')
    y = np.loadtxt('spectY.txt')
    N = x.shape[1]
    p = np.ones(N) / N
    its = 256

    ll = logLike(x, y, p)
    m = mistakes(x, y, p)

    for i in range(its):
        p = EMupdate(x, y, p)
        ll = np.hstack([ll, logLike(x, y, p)])
        m = np.hstack([m, mistakes(x, y, p)])
    
    return ll, m
