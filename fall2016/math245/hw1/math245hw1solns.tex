%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathcal{F}}}
\newcommand{\mg}{martingale}
\newcommand{\E}{\ensuremath{\mathbb{E}}}
\newcommand{\rv}{random variable}
\newcommand{\psd}{\ensuremath{\mathcal{S}_+}}
\newcommand{\psdn}{\ensuremath{\mathcal{S}_+^n}}



%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{Math 245A Homework Assignment \#1}
\author{Instructor:   Jiawang Nie}
\date{\textbf{Due date}:  October 14, 2016}

\begin{document}
\maketitle

\begin{enumerate}
\item Let $a_1, a_2 \in \Rn$.  Define two half spaces \[H_1^+ = \{x \in \Rn : a_1^T x \le 1\}, H_2^+ = \{x \in \Rn : a_2^T x \le 1 \}.\]  If $H_1^+ \subeq H_2^+$, show that there exists $\lambda \in [0, 1]$ such that $a_2 = \lambda a_1$.

\textbf{Solution}  Assume $H_1^+ \subeq H_2^+$.  We first eliminate the case $a_1 = 0$ by considering that, if $a_1 = 0$, we have $H_1^+ = \Rn$, which implies $H_2^+ = \Rn$ and hence $a_2 = 0 = 1 \cdot a_1$.

On the other hand, if $a_1 \neq 0$, let $B = \{v_1, \ldots, v_n\}$ be an orthonormal basis for $\Rn$ with $v_1 = a_1 / ||a_1||_2$.  Then \[H_1^+ = \{\sum_{i = 1}^n c_i v_i : c_1 \le \frac{1}{||a_1||_2}.\]  Representing $a_2$ under $B$ as $a_2 = \sum_{i = 1}^n \alpha_i v_i$, we have for any $x \in H_1^+$ \[a_2^T x \le 1 \iff \sum_{i = 1} c_i \alpha_i \le 1;\] for this to hold for arbitrary $x \in H_1^+$, clearly we must have $\alpha_i = 0$ for $i > 1$ which shows that $a_2 = \lambda a_1$ where $\lambda = \frac{\alpha_1}{||a_1||_2}$.  Furthermore, this gives $\alpha_1 c_1 \le 1, \forall c_1 \le \frac{1}{||a_1||_2}$, which immediately implies $\alpha_1 \le ||a_1||_2 \implies \lambda \le 1$.  We finish by observing that $\lim_{c_1 \to -\infty} \alpha_1 c_1 = \infty$ iff $\alpha_1 < 0$, so we must have $\alpha_1 \ge 0$, which implies that $\lambda \in [0, 1]$.

\item For $x = (x_1, \ldots, x_n) \in \R^n$, denote by $s_{[i]}$ the $i$-th largest entry of $x$.  Let $C$ be a subset of $\Rn$ defined as $C = \{x \in \Rn: x_{[1]} + x_{[2]} \le 1\}$.  Decide whether $C$ is convex.

\textbf{Solution}  Consider that, for $x \in \Rn$, we have $x_{[1]} + x_{[2]} \le 1$ if and only if $x_i + x_j \le 1$ for every pair $i, j \in \{1, \ldots, n\}, i \neq j$.  Therefore \[C = \bigcap_{i \neq j} \{x : x_i + x_j \le 1\} = \bigcap_{i \neq j} \{x : (e_i + e_j)^T x \le 1\},\] where $e_i$ is the $i$-th column of the identity matrix.  Therefore, $C$ is a finite intersection of half-spaces; as half-spaces are always convex and arbitrary intersections of convex sets are convex, $C$ is convex.

\item Let $A \in \mathcal{S}_+^n, b \in \Rn, c \in \R$.  Show that the set \[F = \{x \in \Rn : x^T A x + b^T x + c \le 0\}\] is a spectrahedron.

\textbf{Solution}  $A \in \psdn$, so there is some $B \in \psdn$ such that $A = B^T B$.  Now we consider that, for any $x \in \Rn$, \[x^T A x + b^T x + c \le 0 \iff (-c - b^T x) - x^T A x = (-c - b^T x) - (Bx)^T I^{-1} (Bx) \ge 0.\]  By the Schur's Complement criterion, this inequality is satisfied if and only if the matrix \[\begin{bmatrix} I & Bx \\ (Bx)^T & -c - b^T x \end{bmatrix} \suppeq 0,\] giving that \[F = \{x \in \Rn : \begin{bmatrix} I & Bx \\ (Bx)^T & -c - b^T x \end{bmatrix} \suppeq 0\},\] which is a spectrahedron, as all the entries of the matrix in the inequality are affine functions of $x$.

\item Let $C$ be a subset of $\R^5$ defined as \[C = \{(x_1, x_2, x_3, x_4, t) \in \R_+^4 \times \R : x_1 \cdot x_2 \cdot x_3 \cdot x_4 \ge t^4\}.\]  Show that $C$ is a convex set.

\textbf{Solution}  Consider the set \[D = \{(x_1, \ldots, x_4, s_1, s_2, t) \in \R^6_+ \times \R : x_1 x_2 \ge s_1^2, x_3 x_4 \ge s_2^2, s_1 s_2 \ge t^2\}.\]  Then $D$ is a spectrahedron in $\R^7$ intersected with $\R^6_+ \times \R$, as $x_1 x_2 \ge s_1^2 \iff \begin{bmatrix} x_1 & s_1 \\ s_1 & x_2 \end{bmatrix} \suppeq 0 \ \text{and} \ x_1, x_2 \ge 0$; the other two inequalities may be formulated similarly.  We now show that $C = P(D)$ where $P$ is the projection \[P\left(\begin{bmatrix} x_1 \\ \vdots \\ x_4 \\ s_1 \\ s_2 \\ t \end{bmatrix} \right) = \begin{bmatrix} x_1 \\ \vdots \\ x_4 \\ t \end{bmatrix} \right).  \]  Clearly $P(D) \subeq C$, as any element of $D$ satisfies $x_1 x_2 x_3 x_4 \ge s_1^2 s_2^2 \ge t^4$.  To show $C \subeq P(D)$, we merely observe that, for $(x_1, \ldots, x_4, t) \in C$, we have $(x_1, x_2, x_3, x_4, \sqrt{x_1 x_2}, \sqrt{x_3 x_4}, t) \in D$, which completes the proof!

\item Show that the set \[F = \{X \in \psd^2 : \det(X) \ge 1\}\] is convex.

\textbf{Solution}  For $X = \begin{bmatrix} a & b \\ b & c \end{bmatrix} \in \psd^2$, we have $X \in F$ iff \[\det(X) = ac - b^2 \ge 1 \iff ac \ge 1 + b^2  = \begin{bmatrix} 1 & b\ \end{bmatrix} \begin{bmatrix} 1 \\ b \end{bmatrix} \iff c \ge \begin{bmatrix} 1 & b\ \end{bmatrix} (1/a I) \begin{bmatrix} 1 \\ b \end{bmatrix} = \begin{bmatrix} 1 & b\ \end{bmatrix} (a I)^-1 \begin{bmatrix} 1 \\ b \end{bmatrix};\] we then use the Schur's complement criterion to show that $X \in F$ iff \[\begin{bmatrix} a & 0 & 1 \\ 0 & a & b \\ 1 & b & c \end{bmatrix} \suppeq 0,\] which shows that $F$ is a spectrahedron!

\item Let $\mathcal{E}_1 = \{x \in \Rn : x^T A^{-1} x \le 1\}$ and $\mathcal{E}_2 = \{x \in \Rn : x^T B^{-1} x \le 1\}$ be two ellipsoids with $A, B \in \spdn$.  If $\mathcal{E}_1 \subset \mathcal{E}_2$, show that $B - A \suppeq 0$.

\textbf{Solution}  Assume $\mathcal{E}_1 \subset \mathcal{E}_2$; then 


\end{document}
