%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\renewcommand{\S}{\ensuremath{\mathcal{S}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathcal{F}}}
\newcommand{\mg}{martingale}
\newcommand{\E}{\ensuremath{\mathbb{E}}}
\newcommand{\rv}{random variable}
\newcommand{\psd}{\ensuremath{\mathcal{S}_+}}
\newcommand{\psdn}{\ensuremath{\mathcal{S}_+^n}}
\newcommand{\Tr}{\mathrm{Tr}}
\newcommand{\conv}{\ensuremath{\mathrm{conv}}}
\newcommand{\cone}{\ensuremath{\mathrm{cone}}}
\newcommand{\aff}{\ensuremath{\mathrm{aff}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{Math 245A Homework Assignment \#1}
\author{Instructor:   Jiawang Nie}
\date{\textbf{Due date}:  October 14, 2016}

\begin{document}
\maketitle

\textbf{Problem 1} Show that the set $$K = \left\{\frac{X}{1 + \Tr(X)} : I \preceq X \preceq 2I\right\}$$ is convex.  Is it a spectrahedron?

\textbf{Solution}  For $X \in K$, set $$Y = \frac{X}{1 + \Tr(X)}.$$  We find that $\Tr(Y) = \Tr(X) / (1 + \Tr(X))$, so that $\Tr(X) = \Tr(Y) / (1 - \Tr(Y))$, giving $$X = \frac{Y}{1 - \Tr(Y)}.$$  Therefore, $$K = \left\{Y : I \preceq \frac{Y}{1 - \Tr(Y)} \preceq 2I\right\} = \left\{Y : (1 - \Tr(Y))I \preceq Y \preceq (1 - \Tr(Y)) 2 I \right\},$$ which is indeed a spectrahedron and therefore convex.

\textbf{Problem 2}  Show that the set $$K = \left\{ (X, Y) \in \S^n \times \S^n_+ : X^2 \preceq \Tr(Y) Y \right\}$$ is a spectrahedron.

\textbf{Solution}  Setting $$C = \left\{ (X, Y) \in \S^n \times \S^n_+ : \begin{bmatrix} \Tr(Y) I & X \\ X & Y \end{bmatrix} \succeq 0 \right\},$$ we claim $K = C$, which is a spectrahedron.  Observe that, when $Y \neq 0$, the Schur's Complement criterion gives that $$\begin{bmatrix} \Tr(Y) I & X \\ X & Y \end{bmatrix} \succeq 0 \iff X^T (\Tr(Y) I)^{-1} X \preceq Y \iff X^T X = X^2 \preceq \Tr(Y) Y,$$ so that $K \cap \{Y \neq 0\} = C \cap \{Y \neq 0\}$.  We now need only prove that $K \cap \{Y = 0\} = C \cap \{Y = 0\}$.  Clearly, though, for $(X, Y) \in K, Y = 0$ implies $X = 0$, as $X^2$ has nonnegative real eigenvalues for any $X \in \S^n$.  Therefore, it suffices to show $$M := \begin{bmatrix} 0 & X \\ X & 0 \end{bmatrix} \succeq 0 \iff X = 0.$$  Indeed, if $Xv = \lambda v$, we have $$M \begin{bmatrix} v \\ v \end{bmatrix} = \lambda \begin{bmatrix} v \\ v \end{bmatrix} \ \text{and} \ M \begin{bmatrix} v \\ -v \end{bmatrix} = - \lambda \begin{bmatrix} v \\ -v \end{bmatrix},$$ meaning that $\lambda$ and $-\lambda$ are eigenvalues of $M$; to have $M \succeq 0$, it must be that $\lambda = 0$ for every eigenvalue of $X$, namely $X = 0$.  This completes the proof!

\textbf{Problem 3}  Let $X$ be the set $$X = \left\{ \begin{bmatrix} 1 \\ t \\ t^2 \end{bmatrix} : t \in \R \right\}.$$  Find the convex hull $\conv(X)$ and show that it is a spectrahedron.

\textbf{Solution}  We set $$C = \left\{ \begin{bmatrix} 1 \\ s \\ t \end{bmatrix} : t \ge s^2 \right\}$$ and claim $\conv(X) = C$.  Clearly $\conv(X) \subset C$, as $C$ is convex (being an affine embedding of the epigraph of $f(x) = x^2$ into $\R^3$) and $X \subset C$.  To show $C \subset \conv(X)$, we fix $(1, s, t) \in C$ and write it as a convex combination of elements of $X$; namely, taking $x_1 = (1, -\sqrt{t}, t), x_2 = (1, \sqrt{t}, t)$ where $x_1, x_2 \in X$, we find $$(1, s, t) = \frac{t^{1/2} - s}{2t^{1/2}}x_1 + \frac{t^{1/2} + s}{2t^{1/2}}x_2.$$  This proves the claim.

To show that $C$ is a spectrahedron, we need only observe that $$t \ge s^2 \iff \begin{bmatrix} t & s \\ s & 1 \end{bmatrix} \succeq 0, \ \text{so} \ C = \left\{ \begin{bmatrix} 1 \\ s \\ t \end{bmatrix} : \begin{bmatrix} t & s \\ s & 1 \end{bmatrix} \succeq 0\right\}.$$

\textbf{Problem 4}  Let $X$ be the set $$X = \{(x, t) \in \Rn \times \R_+ : ||x||_2 = t\}.$$  Show that $\conv(X) = \mathcal{L}_n$, the second order cone $$\mathcal{L}_n = \{(x, t) \in \Rn \times \R_+ : ||x||_2 \le t\}.$$

\textbf{Solution}  The containment $\conv(X) \subset \mathcal{L}_n$ is clear, as the perspective of $\mathcal{L}_n$ is the unit ball, which is convex.  To show $\mathcal{L}_n \subset \conv(X)$, we consider $(x, t) \in \mathcal{L}_n$; if $x = 0$, then $(x, t) = 1/2 (te_1, t) + 1/2 (-te_1, t)$.  Else, consider that $(u, t), (-u, t) \in X$ where $u = tx / ||x||_2$, so that $$(x, t) = \frac{t - ||x||_2}{2t} (-u, t) + \frac{t + ||x||_2}{2t} (u, t).$$  This completes the proof.

\textbf{Problem 5}  Let $$M = \left\{ X \in \psdn : \rank(X) = 2, \Tr(X) = 1\right\}.$$  Find $\conv(M), \aff(M),$ and $\cone(M)$.

\textbf{Solution}  For $\conv(M)$, set $C_1 = \{X \in \psdn : \rank(X) \ge 2, \Tr(X) = 1\}$; we claim that $\conv(M) = C_1$.  To show $\conv(M) \subset C_1$, it suffices to prove that $C_1$ is convex.  In turn, it suffices to prove that $\rank(X + Y) \ge \rank(X) + \rank(Y)$ for any $X, Y \in \psdn$ (as the convexity of $\{\Tr(X) = 1\}$ is trivial).  Indeed, if $z \in \Nul(X + Y)$, we have $z^T X z + z^T Y z = 0$, which implies $z \in \Nul(X) \cap \Nul(Y)$ when $X, Y \in \psdn$.  Therefore $\Nul(X + Y) = \Nul(X) \cap \Nul(Y)$ for $X, Y \in \psdn$, so indeed $\rank(X + Y) \ge \rank(X) + \rank(Y)$.

To show $C_1 \subset \conv(M)$, we take $X = \sum_{i = 1}^r \lambda_i u_i u_i^T, \sum_i \lambda_i = 1, r \ge 2$ to be an element of $C_1$, (decomposed into orthonormal eigenvectors).  We now set $$X_i = \frac{r - 1}{\lambda_1 + (r - 1)\lambda_{i + 1}}(\frac{\lambda_1}{r - 1} u_1 u_1^T + \lambda_{i + 1} u_{i + 1} u_{i + 1}^T)$$ for $i = 1, \ldots, r - 1$.  We set $c_i = \frac{\lambda_1 + (r - 1)\lambda_{i + 1}}{r - 1}$; then $\Tr X_i = 1, \rank(X_i) = 2$ for each $i$ and $\sum_{i = 1}^{r - 1} c_i X_i = X$.  This completes the proof, as $\sum_i c_i = 1$ and $c_i \ge 0$.

For $\aff(M)$, we set $C_2 = \{X \in \S^n : \Tr(X) = 1\}$ and claim $\aff(M) = C_2$.  First, we note that $\aff(M) = \aff(\conv(M))$, so it suffices to prove $\aff(C_1) = C_2$.  Furthermore, if $X \in \aff(M)$ has $X = \sum_i^k \lambda_i X_i$ for $X_i \in M, \sum_i \lambda_i = 1$, then $Q X Q^T = \sum_i^k \lambda_i Q X_i Q^T$ for any orthogonal matrix $Q \in O(n)$; as rank and trace are preserved by similarity transformations, this shows that $X \in \aff(M) \iff Q X Q^T \in \aff(M)$ for some $Q \in O(n)$.  In particular, it suffices to prove $\{D : D = \diag(c_1, \ldots, c_n), \sum_{i = 1} c_i = 1\} \subset \aff(M)$ to prove $C_2 \subset \aff(M)$.

Furthermore, it suffices to prove that $e_i e_i^T =: E_i \in \aff(M)$ for each $i$, as $\diag(c_1, \ldots, c_n) = \sum_i c_i E_i$ is an affine combination of these matrices.  To that end, we observe that indeed (taking $j$ to be any index not equal to $i$), $E_i = 3 (2/3 E_i + 1/3 E_j) - 2(1/2 E_i + 1/2 E_j)$, which is an affine combination of elements of $M$.  This completes the proof!

For $\cone(M)$, we set $$C_3 = \{X \in \psdn : \rank(X) \neq 1\} = \{X \in \psdn : \rank(X) \ge 2\} \cup \{0\}$$ and claim $\cone(M) = C_3$.  Since $\cone(M) = \cone(\conv(M))$, it suffices to show that $C_3 = \cup_{t \ge 0} \conv(M)$.  $C_3$ is trivially conic, so $\cone(M) \subset C_3$ is clear.  To show $C_3 \subset \cup_{t \ge 0} \conv(M)$, we consider $X \in C_3$ with trace $\Tr(X) = t$.  If $t = 0$, clearly $X = 0 \cdot Y$ for any $Y \in \conv(M)$.  If $t > 0$, then $\rank(X) \ge 2$ (by definition of $C_3$), so $X / t \in \conv(M)$.  This completes the proof.


\end{document}
