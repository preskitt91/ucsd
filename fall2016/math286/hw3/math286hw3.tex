%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathcal{F}}}
\newcommand{\mg}{martingale}
\newcommand{\E}{\ensuremath{\mathbb{E}}}
\newcommand{\rv}{random variable}
\newcommand{\N}{\ensuremath{\mathbb{N}}}


%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{Math 286A, Homework \#3}
\author{Brian Preskitt}
\date{\today}

\begin{document}
\maketitle

\newcommand{\dx}{\Delta^{(X)_n}}
\newcommand{\dm}{\Delta^{(M)_n}}
\newcommand{\dv}{\Delta^{(V)_n}}

\textbf{Problem 4.3}  Suppose $X = M + V$ is a continuous semimartingale, where $M$ is a continuous local martingale and $V$ is a continuous process of locally bounded variation.  For a sequence of partitions $\{\pi_n^t\}$ of $[0, t]$ with $\lim_{n \to \infty} \max_j t_{j + 1}^n - t_j^n = 0$, show that $S_t^n \to [M]_t$ in probability, where \[S_t^n = \sum_j (X_{t_{j+1}^n} - X_{t_j^n})^2.\]

\textbf{Solution}  Before proceeding, we assign the notations $\dx_j = X_{t_{j + 1}^n} - X_{t_j^n}$ and similarly for $\dm_j$ and $\dv_j$.  Then we have \begin{align*} S_t^n &= \sum_j (\dx_j)^2 = \sum_j (\dm_j + \dv_j)^2 \\ &= \sum_j (\dm_j)^2 + (\dv_j)^2 + 2 \dm_j \dv_j. \end{align*}  By Theorem 4.1(ii) in \cite{introStochProc}, we have that $\sum_j (\dm_j)^2 \to [M]_t$ in probability, so it suffices to prove that $\sum_j (\dv_j)^2 + 2 \dm_j \dv_j \to 0$ in probability.

For $\sum_j (\dv_j)^2$, we write \[0 \le \sum_j (\dv_j)^2 \le \sup_j |\dv_j| \sum_j |\dv_j|.\]  We then consider the events $\{|V|_t \le k\}$ for $k \in \Z$, where $|V|_t$ is the variation of $V$ over $[0, t]$ (as defined in \S 1.3 of \cite{introStochProc}) to oberve that \[1_{\{|V|_t \le k\}} \sum_j |\dv_j|^2 \le 1_{\{|V|_t \le k\}} \sup_j |\dv_j| \sum_j |\dv_j| \le 1_{\{|V|_t \le k\}} k \sup_j |\dv_j| \to 0\] almost surely, as continuity of $V$ gives uniform continuity on $[0, t]$, which in turn gives $\sup_j |\dv_j| \to 0$.  As $V$ is locally of bounded variation, we have $\Pr(\bigcup_k \{|V|_t \le k\}) = 1$, which shows that $\sum_j (\dv_j)^2 \to 0$ in probability.

By a similar argument, \[|\sum_j \dm_j \dv_j| \le \sup_j |\dm_j| \sum_j |\dv_j|,\] so we take a localizing sequence of optional times $\tau_\ell = \inf \{t : |M_t| \le \ell\}$ for $M$ and observe \[\begin{array}{rcl} 1_{\{\tau_\ell \ge t\}} 1_{\{|V|_t \le k\}} \sum_j \dm_j \dv_j & \le & 1_{\{\tau_\ell \ge t\}} 1_{\{|V|_t \le k\}} \sup_j |\dm_j| \sum_j |\dv_j| \\ & \le & 1_{\{\tau_\ell \ge t\}} 1_{\{|V|_t \le k\}} k \sup_j |\dm_j|, \end{array}\] but because $|M|$ is bounded by $\ell$ on $[0, t]$ on the event $\{\tau_\ell \ge t\}$, $M$ is also uniformly continuous on $[0, t]$, so the last term in this equality converges to $0$ almost surely.  Again because $\Pr(\bigcup_\ell \{\tau_\ell \ge t\}) = \Pr(\bigcup_k \{|V|_t \le k\}) = 1$, this gives that $\sum_j \dm_j \dv_j \to 0$ in probability, which completes the proof!

\textbf{Problem 5.8}  Let $-\infty < a < b < \infty$ and let $B$ be a one-dimensional Brownian motion starting at $x \in (a, b)$.  Let $P^x$ and $E^x$ denote the associated probability and expectation.  Further, let $\tau = \inf \{t : B_t \notin (a, b)\}$.  Show that $E^x[\tau] < \infty$ and $P^x(\tau < \infty) = 1$.

\textbf{Solution}  First, without loss of generality, we assume $x = 0$ by translating $(a, b)$ and $B$; in particular, we observe that now $-\infty < a < 0 < b < \infty$.  Next, we consider that $B_t^2 - t$ is a continuous martingale and let $\eta = \inf \{t : |B_t| \ge M\}$ be a sequence of optional stopping times for $B_t^2 - t$, where $M = \max\{|a|, |b|\}$.  By Doob's stopping theorem, we have that $E^x[B_\eta^2 - \eta] = E^x[B_0^2 - 0] = 0$, such that $E^x[B_\eta^2] = E^x[\eta]$.  However, $E^x[B_\eta^2] = M^2$, so $E^x[\eta] = M^2$.  Considering that $\tau \le \eta$, we have that $E^x[\tau] \le M^2 < \infty$, which completes the proof.

$P^x(\tau < \infty) = 1$ immediately follows by considering that $E^x[\tau] \le M^2$ implies that $P^x(\tau \ge k M^2) \le 1/k$ for any $k \in \N$, so $P^x(\tau = \infty) = P^x(\bigcap_k \tau \ge k M^2) = 0$.

\bibliographystyle{plain}
\bibliography{hw3}

\end{document}
