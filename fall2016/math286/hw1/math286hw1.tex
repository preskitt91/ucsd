%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathcal{F}}}
\newcommand{\mg}{martingale}
\newcommand{\E}{\ensuremath{\mathbb{E}}}
\newcommand{\rv}{random variable}


%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{Math 286A, Homework \#1}
\author{Brian Preskitt}
\date{\today}

\begin{document}
\maketitle

\textbf{Exercise 1.11.4}  Verify that when $B_0 \in L^p, p \ge 1, \{B_t, \F_t, t \ge 0\}$ is an $L^p$ \mg\ and $\{B_t^2 - t, \F_t, t \ge 0\}$ is an $L^{p/2}$ \mg\ for $p \ge 2$.

\textbf{Proof}  Assume that $B_0 \in L^p$; then \[\E[|B_t|^p]^{1/p} \le \E[|B_t - B_0|^p]^{1/p} + \E[|B_0|^p]^{1/p};\] the second term here is bounded by hypothesis, and the first is bounded because $B_t - B_0$ is a normally distributed \rv\ with variance $t$, which has all finite moments.

For the \mg\ property, we observe that \[\E[B_t | \F_s] = \E[B_s + (B_t - B_s) | \F_s] = B_s + 0 = B_s,\] where $\E[B_t - B_s | \F_s]$ comes from the property that increments of a Brownian are normally distributed with mean $0$.  Since we additionally showed that $B_t \in L^p$ for each $t$, it follows that $B_t$ is an $L^p$ \mg!  (Also, Brownian motions are always assumed to be realized with a continuous version).

For $B_t^2 - t$, conisder that \[\E[|B_t^2 - t|^{p/2}]^{2/p} \le \E[|B_t^2 - B_0^2|^{p/2}]^{2/p} + \E[|B_0^2 - t|^{p/2}]^{2/p} \le \E[|B_t^2 - B_0^2|^{p/2}]^{2/p} + \E[|B_0|^p]^{2/p} - t.\]  In this last expression, the second term is bounded by hypothesis.  To bound the first, we observe that $(B_t^2 - B_0^2) = (B_t - B_0)^2 + 2B_0(B_t - B_0)$, such that \[\E[|B_t^2 - B_0^2|^{p/2}]^{2/p} \le \E[(|B_t - B_0|^2)^{p/2}]^{2/p} + 2\E[|B_0(B_t - B_0)|^{p/2}].\]  The first term is just the square of the $p$th moment of a Gaussian \rv, so it is finite, whereas the second expectation contains two independent events.  Therefore, we may take \[\E[|B_0(B_t - B_0)|^{p/2} = \E[|B_0|^{p/2}]\E[|B_t - B_0|^{p/2}].\]  Since $L^p$ spaces are nested over a probability space, the first term is finite because $B_0 \in L^p$, whereas the second is finite because it is normally distributed.

To obtain the \mg\ property, we observe \[%
\begin{array}{rcll}
\E[B_t^2 - t | \F_s] & = & \E[B_t^2 - B_s^2 + B_s^2 - s + s - t | \F_s] \\
& = & \E[B_t^2 - B_s^2 | \F_s] + (B_s^2 - s) + (s - t) & (B_s \ \text{is determined at this point}) \\
& = & \E[(B_t - B_s)^2 | \F_s] + 2 \E[B_s(B_t - B_s) | \F_s] + (B_s^2 - s) + (s - t) \\
& = & (t - s) + 2 \cdot 0 + (B_s^2 - s) + (s - t) & (\text{independence;} \ \E[B_t - B_s] = 0) \\
& = & B_s^2 - s,
\end{array}\] which completes the proof!

\textbf{Exercise 1.11.5} Let $\tau$ be an exponentially distributed \rv\ with parameter $\lambda$ and let $X_t$ be the \rv\ given by $X_t = \ee^{\lambda t}1_{\tau < t}$.  Show that $X_t$ is a positive \mg\ but is not uniformly integrable.  Set $X_{0-} = X_0$ and show that $X_{\tau-} \notin L^1$.

\textbf{Solution}  $X_t$ is trivially positive, and because the possible values of $X_t$ are bounded for any fixed $t$, we have $X_t \in L^1$ for each fixed $t$.  To check the \mg\ property, we begin by taking $t > s$ and observing that there are two outcomes for $X_t$: $0$ if $\tau \le t$ and $\ee^{\lambda t}$ otherwise.  Conditioning on $\F_s$ is equivalent to knowing whether $\tau > s$ or not, so we may check the \mg\ property for $\E[X_t | \F_s, \tau \le s]$ and $\E[X_t | \F_s, \tau > s]$ separately.

Indeed, $\E[X_t | \F_s, \tau \le s] = 0 = X_s 1_{\tau \le s}$, since $X_t$ and $X_s$ can only be non-zero if $\tau > s$.  On the other hand, \[\E[X_t | \F_s, \tau > s] = \ee^{\lambda t}P(\tau > t | \tau > s) = \ee^{\lambda t}P(\tau > t - s) = \ee^{\lambda t}\ee^{-\lambda(t - s)} = \ee^{\lambda s},\]  where the second equality comes from the Markov property of the exponential distribution.  This completes the proof when we observe that, when $\tau > s$, we have $X_s = \ee^{\lambda s}$.

To show that $X$ is not uniformly integrable, we consider that \[\E[X_t] = \E[X_0] = 1\] for any $t \ge 0$ by the \mg\ property, and that $X_t \cdot 1_{X_t > \ee^{\lambda t} - 1} = X_t$; therefore \[\lim_{t \to \infty} \E[|X_t|; |X_t| > \ee^{\lambda t} - 1] = \lim_{t \to \infty} \E[|X_t|] = 1,\] which violates the definition of uniform integrability.

As for $X_{\tau-}$, consider that \[\E^\tau[X_{\tau-}] = \int_0^\infty \ee^{\lambda \tau} \cdot \ee^{-\lambda \tau} \, d\tau = \lim_{b \to \infty} \int_0^b 1 \, d\tau = \lim_{b \to \infty} b = +\infty,\] so $X_{\tau-} \notin L^1$.


\end{document}
