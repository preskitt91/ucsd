%-----------------------------------------------------------------------
% Beginning of article-template.tex
%-----------------------------------------------------------------------
%
%    This is a template file for proceedings articles prepared with AMS
%    author packages, for use with AMS-LaTeX.
%
%    Templates for various common text, math and figure elements are
%    given following the \end{document} line.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%    Remove any commented or uncommented macros you do not use.

%    Replace amsproc by the name of the author package.
\documentclass{amsproc}

%    If you need symbols beyond the basic set, uncomment this command.
%\usepackage{amssymb}

%    If your article includes graphics, uncomment this command.
%\usepackage{graphicx}

%    If the article includes commutative diagrams, ...
%\usepackage[cmtip,all]{xy}

%    Include other referenced packages here.
\usepackage{bbm}

%    Update the information and uncomment if AMS is not the copyright
%    holder.
%\copyrightinfo{2009}{American Mathematical Society}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{xca}[theorem]{Exercise}

\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}

\numberwithin{equation}{section}

\setlength{\parindent}{0pt}
\setlength{\parskip}{10pt}

\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\x}{\ensuremath{\mathbf{x}}}
\newcommand{\sgn}{\ensuremath{\mathrm{sgn}}}
\renewcommand{\u}{\ensuremath{\mathbf{u}}}
\renewcommand{\S}{\ensuremath{\mathbb{S}}}

\begin{document}


\maketitle

%    Text of article.

%    Bibliographies can be prepared with BibTeX using amsplain,
%    amsalpha, or (for "historical" overviews) natbib style.
\bibliographystyle{amsplain}
%    Insert the bibliography data here.

In this week's section (of Monday, 5/2/2016), the question was raised: \begin{center} \emph{What is the geometric interpretation of forms?} \end{center}  The response I gave was something to the effect of \begin{center} \emph{Forms give us a language with which we may speak of many geometric phenomena in Euclidean space,} \end{center} which is certainly a vague answer!  I tried to give an example of this, namely by sharing how one can use forms to calculate the surface area of a manifold; namely, I shared a claim I found that the following form measures surface area:

\textbf{Claim}  Let $M \subset \Rn$ be an orientable, $(n-1)$-dimensional smooth manifold in \Rn, and let $\eta : M \to \S^{n-1}$ be a continuous mapping such that $\eta(\x) \perp T_{\x}M$ (that is, $\eta(\x)$ is a unit normal vector to $M$ at \x).  Then if $\omega$ is the $(n-1)$-form field on $M$ defined by \[\omega_{\x}(v_1, \ldots, v_{n-1}) = \det\left[\eta(\x)\ v_1\ \cdots \ v_{n-1}\right],\] then $\int_M \omega$ gives the surface area ($n-1$-dimensional volume) of $M$.

\textbf{Remark}  Consider that this form is defined up to a factor of $\pm 1$, as there are two continuous normal vector fields $\eta$; namely, if $\eta$ is a continuous normal vector field, then so is $-\eta$!  To resolve this ambiguity, we specify an orientation $\Omega$ of $M$ and choose $\eta$ such that $\Omega = \sgn\ \omega$, as on p.~578 of the text.

I tried to demonstrate this claim with an example: namely, we explicitly calculated $\eta$ and $\omega$ for the unit sphere $\S^2 \subset \R^3$, but this is hardly a proof that this form does what it promises, and certainly gave little intuition as to ``why.''  At any rate, the proof that $\omega$ is indeed the $(n-1)$-dimensional volume form in $\Rn$ is pretty quick!  We will show that $\omega$ coincides with \textbf{Definition 5.3.1} from the textbook (p. 533), the volume of a parametrized manifold.  Considering from \textbf{Theorem 5.2.8} (p. 530) that every manifold can be parametrized, we will conclude that $\omega$ correctly expresses the $(n-1)$-dimensional volume of a manifold in terms of the integral of a form! 

\textbf{Proposition}  Given an $(n-1)$-dimensional, orientable, smooth manifold $M \subset \Rn$, an orientation-preserving (p.~584 ) parametrization $\gamma : U \to \Rn$ of $M$, the mapping $\eta$ defined above, and the form $\omega$ defined above, then \[\int_M \omega = \int_{U} \sqrt{\det\left([D\gamma(\mathbf{u})]^T[D\gamma(\mathbf{u})]\right)}\ |d\mathbf{u}^{n-1}|.\]

\textbf{Proof}  According to \textbf{Definition 6.4.12} (p.~592 ), we have \[\begin{array}{rcl}
\int_M \omega & = & \int_U \omega\left(P_{\gamma(\u)}\left(D_1\gamma(\u), \ldots, D_{n-1}\gamma(\u)\right)\right)\ |d^{n-1}\u| \\[7pt]
& = & \int_U \det\left[\eta(\gamma(\u)) \ D_1\gamma \ \cdots \ D_{n-1} \gamma \right]\ |d^{n-1}\u|.
\end{array}\]  We now consider that $D_j\gamma(\u) \in T_{\gamma(\u)} M$ for every $\u \in U, j = 1, \ldots, n-1$.  Indeed, this is equivalent to the definition of $T_{\gamma(\u)}M$.  In particular, we have $D_j\gamma(\u)^T \eta(\gamma(\u)) = 0$, so, setting $A(\u) = \left[\eta(\gamma(\u)) \ D_1 \gamma \ \cdots \ D_{n-1} \gamma \right]$ and recalling that we oriented $\omega$ and $\gamma$ such that $\det A(\u) > 0$, we have \[\begin{array}{r@{=}l}%
\det A(\u) & \sqrt{\det(A(\u)^T) \det(A(\u))} \\[10pt]
 & \sqrt{\det\left(\begin{bmatrix}\eta^T \\ D_1^T \\ \vdots \\ D_{n-1}^T \end{bmatrix} \begin{bmatrix} \eta & D_1 & \cdots & D_{n-1} \end{bmatrix} \right)} \\[35pt]
& \sqrt{\det \left( \begin{bmatrix} 1 & 0 \\ 0 & D\gamma^T D\gamma \end{bmatrix} \right)} \\[15pt]
& \sqrt{\det\left(D\gamma^T D\gamma\right)}.
\end{array}
\]  Combining with the previous calculation gives \[\int_M \omega = \int_U \sqrt{\det\left(D\gamma(\u)^T D\gamma(u)\right)}\ |d^{n-1}\u|\] as promised! \qed

Obviously, the key part of this last proof is that we can somehow get $\sqrt{\det(D\gamma^T D\gamma)}$, the volume element from the parametrization, from the form $\omega$.  This proof technique easily lends itself to devising a $k$-dimensional volume form for any $k$, not just $k = n-1$!  In order to state this form concisely, we introduce the following term:

\textbf{Definition}  Given a $k$-form field $\phi : M \to A_c^k(\Rn)$ and a vector field $\chi : M \to TM$, the \textbf{contraction} of $\phi$ with $\chi$ is the $(k-1)$-form field $\iota_{\chi}\phi$ defined by \[\iota_{\chi}\phi(\x)(v_1, \ldots, v_{k-1}) = \phi(\chi(\x), v_1, \ldots, v_{k-1}).\]  In other words, the contraction of a form $\phi$ with a vector field $\chi$ is just $\phi$ with the first argument always chosen to be $\chi$!

With this notation, it is clear that if $\nu \in A^n(\Rn)$ is the basic volume form (namely $\nu(v_1, \ldots, v_n) = \det[v_1 \ \cdots \ v_n]$), then \[\omega = \iota_{\eta}\nu.\]  Following this example, I will leave it as an exercise for those interested that if $(\eta_1, \ldots, \eta_{n-k}) : M \to TM^{\perp}$ is an appropriately oriented, continuous collection of maps such that $\{\eta_i(\x)\}_{i=1}^{n-k}$ forms an orthonormal basis for $T_\x M^\perp$ (the orthogonal complement of the tangent space) at each $\x \in M$, then the $k$-dimensional volume of $M$ may be found by taking $\omega^{(k)} = \iota_{\eta_{n-k}}\cdots\iota_{\eta_1}\nu$, i.e.~ \[\omega^{(k)}(\x)(v_1, \ldots, v_k) = \det\left[\eta_1 \ \cdots \ \eta_{n-k} \ v_1 \ \cdots \ v_k\right],\] and integrating \[\mathrm{vol}_kM = \int_M \omega^{(k)}.\]  The proof comes easily by following the technique used above.

Enjoy!



\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%    Templates for common elements of an article; for additional
%    information, see the AMS-LaTeX instructions manual, instr-l.pdf,
%    included in every AMS author package, and the amsthm user's guide,
%    linked from http://www.ams.org/tex/amslatex.html .

%    Section headings
\section{}
\subsection{}

%    Ordinary theorem and proof
\begin{theorem}[Optional addition to theorem head]
% text of theorem
\end{theorem}

\begin{proof}[Optional replacement proof heading]
% text of proof
\end{proof}

%    Figure insertion; default placement is top; if the figure occupies
%    more than 75% of a page, the [p] option should be specified.
\begin{figure}
\includegraphics{filename}
\caption{text of caption}
\label{}
\end{figure}

%    Mathematical displays; for additional information, see the amsmath
%    user's guide, linked from http://www.ams.org/tex/amslatex.html .

% Numbered equation
\begin{equation}
\end{equation}

% Unnumbered equation
\begin{equation*}
\end{equation*}

% Aligned equations
\begin{align}
  &  \\
  &
\end{align}

%-----------------------------------------------------------------------
% End of article-template.tex
%-----------------------------------------------------------------------
