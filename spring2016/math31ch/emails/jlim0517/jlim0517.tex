%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, wasysym}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\newcommand{\sgn}{\ensuremath{\mathrm{sgn}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{}
\author{}
\date{}

\begin{document}
\maketitle

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

Just to make sure I answer your question accurately, let's agree on the definition of terms:

\begin{description}
  \item[Space of Forms]  For $k, n \in \N, \Omega^k_n$ will denote the vector space of $C^1$, $k$-form fields acting on $\Rn$ (so $\omega \in \Omega^k_n$ means that, for every $x \in \Rn, \omega(x) : (\Rn)^k \to \R$ is a multilinear, antisymmetric map).
  \item[Mass Form]  Given a function $f : \R^3 \to \R$, define the differential form $M_f \in \Omega^3_3$ by \[M_f(x)(u, v, w) = f(x) \cdot \det\begin{bmatrix} u & v & w \end{bmatrix}. \]  $M_f$ is called the \textbf{mass form} of $f$.
    \item[Orientation given by a form]  Suppose $M \subset \Rn$ is a $k$-dimensional, orientable manifold.  Then given a form $\omega \in \Omega^k_n$, we may use $\omega$ to define an orientation on $M$ if, for every $x \in M$ and every basis $\{v_1, v_2\}$ for $T_xM$, $\omega(x)(v_1, v_2) \neq 0$ \ (that is, $\omega(x)$ must be non-vanishing on bases of $T_xM$).  In this case, the orientation $\Omega : \mathcal{B}_M \to \{-1, +1\}$ is defined by $\Omega(x; v_1, v_2) = \sgn\left(\omega(x)(v_1, v_2)\right)$.
\end{description}

The reason this last bit defines an orientation is because if $(u, v) : M \to (\Rn)^2$ is a continuous choice of bases for the tangent spaces of $M$ (meaning $(u(x), v(x))$ is an ordered basis for $T_xM$ at each $x \in M$), then $\omega(x)(u(x), v(x))$, viewed as a function solely of $x$ (so at each point on $M$, I evaluate $\omega(x)$ on the basis $(u(x), v(x))$ I've assigned to that point) is a \emph{continuous function}.  Since we've further assumed that $\omega$ never gives you zero for a basis of a tangent space of $M$, that means $\omega(x)(u(x), v(x))$ is either always positive or always negative.  Recalling that a continuous choice of bases for $TM$ is equivalent to choosing an orientation (because at each $T_xM$ you've chosen a ``direct'' basis), this shows that $\omega$ defines an orientation on $M$; in particular, it is the orientation that agrees with all continuous choice of bases for $TM$ for which $\omega(x)(u(x), v(x))$ is strictly positive.

All \emph{that} being said, if you want to test whether a parametrization agrees with the orientation of a form, just consider that a parametrization $\gamma : U \to M$ \emph{gives you} a continuous choice of basis for $TM$ --- namely, $\left(D\gamma_1(u), \ldots, D\gamma_k(u)\right)$ is a basis for $T_{\gamma(u)}M$, and $\gamma \in C^1$ so its derivatives are continuous.  Therefore, to check if a parametrization is orientation-preserving with respect to (the orientation defined by) a (non-vanishing) form, you need only take a single $u \in U$ and check $\sgn \omega(\gamma(u))\left(D\gamma_1(u), \ldots, D\gamma_k(u)\right)$.

In the case that $k = n = 3$ and $\omega = M_f$ (indeed, if $\omega \in \Omega_3^3,$ then it must be a mass form), to check if $\gamma$ is orientation preserving with respect to $\omega,$ just pick some $u \in U$ and evaluate $\omega(\gamma(u))(D\gamma) = f(\gamma(u)) \det(D\gamma)$.

One ``standard'' choice of mass form $\omega$ would indeed be $\omega(u, v, w) = \det[u \ v \ w]$; this would be the mass form $M_f$ where $f$ is the constant function $1$.  If the problem doesn't specifically define $f$ such that $\omega = M_f$, then assuming that this is the case should be very safe. As the intent of the mass form is to assign a density $f(x, y, z)$ to each point, and then find the mass of a manifold, $f = 1$ would be an unsurprising choice here, as it assigns every point a uniform density of $1 \ (\text{unit of mass} / \text{unit of length}^3)$.

For the record, $\omega$ will be non-vanishing on bases of $TM$ if and only if $f$ is non-vanishing on $M$ (as it is in the case where $f = 1$) \smiley

\end{document}

