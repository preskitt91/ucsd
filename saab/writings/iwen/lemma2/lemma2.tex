\documentclass{amsart}

%% This has a default type size 10pt.  Other options are 11pt and 12pt
%% This are set by replacing the command above by
%% \documentclass[11pt]{amsart}
%%
%% or
%%
%% \documentclass[12pt]{amsart}
%%

%%
%% Some mathematical symbols are not included in the basic LaTeX
%% package.  Uncommenting the following makes more commands
%% available. 
%%

\usepackage{amssymb}
\usepackage{amsmath}

%%
%% The following is commands are used for importing various types of
%% grapics.
%% 

\usepackage{epsfig}  		% For postscript
\usepackage{epic,eepic}       % For epic and eepic output from xfig

%%
%% The following is very useful in keeping track of labels while
%% writing.  The variant   \usepackage[notcite]{showkeys}
%% does not show the labels on the \cite commands.
%% 

\usepackage{showkeys}
%\usepackage{bbm}


%%%%
%%%% The next few commands set up the theorem type environments.
%%%% Here they are set up to be numbered section.number, but this can
%%%% be changed.
%%%%

\newtheorem{thm}{Theorem}[section]
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}

%%
%% If some other type is need, say conjectures, then it is constructed
%% by editing and uncommenting the following.
%%

%\newtheorem{conj}[thm]{Conjecture} 


%%% 
%%% The following gives definition type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 

\theoremstyle{definition}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}

%%
%% Again more of these can be added by uncommenting and editing the
%% following. 
%%

%\newtheorem{note}[thm]{Note}


%%% 
%%% The following gives remark type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 


\theoremstyle{remark}

\newtheorem{remark}[thm]{Remark}


%%%
%%% The following, if uncommented, numbers equations within sections.
%%% 

\numberwithin{equation}{section}


%%%
%%% The following show how to make definition (also called macros or
%%% abbreviations).  For example to use get a bold face R for use to
%%% name the real numbers the command is \mathbf{R}.  To save typing we
%%% can abbreviate as

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle
      #1}{\displaystyle #2}}}
\newcommand{\ii}{\ensuremath{\mathrm{i}}}

%%
%% The comment after the defintion is not required, but if you are
%% working with someone they will likely thank you for explaining your
%% definition.  
%%
%% Now add you own definitions:
%%

%%%
%%% Mathematical operators (things like sin and cos which are used as
%%% functions and have slightly different spacing when typeset than
%%% variables are defined as follows:
%%%

\DeclareMathOperator{\dist}{dist} % The distance.



%%
%% This is the end of the preamble.
%% 


\begin{document}

\title{Stability Lemma}
\author{Rayan and Brian}
\maketitle

  \textbf{Lemma 2} 
  Let $x \in \Cn, y \in \Rn$ be such that $\| |x| - y \|_2 \le M$.  Let
  also $\Phi \in \Cn$ be defined by 
  \[ \Phi_i = \bigfrac{x_i}{|x_i|} = e^{\ii \phi_i}, \]
  and let $X$ and $A$ be the matrices defined by


  \[ X_{ij} = \left\{
  \begin{array}{rl}
    x_ix_j^* & \text{if } |i-j| \mod n < \delta \\
    0 & \text{otherwise} \\
  \end{array}\right.\]

  \[ A_{ij} = \left\{
  \begin{array}{rl}
    e^{\ii (\phi_i - \phi_j)} & \text{if } |i-j| \mod n < \delta \\
    0 & \text{otherwise} \\
  \end{array}\right., \]

  so that $A = \bigfrac{X}{|X|},$ where $|\cdot|$ is the elementwise
  absolute value operator.  Let $\lambda_1 \ge \lambda_2 \ge \ldots
  \ge \lambda_n$ denote the eigenvalues of $A$.

  Suppose further that $a \in \Cn$ is a vector of approximate phases
  of $x$ obtained in the following way:
  
  Begin with a matrix $\tilde{X}$ such that $||X - \tilde{X}||_F \le
  c_1 ||X||_F$, then set $\tilde{A} =
  \bigfrac{\tilde{X}}{|\tilde{X}|}$ and take $a$ to be the top
  eigenvector of $A$ with norm $||a||_2 = \sqrt{n}$.  Also assume that
  the global phase of $a$ is such that $0 \le \langle a, \Phi \rangle
  \in \R$, and take $\eta$ such that $||A - \tilde{A}||_F\le\eta||A||_F$.

  Then there is a constant $C = C(M, n, \eta, \lambda_1-|\lambda_2|)$
  such that

  \[ ||x - y \circ a||_2 \le C||x||_2. \]

\begin{proof}
  Consider that 
  \[ \begin{array}{rcl}
    ||x - y\circ a||_2 & = & ||x - |x| \circ a + |x| \circ a - y \circ a
    ||_2 \\
    & \le & || |x| \circ (\Phi - a) ||_2 + || (|x| - y)\circ a||_2 \\
    & \le & ||a||_\infty || |x| - y ||_2 + \left(\sum_j |x_j|^2
    (\Phi_j - a_j)^2\right)^{1/2} \\
    & \le & ||a||_\infty M + ||x||_2 ||\Phi -a||_2
  \end{array} \]

  The $||a||_\infty$ term should be close to 1, but maybe that's
  uncertain so far...  On Tuesday, we were considering $a$ to be a set
  of phases, but if it's coming as the top eigenvector of $\tilde{A}$,
  then we can't yet assume that every entry is a phase.  At any rate,
  we'll continue working on the $||\Phi - a||_2$ term.

  According to Lemma 1 (one of the stability results Mark sent us), and
  considering that $\Phi$ is the top eigenvector of $A$, we have

  \[\begin{array}{rcl}
  ||\Phi - a||_2^2 & = & ||\Phi||_2^2 + ||a||_2^2 - 2 \langle a, \Phi
  \rangle \\
  & = & 2(n - \langle a, \Phi \rangle) \\
  & \le & 2\left(n - n\sqrt{1 - \bigfrac{4\eta(2\eta +
      1)||A||_F^2}{\lambda_1(\lambda_1 - |\lambda_2|)}}\right) \\
  & \le & 2n\left(\bigfrac{4\eta(2\eta +
    1)||A||_F^2}{\lambda_1(\lambda_1 - |\lambda_2|)}\right) \\
  \end{array}\]

  Using the fact that $||A||_F^2 = n(2\delta - 1) = n \lambda_1$, we
  have

  \[||\Phi - a||_2^2 \le \left(\bigfrac{8n^2\eta(2\eta +
    1)}{\lambda_1(\lambda_1 - |\lambda_2|)}\right).\]

  This stinks, because we'll need $\eta$ to decay like $1/n^2$.
\end{proof}

The other problem with this result is that right now, $\eta$ is the
\emph{normalized noise,} meaning that it will be worsened by small
entries in $\tilde{X}$.  One other approach we considered to answer
this was to partition the rows of $\tilde{X}$ according to size -- if
a row appears (we'll have to define ``appears'' later) to correspond
to a small entry of $x$, then we cut it out of $\tilde{A}$.  More specifically...

Take $\mathcal{E}\subset [n]$ to be the set of ``small'' rows of
$\tilde{A}$.  Set $p = |E|$ and let $k : [n - p] \to
[n]\setminus\mathcal{E}$ be a mapping that points to the ``large''
rows of $\tilde{A}$ (i.e. $k(i)$ is the $i^{\text{th}}$ remaining
row).  Then take:

\[ S = [e_{k(1)} \cdots e_{k(p)}] \]

Setting $\hat{A} = S^* A S$ makes $\hat{A}$ a copy of $A$ which has had the
small entries truncated out.  

Let $\hat{a}_i$ denote the $i^{\text{th}}$ row of $\hat{A}$.  Now set
$c : [p]\to\R$ by

\[c(i) = \bigfrac{2\delta - 1}{||\hat{a}||_2^2} =
\bigfrac{2\delta - 1}{\text{number of non-zero entries remaining in
    row } i}\]

Then if $C = \mathrm{diag}(c(1), \cdots, c(p))$, we found that the
eigenvalue lemmas for $A$ hold for $A' = C \hat{A}$ (what we've done
here is ``re-normalized'' rows of $\hat{A}$ such that the top
eigenvalue and eigenvector are still $2\delta - 1$ and $\Phi$),
provided that $k(i+1) - k(i) \le \delta$ (all arithmetic is $\mod n$),
namely that we never cut out $\delta$ consecutive row/columns of $A$.
In particular, we showed that $\lambda_1 = 2\delta - 1$ with
eigenvector $\Phi |_{[n]\setminus\mathcal{E}}$ and $\lambda_1 \neq \lambda_2$.

Our hope with this approach is to leverage the fact that the bigger
entries of $x$ should yield more accurate phases.

Our questions/remarks for Mark, at this point, are:

\begin{enumerate}
  \item What do we expect for $M$ in $|| y - |x| ||_2$?
\end{enumerate}

\end{document}
