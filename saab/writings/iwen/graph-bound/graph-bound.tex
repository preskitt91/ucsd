%\documentclass{assignment}
\documentclass{article}[10pt]
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[letterpaper, margin=1in]{geometry}

\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\newcommand{\sgn}{\ensuremath{\mathrm{sgn}}}
\newcommand{\vol}{\ensuremath{\mathrm{vol}}}
\renewcommand{\S}{\ensuremath{\mathbb{S}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{Graph Method Bound}
\author{}
\date{}

\begin{document}
\maketitle

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

\textbf{Acknowledgement}  This proof is a close adaptation of that offered in \cite{phasepol}.

\textbf{Terms and notation}
We begin with an undirected graph $G = (V, E)$ (with vertex set $V = \{1, 2, \ldots, |V|\}$) with weight mapping $p : V \times V \to \R$, where $p_{ij} = p_{ji}$ and $p_{ij} = 0$ iff $\{i, j\} \notin E$.  The \textbf{degree} of a vertex $i$ is \[\deg(i) = \sum_{(i, j) \in E} p_{ij}\] and we define the \textbf{degree matrix} and \textbf{adjacency matrix} of $G$ as \[D = \diag(\deg(i)) \ \text{and} \ A_{ij} = p_{ij}.\]  The \textbf{volume} of $G$ is \[\vol(G) = \sum_{i \in V} \deg(i).\]  The \textbf{Laplacian} of $G$ is the matrix \[L = I - D^{-1/2} A D^{-1/2} = D^{-1/2}(D - A)D^{-1/2}.\]  We consider that \[v^*(D - A)v = \sum_{i \in V} \left(v_i^2\deg(i) - \sum_{j \in V} v_i v_j p_{ij}\right) = \frac{1}{2} \sum_{i, j \in V} p_{ij} (v_i - v_j)^2.\]  Therefore, the nullspace of $(D - A)$ is $\Span(\mathbbm{1})$, and the nullspace of $L$ is $\Span(D^{1/2}\mathbbm{1})$.  Observing that $D - A$ is diagonally semi-dominant, it follows that $(D - A)$ and $L$ are both positive semidefinite; we then order the eigenvalues of $L$ in increasing order: $0 = \lambda_1 < \lambda_2 \le \cdots \le \lambda_n$.  We then define the \textbf{spectral gap} of $G$ to be $\tau = \lambda_2$.

In addition, we are given data in the form of a mapping $\rho : V \times V \to \S^1 \cup \{0\}$.  This is our measured relative phase matrix; that is, we assume there is some objective vector $x \in \C^{|V|}$ and that, for some (Hermitian) perturbations $\eta_{ij}$, we have \[\rho_{ij} = \bigfrac{x_i^*x_j + \eta_{ij}}{|x_i^*x_j + \eta_{ij}|} \cdot \chi_E(i, j).\]  In this instance and throughout this discussion, we consider $\frac{(\cdot)}{|\cdot|}$ to denote the function $\sgn : \C \to \S^1$ defined by \[\sgn(z) = \left\{\begin{array}{r@{,\ }l} z / |z| & z \neq 0 \\ 1 & z = 0\end{array}\right.\]

We then define the \textbf{connection Laplacian} of the graph $G$ conjoined with the relative phase data $\rho$ to be the matrix \[L_1 = I - D^{-1/2} A_1 D^{-1/2},\] where $(A_1)_{ij} = \rho_{ij}$.  We also define $A_0$ by \[(A_0)_{ij} = \bigfrac{x_i^*x_j}{|x_i^*x_j|} \cdot \chi_E(i, j).\]

Given $A_1$ and a vector $\omega : V \to \S^1$, we define the \textbf{frustration of} $\omega$ \textbf{with respect to} $A_1$ by \[\eta(\omega) = \bigfrac{1}{2}\bigfrac{\sum_{\{i, j\} \in E} p_{ij}|\omega_j - \rho_{ij} \omega_i |^2}{\sum_{i \in V} \deg(i) |\omega_i|^2} = \bigfrac{\omega^* (D - A_1) \omega}{\omega^* D \omega}.\]  We may consider $\eta(\omega)$ to measure how well $\omega$ fits the measured relative phases $\rho$.

We begin with a couple of lemmas:

\textbf{Lemma 1}  For any real numbers $a, b \in \R$, we have $\frac{1}{2} a^2 - b^2 \le (a - b)^2$.

\textbf{Proof}  Rearranging, we have $0 \le \frac{1}{2} a^2 + 2b^2 - 2 a b = \frac{1}{2}(a - 2b)^2$.

\textbf{Lemma 2} For any $a, b \in \C$ with $|a| = 1$, we have $|a - \sgn{b}| \le 22|a - b|$.

\textbf{Proof}  Well, consider that $|\sgn(b) - b| = |1 - |b| | = | |a| - |b| | \le |a - b|$, so $|a - \sgn(b)| \le |a - b| + |b - \sgn(b)| \le 2 |a - b|$.

In addition, we cite a theorem from \cite{Cheeger}:

\textbf{Theorem (Cheeger inequality for the connection Laplacian)}  Suppose $G = (V, E)$ is a connected graph with spectral gap $\tau$.  Then, given the relative phase matrix $A_1$, if $u$ is an eigenvector for the smallest eigenvalue of $L_1$, then $w = \sgn(u)$ satisfies \[\eta(w) \le \bigfrac{C'}{\tau} \min_{\omega : V \to \S^1} \eta(\omega),\] where $C'$ is a universal constant.

\textbf{Proof}  Consider that finding the smallest eigenvalue of $L_1$ relaxes the problem $\min_{\omega : V \to \S^1} \eta(\omega)$, as \[\min_{u \in \C^d} \bigfrac{u^* L_1 u^*}{u^* u} = \min_{y \in \C^d} \bigfrac{(D^{1/2}y)^* L_1 (D^{1/2}y)}{(D^{1/2}y)^*(D^{1/2}y)} = \min_{y \in \C^d} \bigfrac{y^*(D - A_1)y}{y^* D y} = \min_{y \in \C^d} \eta(y) \le \min_{\omega : V \to \S^1} \eta(\omega).\]  From here, lemma 3.6 in \cite{Cheeger} gives \[\eta(w) \le \bigfrac{44}{\tau} \eta(u) \le \bigfrac{44}{\tau} \min_{\omega : V \to \S^1} \eta(\omega).\]

We now prove the main proposition:

\textbf{Proposition}  Let $G = (V, E)$ be a connected, unweighted graph ($p_{ij} = \chi_E(i, j)$) with spectral gap $\tau > 0$.  Then if we take $y$ to be an eigenvector corresponding to the smallest non-zero eigenvalue of $L_1$, and define \[u_i = \bigfrac{y_i}{|y_i|} \ \text{and} \ x_i = \bigfrac{(x_0)_i}{|x_0|_i},\] then for some universal constant $C$, \[\min_{\theta \in [0, 2\pi]} ||x - \ee^{\ii \theta} u||_2 \le C \bigfrac{||A_0 - A_1||_F}{\tau}.\]  If, in addition, $G$ is $k$-regular, then we have \[\min_{\theta \in [0, 2\pi]} ||x - \ee^{\ii \theta}u||_2 \le C \bigfrac{||A_0 - A_1||_F}{k^{1/2}\tau}.\]

\textbf{Proof}  We begin by defining $g_i, i \in V$ and $\epsilon_{ij}, (i, j) \in E$ by $g_i = x_i^* u_i$ and $\epsilon_{ij} = (A_0)_{ij}^*(A_1)_{ij} = x_j^* x_i \rho_{ij}$.  Then by lemma 1 and the triangle inequality, we have \[\everymath{\displaystyle}\begin{array}{rcl}%
\sum_{(i, j) \in E}  \left(\frac{1}{2} |g_j - g_i|^2 - |\epsilon_{ij} - 1|^2\right) & \le & \sum_{(i, j) \in E}  \left( |g_j - g_i| - |\epsilon_{ij} - 1|\right)^2 \\%
 & \le & \sum_{(i, j) \in E}  |g_j - \epsilon_{ij}g_i|^2 \\%
& = & \sum_{(i, j) \in E}  |x_j^*u_j - x_j^* x_i \rho_{ij}x_i^* u_i|^2 \\%
& = & \sum_{(i, j) \in E}  |u_j - \rho_{ij} u_i|^2 \\%
& = & \vol(G) \eta(u),%
\end{array}\]%
as the denominator of $\eta(w)$ is $\vol(G)$ whenever the entries of $w$ all have unit modulus.

The Cheeger inequality, then, gives \[\sum_{(i, j) \in E} \left(\frac{1}{2} |g_j - g_i|^2 - |\epsilon_{ij} - 1|^2\right) \le \bigfrac{C' \vol(G)}{\tau} \min_{\omega : V \to \S^1} \eta(\omega) \le \bigfrac{C' \vol(G)}{\tau} \eta(x),\] but \[\begin{array}{rcl}%
\eta(x) & = & \bigfrac{\sum_{(i, j) \in E} |x_j - \rho_{ij} x_i|^2}{\sum_{i \in V} \deg(i) |x_i|^2} \\%
& = & \bigfrac{\sum_{(i, j) \in E} |x_i^* x_j - \rho_{ij}|^2}{\vol(G)} \\%
& = & \bigfrac{||A_0 - A_1||_F^2}{\vol(G)},%
\end{array}\]%
Considering also that $\sum_{(i, j) \in E} |\epsilon_{ij} - 1|^2 = \sum_{(i, j) \in E} |\rho_{ij} - x_i^*x_j|^2 = ||A_0 - A_1||_F^2,$ this analysis gives \[\sum_{(i, j) \in E} |g_i - g_j|^2 \le 2 \left(\bigfrac{C'}{\tau} + 1\right) ||A_0 - A_1||_F^2.\]

We now set $\alpha = \bigfrac{\sum_{i \in V} \deg(i) g_i}{\vol(G)}$ and $w_i = g_i - \alpha$.  Then \[\mathbbm{1}^* D w = \sum_{i \in V} \deg(i)(g_i - \alpha) = 0,\] so $D^{1/2}w \perp D^{1/2}\mathbbm{1}$.  Since we showed in the inital discussion that $\Nul(L) = \Span(D^{1/2}\mathbbm{1})$ and $L \succeq 0$, this gives that \[\bigfrac{(D^{1/2}w)^* L (D^{1/2}w)}{w^* D w} \ge \min_{y \in \C^d, y \perp D^{1/2}\mathbbm{1}} \bigfrac{y^* L y}{y^* y} = \tau.\]  Therefore, \[\everymath{\displaystyle} \begin{array}{rcl}%
\tau w^* D w \le w^*(D - A) w & = & (g - \alpha\mathbbm{1})^*(D - A)(g - \alpha\mathbbm{1}) \\%
 & = & g^*(D - A)g \\%
 & = & \sum_{i \in V} \deg(i) |g_i|^2 - \sum_{i \in V}\sum_{j \in V}  g_i^* g_j \\%
 & = & \sum_{i, j \in V} (1 - g_i^* g_j) \\%
 & = & \sum_{(i ,j) \in E} (2 - g_i^* g_j - g_j^* g_i) \\%
 & = & \sum_{(i, j) \in E} |g_i - g_j|^2 \\%
 & \le & 2\left(\bigfrac{C'}{\tau} + 1\right) ||A_0 - A_1||^2_F.%
\end{array}\]%
We consider now that $\tau w^* D w = \tau \sum_{i \in V}\deg(i)|g_i - \alpha|^2$; by lemma 2, then, we have \[\bigfrac{\tau}{4} \sum_{i \in V} \deg(i) |g_i - \bigfrac{\alpha}{|\alpha|}|^2 \le \tau w^* D w \le 2\left(\bigfrac{C'}{\tau} + 1\right) ||A_0 - A_1||^2_F.\]  Considering the definition of $g_i$, setting $\ee^{\ii\theta} = \bigfrac{\alpha}{|\alpha|}$, and rearranging gives \[\sum_{i \in V} |u_i - \ee^{\ii \theta}x_i|^2 \le \bigfrac{8}{\min(\deg(i))} \left(\bigfrac{C'}{\tau^2} + 1\right) ||A_0 - A_1||^2_F.\]  If $G$ is not $k$-regular, we bound $\min(\deg(i))$ by $1$ ($G$ is connected; in particular, there are no isolated vertices), otherwise we osberve $\min(\deg(i)) = k$, and the desired result follows.

\textbf{Corollary}  Suppose $x_0 \in \C^d$ and let $\tilde{x}_0 = \sgn(x_0)$.  Further let $X_0 \in \C^{d \times d}$ be defined by \[(X_0)_{ij} = \left\{\begin{array}{r@{,\ }l} (x_0)_i^*(x_0)_j & |i - j| \mod d < \delta \\ 0 & \text{otherwise} \end{array}\right.\] and let $\widetilde{X}_0 \in \C^{d \times d}$ be defined by \[(\widetilde{X}_0)_{ij} = \left\{\begin{array}{r@{,\ }l} \sgn((X_0)_{ij}) & |i - j| \mod d < \delta \\ 0 & \text{otherwise}. \end{array}\right.\] Now suppose $X = X_0 + N$ where $N_{ij} = 0$ for $|i - j| \mod d \ge \delta$ and $N$ is Hermitian, and let $\widetilde{X}$ be a similarly normalized version of $X$.  Let $\eta$ be such that $||\widetilde{X}_0 - \widetilde{X}||_F \le \eta ||\widetilde{X}_0||_F$.  Then if $\tilde{x} = \sgn(u)$ where $u$ is the top eigenvector of $\widetilde{X}$, we have \[\min_{\theta \in [0, 2\pi)} ||\tilde{x}_0 - \ee^{\ii\theta}\tilde{x}||_2 \le C \bigfrac{\eta d^{5/2}}{\delta^2}.\]

\textbf{Proof}  We cite the proposition with:

\begin{itemize}
  \item $G = (V, E)$, where $V = [d]$ and $E = \{(i, j) : |i - j| \mod d < \delta\}$.  Observe that $G$ is $2\delta - 2$-regular.
    \item The spectral gap of $G$ is the second smallest eigenvalue of $I - D^{-1/2}A D^{-1/2}$, which is $\tau \ge \frac{1}{2\delta - 2}(C''\delta^3 / d^2) = C_3\delta^2 / d^2$ by lemma 2 of \cite{robust}.
    \item $A_0 = \widetilde{X}_0 - I$ and $A_1 = \widetilde{X} - I$.
      \item $||\widetilde{X}_0||_F = \sqrt{d(2\delta - 1)}$, so $||A_1 - A_0||_F \le C_4\eta (d \delta)^{1/2}$
\end{itemize}

Combining these, we have \[\min_{\theta \in [0, 2\pi)} ||\tilde{x}_0 - \ee^{\ii\theta}\tilde{x}||_2 \le C \bigfrac{C_4 \eta (d \delta)^{1/2}}{C_3 \delta^2 / d^2 \cdot (2 \delta - 2)^{1/2}} = C_6 \bigfrac{\eta d^{5/2}}{\delta^2}.\]

\bibliographystyle{abbrv}
\bibliography{graph-bound}
\end{document}
