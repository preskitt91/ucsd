\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\ensuremath{\mathbbm{e}}}
\newcommand{\ii}{\ensuremath{\mathbbm{i}}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}



%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{Spectrum of $\delta$-truncated matrices}
\author{Prof. Rayan Saab and Brian Preskitt}
\date{}

\begin{document}
\maketitle

%% \begin{abstract}
%% This article discusses the eigenvectors and eigenvalues of matrices of the form $A_\d = 
%% \end{abstract}

\section{Spectrum of $A_\delta$}

Let $x \in \C^d$ be a vector such that $|x_i| = 1$, for $i = 1, \ldots, d$.  Then we take $A = xx^*$; clearly $A$ is a rank-1 matrix with eigenvalue $||x||^2 = d$.

Now let $\delta \in \N$ be such that $\delta \le \lfloor \frac{d+1}{2} \rfloor$ and define the matrix $A_\delta$ by

\[ (A_\delta)_{ij} = \left\{
\begin{array}{r@{\quad,\quad}l}
  A_{ij} & |i - j| \mod d < \delta \\
  0 & \text{otherwise}
\end{array}\right.,\]

where we take $|k| \mod d = \min\{k \mod d, -k \mod d\}$.

We next consider $\tilde{x} = \mathbbm{1}$ with $\tilde{A} = \tilde{x}\tilde{x}^*$; $\tilde{A}_\delta$ is defined in the manner above.  Observe that, for any $\delta$, $\tilde{A}_\delta$ is circulant, so its eigenvectors are the Fourier vectors.  Setting $\omega_j = \ee^{2\pi\ii\frac{j}{d}}$ for $j = 0, 1, \ldots, d-1$, then the eigenvalues (for $j = 0, \ldots, d-1$) are given by 

\begin{eqnarray}
\lambda_j & = & \sum_{k = 1}^d (\tilde{A}_\delta)_{1k}\omega_j^{k-1} \notag \\
& = & \sum_{k = 1}^\delta \omega_j^{k-1} + \sum_{k=d-\delta + 2}^d \omega_j^{k-1} \notag \\
& = & 1 + \sum_{k = 1}^{\delta - 1}\omega_j^k + \omega_j^{-k} \notag \\
& = & 1 + 2 \sum_{k = 1}^{\delta - 1}\cos\left(\bigfrac{2\pi j k}{d}\right),
\label{eqn:lambda}
\end{eqnarray}

so that $\lambda_0 = 2 \delta - 1$.  We set $\Lambda_\delta = \diag\{\lambda_0, \ldots, \lambda_{d-1}\}$ and let $F$ denote the $d \times d$ Fourier matrix, such that $\tilde{A}_\delta = F \Lambda_\delta F^*$.  

Returning to arbitrary $x$, we set $U = \diag\{x_1, \ldots, x_d\}$ and observe that $x = U\tilde{x}$, so $A = U \tilde{A} U^*$ and $A_\delta = U \tilde{A}_\delta U^*$.  As $|x_i| = 1$ for each $i$, we have that $U$ is unitary, so

\[
A_\delta = U F \Lambda_\delta F^* U^*,\]

therefore the eigenvalues of $A_\delta$ are given by equation \ref{eqn:lambda}, and its eigenvectors are simply the Fourier vectors modulated by the entries of $x$.  

\section{Eigenvalue gap}

We next investigate the eigenvalue gap for $A_\delta$.  Set $\theta_j = \frac{2 \pi j}{d}$ and begin by observing that, for any $\theta \in \R$,

\begin{eqnarray*}
  \sum_{k = 1}^{\delta - 1}\cos(\theta k) & = & \bigfrac{\sin(\theta / 2)}{\sin(\theta / 2)}\sum_{k = 1}^{\delta - 1}\cos(\theta k) \\
  & = & \bigfrac{1}{2\sin(\theta / 2)} \sum_{k=1}^{\delta - 1}\left(\sin(\theta(k + 1 / 2)) - \sin(\theta(k - 1 / 2))\right) \\
  & = & \bigfrac{1}{2\sin(\theta / 2)}\left(\sin(\theta(\delta - 1/2)) - \sin(\theta / 2)\right) \\
  & = & \bigfrac{1}{2}\left(\bigfrac{\sin(\theta(\delta - 1/2))}{\sin(\theta / 2)} - 1\right)
\end{eqnarray*}

Accordingly, we define $l_\delta : \R \to \R$ by $l_\delta(\theta) = 1 + 2\sum_{k=1}^{\delta - 1} \cos(\theta k)$.  Now we have

\begin{eqnarray*}
  \lambda_j & = & l_\delta(\theta_j) \\
  & = & 1 + 2 \sum_{k = 1}^{\delta - 1}\cos(\theta_j k) \\
  & = & \bigfrac{\sin(\theta_j(\delta - 1/2))}{\sin(\theta_j / 2)}
\end{eqnarray*}

Of course, $\lambda_0 = 2\delta - 1$ is the largest of these, so the eigenvalue gap is at most equal to

\begin{eqnarray*}
  \lambda_0 - \lambda_1 & = & (2\delta -  1) - \bigfrac{\sin(\pi / d (2\delta - 1))}{\sin(\pi / d)} \\
  & \le & (2 \delta - 1) - \bigfrac{\pi / d(2 \delta - 1) - \frac{1}{6}(\pi / d (2 \delta - 1))^3}{\pi / d} \\
  & = & \frac{1}{6}\left(\frac{\pi}{d}\right)^2(2 \delta - 1)^3 \\
  & \le & O(\bigfrac{\delta^3}{d^2})
\end{eqnarray*}

On the other hand, we find the following lower bound by considering that $\theta_j \in [\pi / d, 2\pi - \pi / d]$ for every $j$, so 

\begin{eqnarray*}
  \lambda_0 - \max \lambda_j & \ge & \lambda_0 - \max_{\theta \in [\pi / d, 2\pi - \pi / d]} l_\delta(\theta) \\
  & = & (2 \delta - 1) - \max_{\theta \in [\pi / d, \pi]} l_\delta(\theta)
\end{eqnarray*}

The second line follows from the symmetry of $l_\delta$ about $\theta = \pi$.

Consider now that 

\begin{eqnarray*}
  l_\delta'(\theta) & = & \bigfrac{(\delta - 1/2)\cos((\delta - 1/2)\theta) \sin(\theta/2) - 1/2 \sin((\delta - 1/2)\theta) \cos(\theta / 2)}{\sin(\theta / 2) ^ 2} \\
  & \le & 0 \\
  & \iff & \\
  (2\delta - 1) \sin(\theta/2)\cos((\delta - 1/2)\theta) & \le & \sin((\delta - 1/2)\theta) \cos(\theta /2 ). \\
\end{eqnarray*}

Since $\tan$ is convex on $[0,\, \pi / 2)$, this last inequality will hold for $\theta \in [0,\, \frac{\pi}{2\delta - 1})$.  For $\theta \in [\frac{\pi}{2\delta-1}, \frac{2\pi}{2\delta -1})$, $\cos((\delta - 1/2)\theta) < 0$ while the remainder of the terms are non-negative, so the inequality also holds.  In addition, $l_\delta(\frac{2\pi}{2\delta - 1}) = 0$, so $l_\delta$ is decreasing on its way to its first zero.  Therefore,

\[\lambda_0 - \max \lambda_j \ge (2 \delta - 1) - \max\{\lambda_1, \max_{\theta \in [\frac{2\pi}{2\delta-1}, \pi]} l_\delta(\theta)\},\]

which permits us to look at $(2 \delta - 1) - \lambda_1$ and $(2 \delta - 1) - \max\limits_{\theta \in [\frac{2\pi}{2\delta-1}, \pi]} l_\delta(\theta)$ separately.

For $\max\limits_{\theta \in [\frac{2\pi}{2\delta-1}, \pi]} l_\delta(\theta)$, consider the formulation \[l_\delta(\theta) = \bigfrac{\sin((\delta - 1/2)\theta)}{\sin(\theta / 2)},\] which allows us to devise an upper bound by considering this as a sine wave modulated by a decreasing envelope; therefore, we take the first non-negative half-cycle of $\sin((\delta - 1/2)\theta)$ and bound $l_\delta$ there.  More specifically,

\begin{eqnarray*}
  \max_{\theta \in [\frac{2\pi}{2\delta-1}, \pi]} l_\delta(\theta) & = & \max_{\theta \in [\frac{4\pi}{2\delta - 1},\, \frac{6\pi}{2\delta - 1}]} l_\delta(\theta) \\
  & \le & \bigfrac{1}{\sin(\frac{2\pi}{2\delta - 1})} \\
  & \le & \bigfrac{2\delta - 1}{4},
\end{eqnarray*}

where the last line assumes $\bigfrac{2 \pi}{2\delta - 1} \le \pi / 2$ (i.e. $\delta \ge 3$).  If this is indeed the top eigenvalue, it gives an eigenvalue gap of $\bigfrac{3}{4}(2\delta - 1)$, which we shall see is much too large.

Now for $\lambda_1$, we are going to assume $\theta_1 \cdot (\delta - 1) \le \pi / 2$ (in other words, $4(\delta - 1) \le d$), such that $\cos$ will be convex on $[0,\, \theta_1(\delta-1)]$.  To justify this assumption, consider that if $\theta_1 (\delta - 1) > \pi / 2$, then the last several terms of the summation in $l_\delta(\theta_1)$ will give negative terms, so we'll have $l_\delta(\theta_1) < l_{\lfloor d /4 + 1 \rfloor}(\theta_1)$.  Additionally, in our application we expect $\delta = O(\log(d))$, so $4(\delta - 1) \le d$ is completely reasonable.

This will give $\sum_{k=1}^{\delta - 1}\cos(k\theta_1) \le (\delta - 1) \cos(\theta_1 \frac{\delta}{2})$, so

\begin{eqnarray*}
  \lambda_0 - \lambda_1 & \ge & 2(\delta - 1)(1 - \cos(\pi\frac{\delta}{d}))\\
  & \ge & 2(\delta - 1)(\pi^2\frac{\delta^2}{2d^2} - \pi^4\frac{\delta^4}{24d^4}) \\
  & = & \pi^2\bigfrac{\delta^3}{d^2} - \pi^2\bigfrac{\delta^2}{d^2} -\pi^4\bigfrac{\delta^5}{12d^4} + \pi^4\bigfrac{\delta^4}{12d^4} \\
  & = & O(\bigfrac{\delta^3}{d^2})
\end{eqnarray*}

Since the $\max\limits_{\theta \in [\frac{2\pi}{2\delta - 1},\, \pi]}l_\delta(\theta)$ case gave $O(\delta)$, we conclude that, asymptotically,

\[\pi^2 \bigfrac{\delta^3}{d^2} \le \lambda_0 - \max_j \lambda_j \le \bigfrac{4\pi^2}{3}\bigfrac{\delta^3}{d^2}\]





\end{document}
