%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, hyperref}
\usepackage{graphics, parskip, color}
\usepackage{afterpage}
\usepackage[margin=0.5in]{geometry}
%\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}

\title{Math 4C Homework Assignments \\ Winter 2017}
\author{Instructor: Brian Preskitt}
\date{\color{red} \Large LAST UPDATED: \today}

\begin{document}
\maketitle

\begin{itemize}
\item Homework \#1, \textbf{Due Date: January 20}
  \begin{itemize}
  \item Section 0.2: 4, 18, 30, 48, 58
  \item Section 0.3: 8, 12, 18, 34, 58, 60, 73
  \item Section 1.1: 4, 8, 14, 16, 20, 24, 32, 36, 60
  \item Section 1.2: 4, 10, 12, 16, 22, 24, 40, 48
  \end{itemize}

\item Homework \#2, \textbf{Due Date: January 27}
  \begin{itemize}
  \item Section 1.3: 8, 12, 14, 18, 28, 34, 46
  \item Section 1.4: 2, 6, 16, 34, 38, 42, 54
  \item Section 1.5: 4, 8, 10, 16, 22, 30, 36
  \item Section 1.6: 2-30, even
  \end{itemize}

\item Homework \#3, \textbf{Due Date: February 3}
  \begin{itemize}
  \item Section 2.1: 4, 6, 12, 14, 16, 26, 28, 32, 36, 42, 44
  \item Section 2.2: 14, 16, 18, 24, 28, 30, 36, 40, 70, 72, 74
  \item Section 2.3: 10, 14, 20, 24, 28, 32, 36, 40, 54, 62, 68, 86, 110, 130
  % \item Section 2.4: 2, 6, 16, 18, 24, 26, 28
  \end{itemize}

\item Homework \#4, \textbf{Due Date: February 10}
  \begin{itemize}
  \item Section 2.4: 2, 8, 12, 18, 20, 24, 26, 28
  \item Section 2.5: 4, 8, 10, 16, 24, 34, 36, 38, 40
  \item Section 3.1: 8, 12, 20, 24, 28, 34, 38, 40,
  \item Section 3.2: 4, 10, 12, 20, 30
  \end{itemize}

\item Homework \#5, \textbf{Due Date: February 17}
  \begin{itemize}
  \item Section 3.3: 2, 4, 8, 24, 26, 28, 32, 34, 61, 64, 72
  \item Section 3.7: 2, 4, 10, 37
  \item Section 4.1: 2, 6, 18, 20, 24, 28, 42, 44, 46, 48, 52, 64
  \item Section 4.2: 2, 8, 12, 16, 24, 26, 32, 34, 38, 42, 46
  \end{itemize}


  %\item Section 4.3: 2, 10, 16, 18, 20, 22, 24, 32

\end{itemize}

\end{document}
