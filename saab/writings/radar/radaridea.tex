%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathbbm{e}}
\newcommand{\ii}{\mathbbm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\newcommand{\sgn}{\ensuremath{\mathrm{sgn}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{Pilot Tone Phase Encoding Idea}
\author{R.~Saab, P.~Salanevich, B.~Preskitt}
\date{}

\begin{document}
\maketitle

\section{Notation}

We let:

\begin{itemize}
\item For $k \in \N$, we denote by $[k]$ the set $[k] = \{1, 2, \ldots, k\} \subset \N$.
\item For $u, v \in \Cn, u \odot v$ is the Hadamard (entry-wise) product of $u$ and $v$.
\item For $z \in \C^k$, $u \ast z \in \C^{k + n - 1}$ is the usual convolution of $u$ and $z$, that is \[(u \ast z)(m) = \sum_{i \in \Z} u(i) z(m - i),\] where we consider $u, z$ to be padded with zeros outside their usual domains $[n]$ and $[k]$.
\item $u \circledast v$ is the circular convolution of $u$ and $v$: \[(u \circledast v)(m) = \sum_{i=1}^n u(i) v(m-i),\] where the indices are taken $\mod n$ in $[n]$.
\item $\mathcal{F}(u) = F_nu = Fu = \hat{u}$ is the normalized DFT of $u$; that is, \[(F_n)_{jk} = \bigfrac{1}{\sqrt{n}} \ee^{2\pi(j-1)(k-1)\ii / n},\] and we omit the subscript $n$ when context makes the dimension clear.
\item $u_{-}$ represents time reversal of $u$, i.e.~\[(u_{-})(i) = u(N - i + 1)\]
\item $|u|$ denotes the entry-wise absolute value of $u$: \[|u|(i) = |u(i)|\]
\item $\overline{u}$ is the complex conjugate of $u$.
\item $\sgn(u)$ is the sign of $u$, namely \[\sgn(u)(k) = \left\{\begin{array}{r@{,\quad}l}%
u(k) / |u(k)| & u(k) \neq 0 \\
1 & u(k) = 0
\end{array}\right.\]%
\item $S : \Cn \to \Cn$ is the circular time shift operator, namely \[(Su)(i) = u(i-1),\] with indices taken $\mod n$ in $[n]$.
\end{itemize}

\section{Our idea}

We consider two parties: a transmitter and a receiver.  The transmitter and the receiver share partial knowledge of a \emph{pilot signal} $u \in \C^P$ and its Fourier transform $\hat{u} \in \C^P$ -- namely, the transmitter knows $u$ completely, while the receiver only knows $|\hat{u}|$.  Furthermore, we will assume that $\hat{u}$ takes on only non-zero values.  The transmitter wants to send a signal $s \in \C^N$ to the receiver, and will embed the pilot signal $u$ in $s$ in the following way:

We require that $N = DP$ for some $D \in \N$, and then the transmitter and receiver agree to assert that $\sqrt{N}\hat{s}(Dk) = \sqrt{P}\hat{u}(k)$ for $k = 1, \ldots, P$.  In other words, $s$ is known to be a member of the $(N - P)$-dimensional subspace \[\mathcal{U} = \{x \in \C^N : \sqrt{N} (F_N x)(Dk) = \sqrt{P} (F_P u)(k)\}.\]

Next, we consider the \emph{transmission channel} between the transmitter and the receiver; namely, we shall assume that there is some vector $h \in \C^L$ of known length $L \le N$ such that, whenever the transmitter sends a signal $x \in \C^M$ (for any $M \in \N$), the receiver detects (gains complete knowledge of) the signal $y = x \ast h$.

After constructing $s \in \mathcal{U}$, the transmitter sends the message $\begin{bmatrix} c \\ s \end{bmatrix}$, where $c \in \C^L$ is the \emph{cyclic prefix} of length $L$.  More specifically, $c(i) = s(N - L + i)$ so that \[\begin{bmatrix} c(1) \\ c(2) \\ \vdots \\ c(L) \end{bmatrix} = \begin{bmatrix} s(N - L + 1) \\ s(N - L + 2) \\ \vdots \\ s(N) \end{bmatrix},\] the last $L$ entries of $s$.  We denote this relationship by $c = CP_L(s)$.  Therefore, the receiver detects the signal $y \in \C^{N + L - 1}$ given by \[y = h \ast \begin{bmatrix} c \\ s \end{bmatrix},\] so if we define $r \in \C^N$ by $r(i) = y(L - 1 + i)$, we will have that \[r = s \circledast \begin{bmatrix} h \\ 0_{N - L} \end{bmatrix}.\]  We set \[h' = \begin{bmatrix} h \\ 0_{N-L} \end{bmatrix} \in \C^N \ \text{ and } \ \widetilde{h} = \begin{bmatrix} 0 \\ h \\ 0_{P - 2L - 1} \\ \overline{h}_{-} \end{bmatrix} \in \C^P\] so that $r = s \circledast h'$.

For our method, we will repeat this process for two signals $s_1, s_2 \in \mathcal{U}$ and the cyclic prefixes $c_1 = CP_L(s_1)$ and $c_2 = CP_L(\overline{s_2}_{-})$.  Transmitting $x = \begin{bmatrix} c_1 \\ s_1 \\ c_2 \\ (\overline{s_2})_{-} \end{bmatrix}$, the receiver will see \[y = x \ast h = \begin{bmatrix} g_1 \\ y_1 \\ g_2 \\ y_2 \\ t \end{bmatrix} \in \C^{3L + 2N - 1},\] with $g_i \in \C^L, y_i \in \C^N, t \in \C^{L-1}$.  Of this, we keep only $y_1 = h' \circledast s_1$ and $y_2 = h' \circledast \overline{s_2}_{-}$.  Using the identity $(x \circledast y)_{-} = S^{-1}(x_{-} \circledast y_{-}),$ we have \[y_2 = S((h'_{-} \circledast \overline{s_2})_{-}),\] giving \[\overline{(S^{-1}(y_2))}_{-} = \overline{h'}_{-} \circledast s_2 = \begin{bmatrix} 0_{N - L} \\ \overline{h}_{-} \end{bmatrix} \circledast s_2.\]  Finally, then, \[F(Sy_1 + \overline{(S^{-1}y_2)}_{-}) = F(Sh') \odot \hat{s}_1 + F(\overline{h'}_{-}) \odot \hat{s}_2\] so that \[\begin{array}{rcl}
\sqrt{N}\ F_N(Sy_1 + \overline{(S^{-1}y_2)}_{-})(Dk) & = & \sqrt{N}\ F_N(Sh' + \overline{h'}_{-})(Dk) \cdot \hat{u}(k) \\
& = & \sqrt{P}\ F_P(Sh' + \overline{h'}_{-})(k) \cdot \hat{u}(k) \\
& = & \sqrt{P}\ F_P(\widetilde{h})(k) \cdot \hat{u}(k)
\end{array}\] Therefore, letting $b = \left(\sqrt{N}\ F_N(Sy_1 + \overline{(S^{-1}y_2)}_{-})(Dk)\right)_{k=1}^P$, we have that \[b = \sqrt{P}\ \diag(\hat{u}) F_P(\widetilde{h}).\]

Because the receiver has full knowledge of $y_1$ and $y_2$ (as they are part of what she actually received), she may calculate $b$; since she further knows $|\hat{u}|$ (which is agreed upon \emph{a priori}), she may calculate \[z = \bigfrac{1}{\sqrt{P}} \diag(|\hat{u}|^{-1}) b = D_\phi F_P\widetilde{h},\] where $D_\phi = \diag(\phi)$ where $\phi = \sgn(\hat{u})$.

We now consider that $F_P\widetilde{h}$ is real, as \[\begin{array}{rcl}%
F_P\widetilde{h} (l) & = & \sum_{k=1}^L \ee^{2 \pi \ii k l / P} h(k) + \sum_{k = P - L}^{P-1} \ee^{2 \pi \ii k l / P} \overline{h}(P - k) \\
 & = & 2 \Re\left(\sum_{k=1} \ee^{2\pi \ii k l / P} h(k) \right). \\
\end{array}\]  Therefore, the data $z$ implies knowledge of $D_\phi$ and therefore $F_P\widetilde{h}$ up to multiplication by $\pm 1$ on each entry!  If we are able to accurately recover this sign pattern (say $F_P\widetilde{h} = D_s|F_P\widetilde{h}|$ where $D_s = \diag(\sgn(F_P\widetilde{h}))$), then we will know $D_\phi$ and $F_P\widetilde{h}$.  Consequently, from the perspective of the communications system, if we are able to recover $D_s$, this scheme allows us to use the phases of $\hat{u}$ to transmit extra data while simultaneously identifying the channel $h$.

We guarantee recovery of $D_s$ as follows.  We attempt to choose a subspace $V = \Col(A) \subset \C^P$ such that if $Ax \in V$, then $Ax + k \notin V$ for any $k \in \Z^P \setminus \{0\}$.  $A \in \C^{P \times K}$ will be assumed to be full rank such that $x \in \C^K$ is the message uniquely encoded into the phase vector $\phi$.  The transmitter and receiver will agree beforehand that $\phi = \ee^{\pi \ii v}$ for some $v = Ax \in V$, so that knowledge of $D_\phi D_s F_P\widetilde{h}$ will uniquely determine $v$, and therefore $x, \phi, D_s$ and $F_P\widetilde{h}$.

For example, taking $a \in \C^P$ to be a randomly chosen vector, say $a \sim \mathcal{N}(0, I_P)$, and setting $V = \Nul(a^*)$ will do, as $a^*x = 0$ has non-zero integer solutions with probability zero.

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

\end{document}
