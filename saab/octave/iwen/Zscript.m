clear

delta = 32;

for i = 1 : 15
    d = i * 80;

    Z = -2 * ones(d);
    Z = Z + (4 * delta - 2) * eye(d);
    Z(1, 1) = Z(1, 1) + 1;
    Z = MatTrunc(Z, delta);
    Z = sparse(Z);

    result(i, 1) = eigs(Z, 1, 'sm');
    result(i, 2) = result(i, 1) * d^2;
    result(i, 3) = result(i, 1) * d;
    result(i, 4) = result(i, 1) * d^(127/64);
    result(i, 5) = d;
end

vals = result(:, 1);
d = result(:, 5);
    
    
