addpath('..');
d = 100;
trials = 10;
max_delt = ceil((d + 1) / 2);

x = ones(d, 1);
A = x * x';

err = zeros(trials, max_delt - 1);
froberr = zeros(trials, max_delt - 1);
totfrob = zeros(trials, max_delt - 1);

for tr = 1 : trials
  theta = 2 * pi * rand(d, 1);
  y = exp(i * theta);
  phi = arg(sum(y));
  y = y * exp(-i * phi);
  
  err(tr, :) = norm(x - y) ^ 2;

  At = y * y';
  totfrob(tr, :) = norm(A - At, 'fro') ^ 2;

  for delta = 2 : max_delt
    Ad = MatTrunc(A, delta);
    Atd = MatTrunc(At, delta);

    froberr(tr, delta - 1) = norm(Ad - Atd, 'fro') ^ 2;
  end
end

dmat = repmat((2 : max_delt), trials, 1);
