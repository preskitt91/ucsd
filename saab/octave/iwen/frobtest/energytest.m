
addpath('..');
clear
mind = 4;
maxd = 6;
trials = 16;

for di = 1 : (maxd - mind + 1)

  d = 2 ^ (di + mind - 1);

  max_delt = floor((d + 1) / 4);
  F = fourmat(d);
  x = ones(d, 1);
  A = x * x';

  froberr{di} = zeros(max_delt, trials);
  enerr{di} = zeros(max_delt, trials);

  for delti = 1 : max_delt
    delta = delti + 1;
    for j = 1 : d
      lambda(j) = (2 * delta - 1) - 2 * ldelta(d, delta, j);
    end

    for tr = 1 : trials
      
      theta = pi * (2 * rand(d, 1) - 1);
      xt = exp(i * theta);

      At = xt * xt';

      Ad = MatTrunc(A, delta);
      Atd = MatTrunc(At, delta);

      froberr{di}(delti, tr) = norm(Ad - Atd, 'fro');
      enerr{di}(delti, tr) = norm(F(:, 1) * F(:, 1)' - (xt .* F(:, 1)) * (xt .* F(:, 1))', 'fro');
    end
  end
  errat{di} = froberr{di} ./ enerr{di};
end
