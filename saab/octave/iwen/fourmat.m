function F = fourmat(n)

	 i = (0 : (n-1))';
	 j = (0 : (n-1));

	 i = repmat(i, 1, n);
	 j = repmat(j, n, 1);

	 F = exp(2 * pi * sqrt(-1) * i .* j / n) / sqrt(n);
