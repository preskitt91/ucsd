d = 128;
maxdel = 24;
its = 8;
evals = 15;
maxmult = 2;
gap = zeros(its, maxdel-1);
avals = zeros(maxdel-1, evals);
allvecs = zeros(d, maxmult, maxdel-1);
x = rand(d, 1) - 0.5;
x = x ./ abs(x);
X = x * x';

for delta = 2 : maxdel
    B = MatTrunc(X, delta);
    % bvals = sort(eigs(B, 2, "lr"), "descend");
    % bgap(i, delta-1) = bvals(1) - bvals(2);
    A = MatNorm(B);
    avals((delta-1), :) = sort(eigs(A, evals), "descend");
    avecs = null(A - avals((delta-1), 2) * eye(d));
    novecs(delta-1) = size(avecs, 2);
    allvecs(:, 1:novecs(delta-1), delta-1) = avecs;
end



% for i = 1 : (delta-1)
%     for j = i : (delta-1)
%         subsp = allvecs(:,:,i)' * allvecs(:,:,j);
%         [~, sig, ~] = svd(subsp);
%         theta = acos(diag(sig));
%         distances(i,j) = norm(theta);
%     end
% end

% agap = real(agap);
% bgap = real(bgap);
