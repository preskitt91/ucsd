function l = ldelta(d, delta, k)

  theta = 2 * pi * (1 : (delta - 1)) / d;

  l = (delta - 1) - sum(cos(theta * k));
  
