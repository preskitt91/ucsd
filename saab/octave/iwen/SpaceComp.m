## Copyright (C) 2015 Brian Preskitt
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} SpaceComp (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Brian Preskitt <ubuntu@ubuntu-HP-Compaq-nc6320-PZ898UA-ABA>
## Created: 2015-10-06

function [dist, vecs, vals] = SpaceComp(x, maxdelt, depth)
    
    x = x / norm(x);
    X = x * x';
    d = size(x, 1);
    
    dist = zeros(depth, (maxdelt + 1));
    vecs = zeros(d, depth, (maxdelt + 1));
    vals = zeros(depth, (maxdelt + 1));    
    
    for delta = 0 : maxdelt
        A = MatTrunc(X, delta);
        
        
        

endfunction
