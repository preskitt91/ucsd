for i = 3 : 6
    d = 2 ^ i;
    D = ones(d);
    I = eye(d);
    P = eye(d^2);
    P = P(:, 1 + ((1 : d) - 1) * d);
    for delta = i : -1 : 1
      D = MatTrunc(D, delta);
      H = kron(D, I) - kron(I, D);
      H = H * P;
      A = H' * H;
      A(1, 1) = A(1, 1) + 1;

      result(i-2, delta) = eigs(A, 1, "sm");
    end
end
