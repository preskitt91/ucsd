
function X = MatTrunc(X, delta)
    
    d = size(X, 1);

    i = kron([1:d], ones(d,1));
    j = i';
    t = (mod(i-j, d) < delta) | (mod(j-i, d) < delta);
    X = X .* t;
    
endfunction
