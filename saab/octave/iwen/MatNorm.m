## Copyright (C) 2015 Brian Preskitt
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} MatNorm (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Brian Preskitt <ubuntu@ubuntu-HP-Compaq-nc6320-PZ898UA-ABA>
## Created: 2015-10-06

function A = MatNorm(X)
    
    div = X + (X == 0);
    A = X ./ abs(div);

endfunction
