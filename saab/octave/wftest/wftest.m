% ## Copyright (C) 2015 Brian Preskitt 
% ## 
% ## This program is free software; you can redistribute it and/or modify it
% ## under the terms of the GNU General Public License as published by
% ## the Free Software Foundation; either version 3 of the License, or
% ## (at your option) any later version.
% ## 
% ## This program is distributed in the hope that it will be useful,
% ## but WITHOUT ANY WARRANTY; without even the implied warranty of
% ## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% ## GNU General Public License for more details.
% ## 
% ## You should have received a copy of the GNU General Public License
% ## along with this program.  If not, see <http://www.gnu.org/licenses/>.

% ## -*- texinfo -*- 
% ## @deftypefn {Function File} {@var{retval} =} wftest (@var{input1}, @var{input2})
% ##
% ## @seealso{}
% ## @end deftypefn

% ## Author: Brian Preskitt <ubuntu@ubuntu-HP-Compaq-nc6320-PZ898UA-ABA>
% ## Created: 2015-05-28

% m = 500;
% n = 5;

A = randn(m, n);
x = randn(n, 1);
x = x / norm(x);

y = (A * x) .^ 2;

lambda = sqrt(n * sum(y) / (norm(A, "fro") ^ 2));

Y = 1 / m * A' * diag(y) * A;

[v, l] = eig(Y);
l = diag(l);
[~, i] = max(l);

z0 = v(:, i);
z0 = z0 * lambda / norm(z0);

d = min(norm(x - z0), norm(x + z0));

