'''
Different functions for basic testing of BlockPR ideas.
'''

import numpy as np
from scipy import sparse
from scipy.sparse import linalg
import scipy.linalg
from numpy import random as rnd
import blocktest as bt
# from matplotlib import pyplot as plt

d = 64

rx = rnd.normal(size=d)
ix = rnd.normal(size=d)

x0 = rx + 1j * ix

def init_test_dat(x='random', delt=5, d=64, noise='normal'):
    if x is None:
        x = np.arange(10) + 1j * np.arange(10, 0, -1)
    if x == 'random':
        x = np.random.normal(size=d) + 1j * np.random.normal(size=d)
    A = np.dot(x[:, None], x[None, :].conjugate())
    A = tdelt(A, delt)

    if noise == 'normal':
        a = A.tocsr()
        n = (np.random.normal(size=A.nnz)
             + 1j * np.random.normal(size=A.nnz)) / 100
        N = sparse.csr_matrix((n, a.indices, a.indptr), shape=a.shape)
        B = (A + N).tolil()
    y = x / normalize(x[0])
    return y, A, B


def weighted_graph_matrix(T, neg_off=True):
    deg = np.array(np.abs(T).sum(axis=1) -
                   np.abs(T.diagonal())[:,None]).ravel()
    if neg_off:
        B = -T.copy()
    else:
        B = T.copy()
    B.setdiag(values=deg)
    return B
    

def meas_sparse(x0, delta, snr=-1, noise='gaussian'):
    '''
    meas_sparse returns noisy measurements of the objective vector x0
    against the "sparse" or "stupid" construction of measurement masks.
    '''
    ''' So far...  Don't really have anything :) so it just returns
    the truncated matrix you'd get from solving the linear system.'''
    if snr < 0:
        return tdelt(x0, delta)
    else:
        return 0
    # d = len(x0)
    # B = np.zeros([d, 2 * delta - 1])
    # B[:, 0] = x0 * x0.conjugate()
    # for k in range(1, delta):
        
def tdelt(x, P):
    '''
    tdelt performs the "banded truncation" of the object x, where P
    is either "delta" as used in the paper, or an index set of offset 
    diagonals to be kept.
    If x is a vector, it works on xx^*, if x is a matrix, it truncates x
    as is.
    '''
    if x.ndim == 1:
        d = len(x)
        try:
            P[0]
        except TypeError or IndexError:
            P = np.arange(P)
        ''' For sparse storage by diagonals. '''
        offsets = np.array([])
        data = np.array([[]])
        for k in P:
            '''
            This captures the "main diagonal" specified by offset k.
            '''
            offsets = np.hstack([offsets, k])
            diag = np.roll(x, k) * x.conjugate()
            if k == 0:
                data = diag
            if k != 0:
                '''
                These lines capture the reflections and wraparounds of the
                kth diagonal.
                '''
                cdia = np.roll(x, -k) * x.conjugate()
                data = np.vstack([data, diag])
                offsets = np.hstack([offsets, k - d])
                data = np.vstack([data, diag])
                offsets = np.hstack([offsets, -k])
                data = np.vstack([data, cdia])
                offsets = np.hstack([offsets, d - k])
                data = np.vstack([data, cdia])

        ''' Store as a sparse matrix and return. '''
        return sparse.dia_matrix((data, offsets), shape=(d,d))

    if x.ndim == 2:
        d = x.shape[0]
        try:
            P[0]
        except TypeError or IndexError:
            P = np.arange(P)
        ''' Get the non-zero locations.  Keep those values and return! '''
        li, lj = getLocs(P, d)
        A = sparse.lil_matrix(x.shape, dtype='complex')
        A[li,lj] = x[li,lj]

        return A

def mag_solve(T, method='eigen', vecs=None):
    '''
    Given the truncated, post-linear system matrix T, solve for the magnitudes
    of the objective vector.
    '''
    if method == 'eigen':
        d, P, delt = getP(T)
        if vecs is None:
            vecs = blocky_block_vecs(T)
        mags = np.empty(d)

        for i in range(d):
            col_inds = np.arange(i - delt + 1, i + 1)
            row_inds = np.arange(delt-1, -1, -1)
            inds = zip(row_inds, col_inds)
            mags[i] = 1 / delt * sum([np.abs(vecs[i,j]) for i, j in inds])
        return mags
    else:
        '''
        The alternate method is to read magnitues from the diagonal as 
        originally done
        '''
        mags = np.sqrt(np.abs(T.diagonal()))
        return mags

def blocky_block_vecs(T):
    if T.dtype.kind not in ['c', 'f']:
        A = sparse.lil_matrix(T, dtype='float')
    else:
        A = sparse.lil_matrix(T)
    '''
    Obtain parameters describing the non-zero shape of the matrix.
    i and j will be used to scan through the "complete" submatrices
    whose eigenvectors will tell us about the magnitudes.
    '''
    d, P, delt = getP(T)
    i = P.reshape((delt, 1)).repeat(delt, axis=1)
    j = P.reshape((1, delt)).repeat(delt, axis=0)
    vecs = np.zeros((delt, d), dtype=complex)
    # print(d, delt) # Debug line
        
    for l in range(d):
        '''
        For each index, form the proper submatrix.  Take its top
        eigenvector and accumulate
        '''
        ii = i + l
        jj = j + l
        ii = np.where(ii < d, ii, ii - d)
        jj = np.where(jj < d, jj, jj - d)
        M = A[ii, jj].toarray()
        # print(M) # Debug line
        mag, v = scipy.linalg.eigh(M, eigvals=(delt-1, delt-1),
                                   overwrite_a=True)
        # print(mag, v) # Debug line
        v = v * np.sqrt(mag) / np.sqrt(np.abs(np.dot(v.T.conj(), v)))
        vecs[:,l] = v.ravel()
        # print(vecs, l) # Debug linex
            
    return vecs / normalize(vecs[0, :])


def ang_solve(T, method='eigen'):
    '''
    Solve for the phases using that graph eigenvector thing!
    '''
    d, P, delt = getP(T)
    li, lj = getLocs(P, d)
    '''
    Normalize the non-zeros, get the top eigenvector, normalize it,
    and get outta there!
    '''
    if method == 'weighted':
        deg = np.array(np.abs(T).sum(axis=1) -
                       np.abs(T.diagonal())[:,None]).ravel()
        B = -T.copy()
        B.setdiag(values=deg)
        A = B.tocsr()
        _, u = sparse.linalg.eigsh(A, k=1, sigma=0)
        return normalize(u) / normalize(u[0])
    else:
        A = sparse.lil_matrix(T.shape, dtype='complex')
        A[li,lj] = normalize(T[li,lj])
        A = A.tocsr()
        _, u = sparse.linalg.eigsh(A, k=1)
        return normalize(u) / normalize(u[0])


def normalize(v):
    '''
    Normalizes non-zero entries of v.  Turns zeros into 1's.
    '''
    try:
        v[0]
    except:
        if v == 0: return 1
        return v / np.abs(v)

    if not sparse.issparse(v):
        v[np.where(v == 0)] = 1
    else:
        v[np.where(v.toarray() == 0)] = 1
    return v / np.abs(v)


def measFour(x0, delta, snr=-1, noise='gaussian'):
    '''
    Return the suite of measurements of x0 that would be obtained by
    using the 'Fourier' construction of masks (first example in the paper)
    '''
    pass


def phaseRet(X):
    pass


def getP(A):
    '''
    From the matrix A, determine the support set of the masks/non-zeros.
    '''
    try:
        P, = np.where(A.toarray()[0,:])
        d = A.toarray().shape[0]
    except AttributeError:
        P, = np.where(A[0,:])
        d = A.shape[0]
    ''' Restrict to [0,d/2) so we don't double count everything. '''
    P = P[np.where(P < d/2)]
    delt = len(P)
    return d, P, delt


def getLocs(P, d):
    '''
    Gets locations of non-zeros of a T_delta'd matrix using support set
    P and of size d x d
    '''
    delt = len(P)
    li = np.repeat(np.arange(d), 2 * delt - 1)
    lj = np.tile(np.hstack([P, -P[1:]]), d)
    lj = np.mod(li + lj, d)
    return li, lj


def mprime(masks, d, delta):
    num = masks.shape[1]
    l = 2 * delta - 1
    Mp = np.zeros([num, l, delta])


def full_solve(T, method='fullblock', option='opt', ret_phs=False):
    if method == 'fullblock':
        d, P, delt = getP(T)        
        vecs = blocky_block_vecs(T)
        A, W = block_decomp(T, vecs)
        phase = bt.run_opt(A.toarray(), option)
        y = W * phase / delt
        y = y / normalize(y[0])
    if method == 'oldschool':
        mags = mag_solve(T, method='old')
        phase = ang_solve(T, method='eigen')
        y = mags.ravel() * phase.ravel()
        y = y / normalize(y[0])
    if method == 'weightedold':
        mags = mag_solve(T, method='old').ravel()
        phase = ang_solve(T, method='weighted').ravel()
        y = mags * phase
        y = y / normalize(y[0])
    if method == 'ezblock':
        d, P, delt = getP(T)
        phase = np.zeros(d, dtype='complex')
        phase[0] = 1
        vecs = blocky_block_vecs(T)
        for i in range(1, d):
            phase[i] = normalize(np.dot(vecs[:-1, i].conj().T,
                                        vecs[1:, i-1])) * phase[i-1]
        _, W = block_decomp(T, vecs)
        y = W * phase / delt
        y = y / normalize(y[0])
    if method == 'eigenblock':
        

    if ret_phs:
        return y, phase
    return y


def ang_solve_test(d, N, P):
    pass


def block_decomp(T, vecs=None):
    d, P, delt = getP(T)
    if vecs is None:
        vecs = blocky_block_vecs(T)    
    data = np.vstack([vecs[0, :], vecs[1:, :], vecs[1:, :]])
    offsets = np.hstack([P[0], -P[1:], d - P[1:]])
    W = sparse.dia_matrix((data, offsets), shape=(d,d))
    A = W.getH() * W
    return A, W

def eigen_block(T):
    
