import numpy as np
import scipy.optimize as opt
import scipy.linalg as spla
import phaseret as pr

# x1 = np.array([[1], [1j]])
# x2 = np.array([[3], [-4 + 2j]])
# mu1 = 17
# mu2 = 9

# A = mu1 * np.dot(x1, x1.T.conj()) + mu2 * np.dot(x2, x2.T.conj())
# w, v = np.linalg.eigh(A)

# v1 = v[:, -1]




def realify(A):
    try:
        (m, n) = A.shape
    except ValueError:
        m = A.shape[0]
        n = 1
    if n == 1:
        x = np.zeros(2 * m)
        x[0::2] = np.real(A).ravel()
        x[1::2] = np.imag(A).ravel()
        return x
    else:
        X = np.zeros((2 * m, 2 * n))
        for j in range(m):
            X[0::2, 2*j] = np.real(A[:, j])
            X[1::2, 2*j] = np.imag(A[:, j])
            X[1::2, 2*j+1] = np.real(A[:, j])
            X[0::2, 2*j+1] = -np.imag(A[:, j])
        return X


def complexify(A):
    try:
        (m, n) = A.shape
    except ValueError:
        m = A.shape[0]
        n = 1
    if n == 1:
        x = A[0::2] + 1j * A[1::2]
        return x
    else:
        X = np.zeros((m / 2, n / 2), dtype='complex')
        for i in range(m / 2):
            X[:, i] = A[0::2, 2*i] + 1j * A[1::2, 2*i]
        return X


def run_opt(A, option='opt', ret_res=False):
    d = A.shape[0]
    X = realify(A)
    constraints = [dict() for i in range(d)]
    for i in range(d):
        constraints[i]['type'] = 'ineq'
        constraints[i]['fun'] = abs_val_cnst
        constraints[i]['args'] = [i]
    fun = obj_fun
    val, v = spla.eigh(A, eigvals=(d-1, d-1))
    args = (X) / val[0]
    v = v.ravel()
    v = v / max(abs(v))
    x0 = v.copy()
    xt = x0.copy()
    for i in np.random.permutation(d):
        x0[i] = sign(np.dot(A[:, i].conj(), x0))
        xt[i] = sign(xt[i])
    xr = np.random.normal(size=2*d)
    xr = xr / max(abs(xr))
    if option == 'eig':
        res = opt.minimize(fun, realify(v), args=args,
                           jac=True, constraints=constraints)
    if option == 'norm':
        res = opt.minimize(fun, realify(x0), args=args,
                           jac=True, constraints=constraints)
    if option == 'zero':
        res = opt.minimize(fun, np.zeros(2*d), args=args,
                           jac=True, constraints=constraints)
    if option == 'random':
        res = opt.minimize(fun, xr, args=args,
                           jac=True, constraints=constraints)
    if option == 'opt':
        res = opt.minimize(fun, realify(xt), args=args,
                                   jac=True, constraints=constraints)
    # return v, x0, xt, eig_res, norm_eig_res, opt_eig_res
    phase = complexify(res.x)
    phase = phase / sign(phase[0])
    if ret_res:
        return phase, res
    return phase


def sign(z):
    if z == 0:
        return 1
    else:
        return z / np.abs(z)


def obj_fun(x, X):
    gradient = -np.dot(X, x)
    fun = np.dot(x, gradient)
    return fun, 2 * gradient


def obj_jac(x, X):
    return -2 * np.dot(X, x)


def abs_val_cnst(x, i):
    return 1 - (x[2*i]**2 + x[2*i+1]**2)
