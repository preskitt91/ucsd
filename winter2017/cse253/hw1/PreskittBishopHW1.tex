%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{nips15submit_e}
\usepackage{bbm, hyperref, float}
\usepackage{graphics, parskip, color}
\usepackage{enumerate}
\usepackage{afterpage}
\usepackage{listings}
\usepackage[margin=0.5in]{geometry}
%\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\newcommand{\sgn}{\ensuremath{\mathrm{sgn}}}
\newcommand{\x}{\ensuremath{\mathbf{x}}}

%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}

\title{CSE 253 Homework \# 1, Book Problems}
\author{Brian Preskitt \\ \href{mailto:bpreskit@ucsd.edu}{bpreskitt@ucsd.edu}}
\date{\today}

\begin{document}
\maketitle

\section{Problem 1.1}
Consider the identities $$\int_{-\infty}^{\infty} \exp(-\frac{\lambda}{2} x^2) \, dx = \left(\frac{2\pi}{\lambda}\right)^{1/2}, \prod_{i=1}^d \int_{-\infty}^{\infty} e^{-x_i^2} \, dx_i = S_d \int_0^{\infty} e^{-r^2} r^{d - 1} \, dr, \Gamma(x) = \int_0^{\infty} u^{x - 1} e^{-u} \, du$$ and prove that $$S_d = \dfrac{2 \pi^{d/2}}{\Gamma(d / 2)}.$$

\textbf{Solution}  Begin by setting $u = r^2$; then $du = 2r\, dr$, so
\begin{align*} \int_0^{\infty} e^{-r^2} r^{d - 1} \, dr &= \frac{1}{2} \int_0^{\infty} e^{-r^2} (r^2)^{d/2 - 1} (2r \, dr) \\
  &= \frac{1}{2} \int_0^{\infty} e^{-u} u^{d / 2 - 1} \, du \\
  &= \frac{1}{2} \Gamma(d / 2),
\end{align*}
so $$\prod_{i = 1}^d \int_{- \infty}^{\infty} e^{- x_i^2} \, dx_i = S_d(\frac{1}{2} \Gamma(d / 2)),$$ so $$S_D = \dfrac{2 (\int_{-\infty}^{\infty} \exp(- \frac{2}{2} x^2) \, dx)^d}{\Gamma(d/2)} = \dfrac{2(\pi^{1/2})^d}{\Gamma(d/2)} = \dfrac{2 \pi^{d/2}}{\Gamma(d/2)},$$ as desired.

Considering that $\Gamma(1) = 1$ and $\Gamma(3/2) = \sqrt{\pi} / 2$, this gives that $S_2 = 2 (\pi^{1/2})^2 / 1 = 2 \pi$ (the circumference of a circle of radius 1) and $S_3 = 2 (\pi^{1/2})^3 / (\sqrt{\pi} / 2) = 4 \pi$ (the surface area of the sphere in $\R^3$ of radius 1).

\section{Problem 1.2}
Using this result, show that $$V_d = \dfrac{S_d a^d}{d},$$ where $V_d$ is the volume of a hypersphere of radius $a$ in $d$ dimensions.  Hence show that the ratio between the volume of this sphere and the volume of a hypercube of side length $2a$ is given by $$\dfrac{V_d}{C_d} = \dfrac{\pi^{d / 2}}{d 2^{d - 1} \Gamma(d / 2)}.$$  Using the approximation $$\Gamma(x + 1) \approxeq (2\pi)^{1/2} e^{-x} x^{x + 1/2},$$ show that, as $d \to \infty$, this ratio goes to 0.  Similarly, show that the ratio between the distance from the center of the hypercube to a corner and the (orthogonal) distance from its center to any side is $\sqrt{d}$ and goes to $\infty$ as $d \to \infty$.

\textbf{Solution}
Well, we begin by observing that $$V_d = \int_0^a S_d r^{d - 1} \, dr = \frac{1}{d} a^d S_d = \dfrac{S_d a^d}{d}$$ as desired, where we have used the fact that $S_d$ is a $d - 1$ dimensional manifold and thus has its area stretched by a factor of $t^{d - 1}$ when it is transformed by $\x \mapsto t \x$.  Since the volume of the hypercube of side length $2a$ is immediately $C_d = (2a)^d$, we have $$\dfrac{V_d}{C_d} = \dfrac{S_d a^d / d}{(2a)^d} = \dfrac{2\pi^{d / 2} a^d}{d \cdot 2^d \cdot a^d \cdot \Gamma(d/2)} = \dfrac{\pi^{d / 2}}{d 2^{d - 1} \Gamma(d / 2)},$$ as claimed.  With the approximation listed above, we may estimate that $$\lim_{d \to \infty} \dfrac{V_d}{C_d} = \lim_{d \to \infty} \dfrac{\pi^{d / 2}}{d 2^{d - 1} (2 \pi)^{1 / 2} e^{-d / 2} (d/2)^{d/2 + 1/2}} = 0,$$ as the ratio is dominated by the $O(d^d)$ term in the denominator.

The distance from the center of the hypercube to a corner is $$||\mathbbm{1}|| = \sqrt{\sum_{i = 1}^d 1^2} = \sqrt{d},$$ whereas the perpendicular distance to any side is just $$||e_i|| = 1,$$ so the ratio is immediately $\sqrt{d}$, which trivially tends to infinity as $d \to \infty$.

\section{Problem 1.3}
Consider a sphere of radius $a$ in $d$ dimensions.  Use the result from the previous problem to show that the fraction of the volume of the sphere which lies at values of the radius between $a - \epsilon$ and $a$, where $0 < \epsilon < a$, is given by $$f = 1 - (1 - \frac{\epsilon}{a})^d.$$  Hence show that, for any fixed $\epsilon, \lim_{d \to \infty} f = 1$.  Evaluate the ratio numerically, with $\epsilon / a - 0.01$, for $d = 2, 10, 1000$.  Similarly, evaluate the fraction of the volume of the sphere lying within a radius of $a / 2$, again for $d = 2, 10, 1000$.

\textbf{Solution}
Letting $V_d(r)$ stand for the volume of a sphere in $d$ dimensions of radius $r$, we have that $$f = \dfrac{V_d(a) - V_d(a - \epsilon)}{V_d(a)} = \dfrac{(S_d / d)(a^d - (a - \epsilon)^d)}{(S_d / d) a^d} = 1 - (1 - \frac{\epsilon}{a})^d,$$ as claimed.  Certainly, then, for any $0 < \epsilon < a$, we have $0 < 1 - \frac{\epsilon}{a} < 1$, so indeed $\lim_{d \to \infty} 1 - (1 - \frac{\epsilon}{a})^d = 1$.  The results of the numerical calculations are summarized in table \ref{tab:shellvol}
\begin{table}[H]
  \begin{center}
    \begin{tabular}{|r|rr|}
      \hline $d$ & $f$, with $\epsilon / a = 0.01$ & $V_d(a/2) / V_d(1)$ \\ \hline
      2 & 1.99\% & 0.25 \\
      10 & 9.56\% & $9.77 \times 10^{-4}$ \\
      1000 & 99.9957\% & $9.3326 \times 10^{-302}$ \\ \hline
    \end{tabular}
  \end{center}
  \caption{Volumes of spherical shells over dimension}
  \label{tab:shellvol}  
\end{table}

\section{Problem 1.4}
Consider a probability density function $p(\x)$ in $\R^d$ of the form $$p(\x) = \dfrac{1}{(2 \pi \sigma^2)^{1/2}} \exp\left(-\dfrac{|| \x ||^2}{2 \sigma^2} \right).$$  Show that the probability mass inside a thin shell of radius $r$ and thickness $\epsilon$ is given by $\rho(r)\epsilon$ where $$\rho(r) = \dfrac{S_d r^{d-1}}{(2 \pi \sigma^2)^{1/2}} \exp\left(- \frac{r^2}{2 \sigma^2}\right).$$  Show that $\rho(r)$ has a single maximum, which, for large values of $d$, is located at $\hat{r} \approxeq \sqrt{d} \sigma.$  Finally, by considering $\rho(\hat{r} + \epsilon)$ where $\epsilon << \hat{r}$ show that for large $d$ $$\rho(\hat{r} + \epsilon) = \rho(\hat{r}) \exp\left(-\frac{3 \epsilon^2}{2 \sigma^2}\right).$$  Furthermore, show that the ratio between the probability density here is smaller by a factor of $\exp(d/2)$ than the density at the origin.

\textbf{Solution}
Well, the volume of the $\epsilon$-shell at a radius of $\epsilon$ is roughly $\epsilon \cdot S_d \cdot r^{d - 1}$ -- this comes by using the fact that the surface area of the sphere at this radius is $S_d r^{d - 1}$ (again, because the sphere is a $d - 1$ dimensional manifold) and approximating the volume by multiplying the area with the thickness.  This approximation holds for $\epsilon << r$.  Then, again for $\epsilon << r$, we may approximate the probability density as being constant over this shell, since the radius is approximately constant.  Therefore, this gives a total probability mass of $$(\text{Volume}) \times (\text{Prob. Density}) = (S_dr^{d - 1} \epsilon) \cdot (\dfrac{1}{(2 \pi \sigma^2)^{1/2}} \exp\left(-\dfrac{r^2}{2 \sigma^2} \right)) = \rho(r) \epsilon,$$ using $\rho$ as defined above.

To find the maximum of $\rho(r)$, we merely take the derivative: $$\frac{d \rho}{d r} = \left(\dfrac{S_d}{(2 \pi \sigma^2)^{1/2}} \exp(-\frac{r^2}{2\sigma^2})\right)((d-1) r^{d - 2} - \frac{r^d}{\sigma^2}),$$ and we immediately find that this is zero when $r = \sqrt{d - 1}\sigma \approxeq \sqrt{d}\sigma$ for large values of $d$.
We then consider that \begin{align*}
  \rho(\hat{r} + \epsilon) &= \dfrac{S_d (\hat{r} + \epsilon)^{d-1}}{(2 \pi \sigma^2)^{1/2}} \exp(-\frac{(\hat{r} + \epsilon)^2}{2 \sigma^2}) \\
  &\approxeq \dfrac{S_d (\hat{r})^{d-1}}{(2 \pi \sigma^2)^{1/2}}\exp(-\frac{\hat{r}^2}{2 \sigma^2}) \exp(-\frac{2 \hat{r} \epsilon + \epsilon^2}{2 \sigma^2}) \\
  &= \rho(\hat{r})\exp(-\frac{2\sqrt{d}\sigma \epsilon + \epsilon^2}{2 \sigma^2}) \\
  &\approxeq \rho(\hat{r}) \exp(-\frac{\sqrt{d} \epsilon}{\sigma}),
\end{align*} which doesn't reflect the stated result, but is actually somewhat stronger...  There is exponential decay in $\epsilon$ (rather than $\epsilon^2$), and the length scale is $\sigma / \sqrt{d} << \sigma!$

We now consider that the probability density at the origin is given by $$p(0) = \dfrac{1}{(2 \pi \sigma^2)^{1/2}} \ \text{while} \ p(\hat{r}) = \dfrac{1}{(2 \pi \sigma^2)^{1/2}} \exp(- \frac{(\sqrt{d} \sigma)^2}{2 \sigma ^2}) = \dfrac{1}{(2 \pi \sigma^2)^{1/2}} \exp(-\frac{d}{2}) = p(0) \cdot \exp(-\frac{d}{2})$$ as claimed.





\end{document}
