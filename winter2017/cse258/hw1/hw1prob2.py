# Imports
import numpy as np
import pickle

# Unpickle the data
datdir = '/path/to/data/pickles/'
winfil = datdir + 'winedat.pkl'
with open(winfil, 'rb') as file:
    dat = pickle.load(file)

# Turn the data into a matrix (instead of list of Dict)
keys = list(dat[0].keys())
obj = 'quality'                 # Move the 'objective feature'
keys.remove(obj)                # to the end of the matrix
keys.append(obj)
X = np.array([[r[k] for k in keys] for r in dat])

# Size parameters
N = X.shape[0]
Ntr = round(N / 2)
Nts = N - Ntr
L = max([len(s) for s in keys])
nf = X.shape[1]

# Separate training (tr*) and test (ts*) sets
trd = X[:Ntr,:]
tsd = X[Ntr:,:]

# Produce linear feature matrix -- perform linear regression and report.
At = np.hstack([np.ones([Ntr, 1]), trd[:,:-1]])
bt = trd[:,-1:None]
tht = np.linalg.lstsq(At, bt)
coefs = tht[0].ravel()
print('===== ALL FEATURES, LINEAR =====')
for i, feat in enumerate(keys):
    if feat != obj:
        print('Coef. for {0} = {1: 5e}'.format(feat.ljust(L), coefs[i]))
    
x = tht[0]

# Use model to make predictions on the test data
As = np.hstack([np.ones([Nts, 1]), tsd[:,:-1]])
bs = tsd[:,-1:None]
r = np.dot(As, x) - bs
mse = [sum(r**2) / Nts]
print('Train MSE = {0:.5f}'.format(tht[1][0] / Ntr))
print("Test MSE  = {0:.5f}".format(mse[0][0]))

# Run an ablation loop
print('===== ABLATION RESULTS =====')
for feat in np.arange(1, nf):
    rm = keys[feat - 1]             # Feature to be removed
    Ap = np.delete(At, feat, 1)     # Matrix with feature deleted
    theta = np.linalg.lstsq(Ap, bt) # Regression coefficients
    Atp = np.delete(As, feat, 1)    # Delete feature from test data
    r = np.dot(Atp, theta[0]) - bs  # Calculate residual...
    mse += [sum(r**2) / Nts]        # ...and MSE
    print('Removing {0} MSE\t: {1}'.format(rm.ljust(L), mse[-1]))
