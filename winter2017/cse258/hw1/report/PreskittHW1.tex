%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol, minted}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathcal{F}}}
\newcommand{\mg}{martingale}
\newcommand{\E}{\ensuremath{\mathbb{E}}}
\newcommand{\rv}{random variable}


%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{CSE 258 Assignment \#1 Report}
\author{Brian Preskitt}
\date{\today}

\begin{document}
\maketitle

\textbf{Problem 1}  I trained a predictor on the \textbf{50,000 beer reviews} data provided that attempted to predict the rating of each beer based on the year of the review.  Letting $r_i$ be the rating given by review $i$ and $y_i$ the years since 1999 in which review $i$ was written, I attempted to make the prediction model $$r_i \approx \theta_0 + \theta_1 y_i,$$ where $\theta_j$ are the coefficients learned by our model.

Doing a basic least-squares solve on this system, I obtained an MSE of $$(MSE) = \frac{1}{N} ||r - Y\theta||_2^2 = 0.49004,$$ where $r, \theta$, and $Y$ are the $r_i, \theta_j$, and $y_i$ values appropriately vectorized, with $Y$ including an appended constant column.  Given that the $r$ values are all between $0$ and $5$, this result is not good -- it implies that the predictions are typically off by $\sqrt{MSE} \approx 0.7$ stars on each prediction, which one could likely obtain by always guessing the mean rating (assuming that the ratings tend to be somewhat concentrated at the mean).  Indeed, the standard deviation of the ratings is $\approx 0.7$, so this definitely hasn't beaten a trivial predictor.  The coefficients yielded by the computation are $$\theta_0 = 3.6838 \ \text{and} \ \theta_1 = 0.02144,$$ ($\theta_1$ is expected to be small, since it scales ``years elapsed since 1999'') which shows that the constant feature was far more important than the year feature -- indeed, since the latest review was in 2012, the highest impact the $\theta_1$ term had on the prediction was $13 \theta_1 < 0.28$ stars.

\textbf{Problem 2}  This model may be unrealistic because it supposes that the ratings change linearly with time -- intuition does not support this idea, so one might try to add some polynomial or non-linear transformations of the year variable to see if the ratings vary in some other way over time.  In particular, I tried fitting the ratings by $$r_i = \theta_0 + \theta_1 y_i + \theta_2 y_i^2 + \theta_3 y_i^3 + \theta_4 \ln(y_i + 1) + \theta_5 \frac{1}{y_i + 1},$$ ($y_i$ is shifted for non-linear features to avoid NaN) but to no avail.  The MSE dropped only to $0.48973$, a roughly $0.2\%$ improvement.

Just for fun, I added several (uniformly distributed on $[0, 1)$) random features, and continued to see no help -- after adding 50 random vectors to $Y$, I was left with an MSE of $0.48919$, and with 500 random vectors, I had $0.48446$, but at that point I've added 500 dimensions to a previously 6 dimensional linear space, which is major cheating, especially for such measely results.

The commands for and output of my experiments are reproduced below:

\begin{minted}{python}
import hw1prob1
Basic MSE =  [ 0.49004382]
Basic coefs =  [[[ 0.02143798]]

 [[ 3.68377021]]]
Extended Feature MSE =  [ 0.48972691]
Random Feature MSE =  [ 0.48919223]
High-dim Random MSE =  [ 0.48446014]
\end{minted}

The code for \texttt{hw1prob1.py} is included below:

\inputminted[linenos]{python}{../hw1prob1.py}

\pagebreak

\textbf{Problem 3-4}  Next, I train a regressor to predict the quality of white wine based on the various chemical qualities provided in the \emph{UCI Wine Quality Dataset}.  After this, I ran an ablation routine to see which features offered the greatest information on the quality of the wines; as seen in the results of the experiment, the greatest positive changes in MSE were seen when removing ``volatile acidity,'' and ``alcohol,'' while the greatest \emph{negative} changes were seen when removing ``density'' and ``residual sugar.''

At first, it seems hard to explain how removing features would help the MSE of the regressor -- you are only decreasing the column space of your feature matrix, which should make it hard to fit the data well.  However, it's important to emphasize that the MSE's reported in the ablation chart are calculated on the \emph{test set}.  Therefore, one way to account for this would be to suppose that the features whose ablation \emph{improved the results} are somehow ineffective features.  My guess is that they contribute to an overfitting on the training data that hurts performance on the test set.

On the other hand, the features whose removal increased the MSE (the model does worse without them) can somewhat confidently be thought of as important features -- taking them out degrades the model's generalization.

One might conclude from this experiment that ``alcohol'' and ``volatile acidity'' were the strongest features in predicting ``quality,'' while ``density'' and ``residual sugar'' were the least consistently correlated features.

Commands to run the experiment and the results are summarized below:

\begin{minted}{python}
  >>> import hw1prob2
  ===== ALL FEATURES, LINEAR =====
  Coef. for chlorides            =  2.564203e+02
  Coef. for fixed acidity        = -2.767751e-01
  Coef. for pH                   =  1.354213e-01
  Coef. for total sulfur dioxide =  1.195406e+00
  Coef. for sulphates            =  3.850240e-05
  Coef. for density              =  8.330063e-01
  Coef. for citric acid          = -2.586528e+02
  Coef. for free sulfur dioxide  =  1.026512e-01
  Coef. for volatile acidity     =  6.343322e-03
  Coef. for alcohol              = -1.729949e+00
  Coef. for residual sugar       =  9.793044e-02
  Train MSE = 0.60231
  Test MSE  = 0.56246
  ===== ABLATION RESULTS =====
  Removing chlorides            MSE	: [ 0.56262927]
  Removing fixed acidity        MSE	: [ 0.55911341]
  Removing pH                   MSE	: [ 0.55956663]
  Removing total sulfur dioxide MSE	: [ 0.56242901]
  Removing sulphates            MSE	: [ 0.55734635]
  Removing density              MSE	: [ 0.54472655]
  Removing citric acid          MSE	: [ 0.5622217]
  Removing free sulfur dioxide  MSE	: [ 0.55614082]
  Removing volatile acidity     MSE	: [ 0.59638485]
  Removing alcohol              MSE	: [ 0.57321474]
  Removing residual sugar       MSE	: [ 0.55362506]
\end{minted}

The code in \texttt{hw1prob2.py} is supplied below:

\inputminted[linenos]{python}{../hw1prob2.py}

\pagebreak

\textbf{Problem 5} An SVM classifier (as implemented in \texttt{sklearn.svm}) was used to classify the wine data.  After a few trial and error iterations (looking at classification accuracy on both the train and test data sets), I found that using the default complexity parameter of $C = 1$ yielded among the best results I saw.  The other parameters were left as is.  The support vector machine managed to classify the training data with 80.3\% accuracy, and the test data with 76.2\% accuracy.  These results aren't entirely optimistic -- these suggest to me that the data is not well-suited for linear separation.

Commands and results summarized below:

\begin{minted}{python}
  >>> import winclass
  Train Acc =  0.803184973459
  Test  Acc =  0.76194365047
\end{minted}

The code in \texttt{winclass.py}:

\inputminted[linenos]{python}{../winclass.py}

\pagebreak

\textbf{Problem 6}  The same data was classified based on a logistic regression model trained through gradient descent on the training data.  The \texttt{scipy.optimize.fmin\_l\_bfgs\_b} routine was used to minimize the log-likelihood of the labels on the training data conditioned on the model, according to the example demonstrated in class.

The code to calculate the log-likelihood and its gradient was revised for efficiency.  For example, operations were vectorized when possible -- while this may not have contributed a huge speedup in our case, larger datasets (and/or datasets with more features) can incur high computational cost if you don't use \texttt{numpy} operations for, say, matrix-vector multiplies.

Additionally, the \texttt{scipy} optimization function permits you to submit an objective routine that returns both the objective function and its gradient at a certain point, which helps in our case, because both $l_\theta$ and $\nabla l_\theta$ require a matrix-matrix multiply involving the entire dataset -- one can cut these operations down by 50\% by computing the product once and using it for both calculations.

Furthermore, the formulae for $l_\theta$ and $\nabla l_\theta$ were revised by considering the following derivation, where 
\begin{itemize}
\item $y_i \in \{0, 1\}$ is the label of the $i^{\text{th}}$ data point.
  \item $X_i$ is the feature vector of the $i^{\text{th}}$ data point.
  \item $\theta$ is the current estimate of the weight/model vector.
    \item $\sigma(z) = \frac{1}{1 + e^{-z}}$ is the sigmoid function
\end{itemize}
\begin{align*}
  l_\theta(y | X) &= \sum_{y_i = 1} \ln \sigma(\theta \cdot X_i) + \sum_{y_0 = 1} \ln (1 - \sigma(\theta \cdot X_i)) \\
  &= \sum_{i=1}^N \ln ((1 - y_i) + (-1)^{y_i + 1} \sigma(\theta \cdot X_i)).
\end{align*}
Considering the equivalent expression obtained in the lecture slides $$l_\theta(y | X) = \sum_i -\ln(1 + e^{-X_i \cdot \theta}) - \sum_{y_i = 0} X_i \cdot \theta,$$ one can compute the gradient as 
\begin{align*}
  \nabla_\theta l_\theta(y | X) &= \sum_i \dfrac{X_i e^{-X_i \cdot \theta}}{1 + e^{- X_i \cdot \theta}} - \sum_{y_i = 0} X_i \\
  &= \sum_{y_i = 1} X_i \dfrac{e^{-X_i \cdot \theta}}{1 + e^{-X_i \cdot \theta}} - \sum_{y_i = 0} X_i \dfrac{1}{1 + e^{-X_i \cdot \theta}} \\
  &= \sum_{y_i = 1} X_i \sigma(-X_i \cdot \theta) - \sum_{y_i = 0} X_i \sigma(X_i \cdot \theta) \\
  &= \sum_{y_i = 1} X_i (1 - \sigma(X_i \cdot \theta)) - \sum_{y_i = 0} X_i \sigma(X_i \cdot \theta) \\
  &= \sum_i X_i (y_i - \sigma(X_i \cdot \theta)),
\end{align*}
which is the formula used in my code.

After running the optimization, a log-likelihood of 1318.491 was obtained, and the classification accuracy on the test set was 75.2\%.

\textbf{Remark}  Because the test set happened to be classifying more accurately than the training set, I excluded the regularization parameters -- I would also have done this in the case that I was using a validation set to determine whether regularization was necessary, because this indicates that the model is \emph{not overfitting}.  Playing a little with $L_1$ regularization showed a slight ($< 0.5\%$) improvement in the test classification accuracy for a value of $\lambda_1 = 0.001$, but I would attribute this to a statistical coincidence rather than a bad case of overfitting.  Therefore, the regularization terms were excluded from the gradient derivation provided above -- they are included here for completeness:%
\begin{align*}
  \nabla ||\theta||_2^2 = \nabla \sum_i \theta_i^2 &= 2\theta \\
  \nabla ||\theta||_1 = \nabla \sum_i |\theta_i| &= \sum_{\theta_i > 0} e_i - \sum_{\theta_i < 0} e_i
\end{align*}

The commands for and results of the experiment are included below:

\begin{minted}{python}
  >>> import winlog
  ===== Logistical Regression Classifier =====
  Training Classification Accuracy:  0.719477337689
  Test Set Classification Accuracy:  0.752143732136
  Log-Likelihood at Convergence   :  1318.49119839
\end{minted}

The code in \texttt{winlog.py} is supplied here:

\inputminted[linenos]{python}{../winlog.py}

\end{document}
