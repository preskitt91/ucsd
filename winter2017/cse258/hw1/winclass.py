# Imports
import numpy as np
import pickle
import scipy.optimize as opt
import sklearn
from sklearn import svm

# Unpickle the data
datdir = '/path/to/data/pickles/'
winfil = datdir + 'winedat.pkl'
with open(winfil, 'rb') as file:
    dat = pickle.load(file)

# Turn the data into a matrix (instead of list of Dict)
keys = list(dat[0].keys())
obj = 'quality'                 # Move the 'objective feature'
keys.remove(obj)                # to the end of the matrix
keys.append(obj)
X = np.array([[r[k] for k in keys] for r in dat])

# Size parameters
N = X.shape[0]
Ntr = round(N / 2)
Nts = N - Ntr

# Separate training ('*t') and test ('*s') sets
Xt = X[:Ntr,:]
Xs = X[Ntr:,:]

# Pluck off the objective feature and form labels
trd = Xt[:,:-1]
trl = np.where(Xt[:,-1] > 5, 1, -1)
tsd = Xs[:,:-1]
tsl = np.where(Xs[:,-1] > 5, 1, -1)

# Preprocess the data, according to sklearn User Guide online
# (SVM is not scale-invariant, so normalize to zero-mean, unit variance)
scaler = sklearn.preprocessing.StandardScaler().fit(trd)
trd = scaler.transform(trd)
tsd = scaler.transform(tsd)

# Define classifier object and train
clf = svm.SVC(C=1.0)            # C-value was chosen through trial and error
clf.fit(trd, trl)

# Come up with predictions...
trp = clf.predict(trd)
tsp = clf.predict(tsd)

# Calculate accuracy and present results!
tracc = sum(trp == trl) / Ntr
tsacc = sum(tsp == tsl) / Nts
print('Train Acc = ', tracc)
print('Test  Acc = ', tsacc)
