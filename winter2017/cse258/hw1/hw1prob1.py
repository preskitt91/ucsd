import numpy as np
import pickle

datdir = '/path/to/data/pickles/'
beerfil = datdir + 'beerdat.pkl'

# I've pickled my data for easy reading
with open(beerfil, 'rb') as file:
    dat = pickle.load(file)

# Extract the 'year' entries!
x = np.array([a[b'review/timeStruct'][b'year'] for a in dat])
# Turn into a vector and 'normalize' to help condition number
# This will be okay because a constant subtract is in the span of
# our constant "bias" feature.
x = x[:,None] - min(x)
N = len(x)

# Extract the 5-star review values
b = np.array([a[b'review/overall'] for a in dat])
b = b[:,None]

# Come up with our feature matrix and solve!
A = np.hstack([x, np.ones(x.shape)])
theta = np.linalg.lstsq(A, b)
print('Basic MSE = ', theta[1] / N)
print('Basic coefs = ', theta[0][:,None])

# Since the MSE stinks, we'll try adding some polynomial
# features.  Heck, why not some non-linear terms as well?
Ap = np.hstack([A, x**2, x**3, np.log(x + 1), 1 / (x + 1)])
thetap = np.linalg.lstsq(Ap, b)
print('Extended Feature MSE = ', thetap[1] / N)

# Since the MSE still stinks, let's add a bunch of random features.
# That will show 'em!  (~ 0.1% of data dimension)
Ar = np.hstack([Ap, np.random.rand(N, round(N / 1000))])
thetar = np.linalg.lstsq(Ar, b)
print('Random Feature MSE = ', thetar[1] / N)

# ...Still not much improvement.  Let's add moar.
Am = np.hstack([Ar, np.random.rand(N, round(N / 100 - N / 1000))])
thetam = np.linalg.lstsq(Am, b)
print('High-dim Random MSE = ', thetam[1] / N)
