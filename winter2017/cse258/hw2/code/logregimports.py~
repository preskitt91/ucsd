# Imports
import numpy as np
import pickle
import scipy.optimize as opt
import sklearn
from sklearn import svm

# Unpickle the data
datdir = '/path/to/data/pickles/'
winfil = datdir + 'winedat.pkl'
with open(winfil, 'rb') as file:
    dat = pickle.load(file)

# Turn the data into a matrix (instead of list of Dict)
keys = list(dat[0].keys())
obj = 'quality'                 # Move the 'objective feature'
keys.remove(obj)                # to the end of the matrix
keys.append(obj)
X = np.array([[r[k] for k in keys] for r in dat])

# Size parameters
N = X.shape[0]
Ntr = round(N / 2)
Nts = N - Ntr
nfeat = X.shape[1]

# Separate training ('*t') and test ('*s') sets
Xt = X[:Ntr,:]
Xs = X[Ntr:,:]
Xt = np.hstack([np.ones((Ntr, 1)), Xt])
Xs = np.hstack([np.ones((Nts, 1)), Xs])

# Pluck off the objective feature and form labels
trd = Xt[:,:-1]
trl = np.where(Xt[:,-1] > 5, 1, 0)
tsd = Xs[:,:-1]
tsl = np.where(Xs[:,-1] > 5, 1, 0)

# Normalize the data (this keeps from starting with a crazy log-likelihood)
div = np.max(trd, 0)
trd = trd / div
tsd = tsd / div

# Regularization parameters (not found to be useful here)
lam1 = 0.
lam2 = 0.

# Basic sigmoid calculation
def sigmoid(x):
    return 1.0 / (1 + np.exp(-x))

# NEGATIVE Log-likelihood
def loglike(theta, X, y, lam1=0., lam2=0., gradOnly=False):
    # Formulae justified in report
    mask = np.where((y == 0), -1, 1)
    offs = np.where((y == 0), 1, 0)
    xt = np.dot(X, theta[:,None]).ravel()
    sig = sigmoid(xt)

    # THIS IS MY FPRIME (done this way to reduce 'xt' mat-mat multiplies)
    grad = -np.sum(X * (y - sig)[:,None], 0)

    # Regularization terms
    if lam1 > 0:
        grad += lam1 * np.where(theta > 0, 1, -1)
    if lam2 > 0:
        grad += 2 * lam2 * theta
    if not gradOnly:
        ll = -np.sum(np.log(offs + mask * sig))
        return ll, grad
    else:
        return grad

# Classification accuracy
def classAcc(theta, X, y):
    pred = np.where(np.dot(X, theta) > 0, 1, 0)
    acc = sum(pred == y) / len(y)
    return acc

# Initialize theta (seemed to help the scipy.optimize routine)
def initTheta(theta, X, y, lam1, lam2, eta=0.01, its=16):
    # Calculate the gradient and accumulate
    for i in range(its):
        g = loglike(theta, X, y, lam1, lam2, gradOnly=True)
        theta = theta - eta * g
    return theta
