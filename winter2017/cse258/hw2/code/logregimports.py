# Imports
import numpy as np
import pickle
import scipy.optimize as opt
import sklearn
from sklearn import svm

# Basic sigmoid calculation
def sigmoid(x):
    return 1.0 / (1 + np.exp(-x))

# NEGATIVE Log-likelihood
def loglike(theta, X, y, lam1=0., lam2=0., gradOnly=False):
    # Formulae justified in report
    mask = np.where((y == 0), -1, 1)
    offs = np.where((y == 0), 1, 0)
    xt = np.dot(X, theta[:,None]).ravel()
    sig = sigmoid(xt)

    # THIS IS MY FPRIME (done this way to reduce 'xt' mat-mat multiplies)
    grad = -np.sum(X * (y - sig)[:,None], 0)

    # Regularization terms
    if lam1 > 0:
        grad += lam1 * np.where(theta > 0, 1, -1)
    if lam2 > 0:
        grad += 2 * lam2 * theta
    if not gradOnly:
        ll = -np.sum(np.log(offs + mask * sig))
        return ll, grad
    else:
        return grad

# Classification accuracy
def classAcc(theta, X, y):
    pred = applyModel(theta, X, y)
    acc = sum(pred == y) / len(y)
    return acc

def applyModel(theta, X, y):
    pred = np.where(np.dot(X, theta) > 0, 1, 0)
    return pred

def modelPerformance(theta, X, y):
    pred = applyModel(theta, X, y)
    tp = sum((pred == 1) * (y == 1))
    fp = sum((pred == 1) * (y == 0))
    tn = sum((pred == 0) * (y == 0))
    fn = sum((pred == 0) * (y == 1))
    fpr = fp / (tp + fn)
    fnr = fn / (tn + fp)
    ber = 1/2 * (fpr + fnr)
    return tp, fp, tn, fn, ber

# Initialize theta (seemed to help the scipy.optimize routine)
def initTheta(theta, X, y, lam1, lam2, eta=0.01, its=16):
    # Calculate the gradient and accumulate
    for i in range(its):
        g = loglike(theta, X, y, lam1, lam2, gradOnly=True)
        theta = theta - eta * g
    return theta

def regTrain(data, labels, lam1, lam2, init='zeros'):
    Nf = data.shape[1]
    if init == 'zeros':
        tht0 = initTheta(np.zeros(Nf), data, labels, lam1, lam2)
    else:
        tht0 = initTheta(np.zeros(Nf), data, labels, lam1, lam2)        
    x, f, d = opt.fmin_l_bfgs_b(loglike, tht0, args=(data, labels, lam1, lam2))
    return x, f, d

def precision_recall(labels, k):
    relevant_retrieved = sum(labels[:k])
    relevant = sum(labels)
    retrieved = k

    precision = relevant_retrieved / k
    recall = relevant_retrieved / relevant
    return precision, recall
