# Imports
import numpy as np
import pickle
import scipy.optimize as opt
import sklearn
from importlib import reload
import logregimports as lr
reload(lr)
from sklearn.decomposition import PCA
from matplotlib import pyplot as plt

# Unpickle the data
datdir = 'data/'
winfil = datdir + 'winedat.pkl'
with open(winfil, 'rb') as file:
    dat = pickle.load(file)

# Turn the data into a matrix (instead of list of Dict)
keys = list(dat[0].keys())
obj = 'quality'                 # Move the 'objective feature'
keys.remove(obj)                # to the end of the matrix
keys.append(obj)
X = np.array([[r[k] for k in keys] for r in dat])
N = X.shape[0]

# Shuffle the data
p = np.random.permutation(N)
X = X[p,:]

# Size parameters
Nt = round(N / 3)               # Training set
Nv = Nt                        # Validation set
Ns = N - Nt - Nv                # Test set

# Divvy up the data
Xt = X[:Nt,:]
Xv = X[Nt:(Nt + Nv),:]
Xs = X[(Nt + Nv):,:]

# Pluck off the objective feature and form labels
td = Xt[:,:-1]
bt = Xt[:,-1:None]
tl = np.where(Xt[:,-1] > 5, 1, 0)
vd = Xv[:,:-1]
bv = Xv[:,-1:None]
vl = np.where(Xv[:,-1] > 5, 1, 0)
sd = Xs[:,:-1]
bs = Xs[:,-1:None]
sl = np.where(Xs[:,-1] > 5, 1, 0)
Nf = td.shape[1]

# Subtract the mean and use this to calculate "reconstruction error"
# from the compression of "replace with mean"
zero_mean_data = td - np.mean(td, 0)
reconst_err = sum([a**2 for a in zero_mean_data.flatten()])
print('===== Reconstruction Error for "compression" with mean =====')
print('Reconstruction Error:  {0}'.format(reconst_err))

# Create and fit a PCA model, report the components
pca = PCA()
model = pca.fit(td)
print('===== PCA Components on Training Data =====')
print(model.components_)

# Use the singular values to calculate the reconstruction error incurred from
# replacing the data with only a few dimensions
svds = np.linalg.eigvalsh(np.dot(zero_mean_data.T, zero_mean_data))
print('===== Reconstruction Error under 4 Dimensions =====')
print('Reconst. Error    : {0}'.format(sum(svds[:-4])))

# Transform the data for linear fitting
T_pca = model.transform(td)
V_pca = model.transform(vd)
S_pca = model.transform(sd)

# Allocate memory for the MSE results
train_mse = np.empty(Nf + 1)
test_mse = np.empty(Nf + 1)

# Calculate and report the regression results!
print('==============================================')
print('====== PCA Regression, Num. Dimensions =======')
print('==============================================')
print('| Num. Dims | Train MSE | Test MSE |')
for dims_kept in range(0, Nf+1):
    At = np.hstack([np.ones([Nt, 1]), T_pca[:,:dims_kept]])
    As = np.hstack([np.ones([Ns, 1]), S_pca[:,:dims_kept]])
    tht = np.linalg.lstsq(At, bt)
    coefs = tht[0]
    train_mse[dims_kept] = tht[1][0] / Nt
    test_mse[dims_kept] = sum(r**2 for r in (np.dot(As, coefs) -
                                             bs).flatten()) / Ns
    print('| {0:9d} | {1} | {2} |'
          ''.format(dims_kept,
                    '{0:.4f}'.format(train_mse[dims_kept]).rjust(9),
                    '{0:.4f}'.format(test_mse[dims_kept]).rjust(8)))

# Make a plot of all the MSE
dims_kept = np.arange(0, Nf+1)
plt.figure(1)
lines = plt.plot(dims_kept, train_mse, 'r--', dims_kept, test_mse, 'b--')
plt.legend(lines, ('Train MSE', 'Test MSE'), loc=0)
plt.title('MSE vs. PCA Dimensions Used (of 11)')
plt.xlabel('k (Dimensions Used)')
plt.ylabel('MSE')
plt.show()
plt.clf()
