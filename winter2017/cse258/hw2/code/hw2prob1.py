# Imports
import numpy as np
import pickle
import scipy.optimize as opt
import sklearn
from importlib import reload
import logregimports as lr
reload(lr)
from sklearn import svm
from matplotlib import pyplot as plt

# Unpickle the data
datdir = 'data/'
winfil = datdir + 'winedat.pkl'
with open(winfil, 'rb') as file:
    dat = pickle.load(file)

# Turn the data into a matrix (instead of list of Dict)
keys = list(dat[0].keys())
obj = 'quality'                 # Move the 'objective feature'
keys.remove(obj)                # to the end of the matrix
keys.append(obj)
X = np.array([[r[k] for k in keys] for r in dat])

# Add constant feature 
N = X.shape[0]
X = np.hstack([np.ones((N, 1)), X])

# Shuffle the data
p = np.random.permutation(N)
X = X[p,:]

# Size parameters
Nt = round(N / 3)               # Training set
Nv = Nt                        # Validation set
Ns = N - Nt - Nv                # Test set

# Divvy up the data
Xt = X[:Nt,:]
Xv = X[Nt:(Nt + Nv),:]
Xs = X[(Nt + Nv):,:]

# Pluck off the objective feature and form labels
td = Xt[:,:-1]
tl = np.where(Xt[:,-1] > 5, 1, 0)
vd = Xv[:,:-1]
vl = np.where(Xv[:,-1] > 5, 1, 0)
sd = Xs[:,:-1]
sl = np.where(Xs[:,-1] > 5, 1, 0)
Nf = td.shape[1]

# Normalize the data (this keeps from starting with a crazy log-likelihood)
div = np.max(td, 0)
td = td / div
vd = vd / div
sd = sd / div

# Regularization parameters
lams = [0, 0.01, 1.0, 100]
lam1 = 0.
lam2 = 0.

for lam in lams:
    # Initialize theta and train the model!
    x, f, _ = lr.regTrain(td, tl, lam1, lam)

    # Calculate performance

    # Calculate and print classification accuracy
    print('===== Logistical Regression Classifier, Lambda = {0:.3f} ====='
          ''.format(lam))
    print('Training Classification Accuracy: '
          '{0:.2f}%'.format(100 * lr.classAcc(x, td, tl)))
    print('Valid.   Classification Accuracy: '
          '{0:.2f}%'.format(100 * lr.classAcc(x, vd, vl)))
    print('Test Set Classification Accuracy: '
          '{0:.2f}%'.format(100 * lr.classAcc(x, sd, sl)))
    print('Log-Likelihood at Convergence   : '
          '{0:.2f}'.format(f))

# Here we fix the lambda value and train the model.n
lam2 = 0.01
x, f, _ = lr.regTrain(td, tl, lam1, lam2)

# Calculate and report "true/false positives/negatives"
tp, fp, tn, fn, ber = lr.modelPerformance(x, sd, sl)
print('===== Results on Test Data, Lambda = {0:.2E} ===='.format(lam2))
print('Number of points   : ', len(sl))
print('True  Positives    : ', tp)
print('False Positives    : ', fp)
print('True  Negatives    : ', tn)
print('False Negatives    : ', fn)
print('Balanced Err (BER) : ', ber)

# Rank the predictions by confidence
model_values = np.dot(sd, x)
sorted_indices = np.argsort(model_values)
sorted_labels = sl[sorted_indices[::-1]]

# For a fixed set of kept predictions, we consider
# "precision" and recall
number_predictions = [10, 500, 1000]

# Calculate and report these values
print('===== Precision and Recall of Confident Predictions =====')
print('| Num. Predictions | Precision |    Recall |')
for k in number_predictions:
    precision, recall = lr.precision_recall(sorted_labels, k)
    print('| {0:16d} | {1}% | {2}% |'
          ''.format(k,
                    '{0:.4g}'.format(precision * 100).rjust(8),
                    '{0:.4g}'.format(recall * 100).rjust(8)))

# Calculate precision and recall for a plot against all possible
# recovery set sizes.
precision = np.empty(Ns)
recall = np.empty(Ns)
for k in range(Ns):
    precision[k], recall[k] = lr.precision_recall(sorted_labels, k + 1)

# Plot the results -- Precision vs. recall...
plt.figure(1)
plt.plot(recall, precision)
plt.title('Precision Vs. Recall')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.axis([0.,1.,0.,1.])

# ...and Prec/Recall vs. number of predictions used.
plt.figure(2)
k = np.arange(1, Ns + 1)
lines = plt.plot(k, recall, 'b', k, precision, 'r')
plt.legend(lines, ('Recall', 'Precision'), loc=0)
plt.xlabel('k (Predictions Considered)')
plt.ylabel('Prec and Recall')
plt.axis([0,Ns + 1,0.,1.])
plt.title('Precision@k and Recall@k ')

# Display plots
plt.show()
