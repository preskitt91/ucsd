%\documentclass{assignment}
\documentclass{article}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, multicol, minted, enumitem, float}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\newtheorem{question}{Question} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\renewcommand{\c}{\ensuremath{\mathbf{c}}}
\newcommand{\F}{\ensuremath{\mathcal{F}}}
\newcommand{\mg}{martingale}
\newcommand{\E}{\ensuremath{\mathbb{E}}}
\newcommand{\rv}{random variable}


%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{CSE 258 Assignment \#2 Report}
\author{Brian Preskitt}
\date{\today}

\begin{document}
\maketitle

For the problems, results from experiments are listed with commentary (but not the code that produced them).  While some of the commentary includes explicitly described additional calculations, the code for most of the calculations is reproduced in the appendix.

\section*{Tasks (Classifier evaluation)}

The script \texttt{hw2prob1.py} (included in the appendix) runs all the experiments for this section.  The code is run in python3, and requires the white wine data to be stored in a folder called \text{data}, serialized as a dictionary object according to the \texttt{pickle} library.

\begin{enumerate}
  \item The logistic regression was run on the data, split $1/3, 1/3, 1/3$ into training, validation, and test sets.  The data was shuffled so that this process would not invalidate our results by coincidentally choosing a favorable dataset.  The performance is reported here for the values of $\lambda \in \{0, 0.01, 1.0, 100.0\}$ (where $\lambda$ is used for $L_2$ regularization only):

    \begin{minted}{python}
      >>> import hw2prob1
      ===== Logistical Regression Classifier, Lambda = 0.000 =====
      Training Classification Accuracy: 75.93%
      Valid.   Classification Accuracy: 74.16%
      Test Set Classification Accuracy: 74.39%
      Log-Likelihood at Convergence   : 807.00
      ===== Logistical Regression Classifier, Lambda = 0.010 =====
      Training Classification Accuracy: 75.44%
      Valid.   Classification Accuracy: 75.08%
      Test Set Classification Accuracy: 75.00%
      Log-Likelihood at Convergence   : 821.82
      ===== Logistical Regression Classifier, Lambda = 1.000 =====
      Training Classification Accuracy: 72.44%
      Valid.   Classification Accuracy: 72.32%
      Test Set Classification Accuracy: 70.53%
      Log-Likelihood at Convergence   : 872.34
      ===== Logistical Regression Classifier, Lambda = 100.000 =====
      Training Classification Accuracy: 67.30%
      Valid.   Classification Accuracy: 66.56%
      Test Set Classification Accuracy: 65.69%
      Log-Likelihood at Convergence   : 1023.16
    \end{minted}

    \item The counts of True and False Positives and Negatives (obtained from applying the model to the test set) are reported here, along with the Balanced Error Rate (BER).  These results come from running the same script:

      \begin{minted}{python}
        ===== Results on Test Data, Lambda = 1.00E-02 ====
        Number of points   :  1632
        True  Positives    :  954
        False Positives    :  290
        True  Negatives    :  270
        False Negatives    :  118
        Balanced Err (BER) :  0.240618336887
      \end{minted}

      \item The predictions on the test set were sorted by confidence, and we kept only the most confident predictions; the observed ``precision'' and ``recall'' are reported here:

        \begin{minted}{python}
          ===== Precision and Recall of Confident Predictions =====
          | Num. Predictions | Precision |    Recall |
          |               10 |       90% |   0.8396% |
          |              500 |     90.6% |    42.26% |
          |             1000 |     81.7% |    76.21% |
        \end{minted}

        We remark that these quantities are defined by $$(\text{precision}) = \dfrac{(\text{Number of 'Good' objects recommended})}{(\text{Number of objects recommended})} \ \text{and} \ (\text{recall}) = \dfrac{(\text{Number of 'Good' objects recommended})}{(\text{Number of 'Good' objects in dataset})}.$$

        \item In figure \ref{fig:prec_v_recall}, we show precision as a function of recall as the number of items recommended goes up.  This ``decreasing sawtooth'' is expected behavior -- the recall can only go up as we recommend more of the good objects, but every time we accidentally recommend a bad object, the precision takes a hit.  However, when we are on a string of consecutive good objects, the precision and recall increase together.

          \begin{figure}[H]
            \centering \includegraphics[width=5in]{figs/precvrecall}
            \caption{Precision vs. Recall}
            \label{fig:prec_v_recall}
          \end{figure}

          To see the overall trends of these measurements as we recommend more and more objects (which become less and less confident recommendations), we include a plot of precision and recall vs.~recommendations made in figure \ref{fig:prec_and_recall}.

          \begin{figure}[H]
            \centering \includegraphics[width=5in]{figs/prec_and_recall}
            \caption{Precision and Recall vs. Predictions Considered}
            \label{fig:prec_and_recall}
          \end{figure}
\end{enumerate}

\section*{Tasks (Dimensionality reduction)}

The experiments for this section are run in the script \texttt{hw2prob2.py}, included in the appendix.  The requirements for placement and serialization of data are identical for \texttt{hw2prob1.py}.

\begin{enumerate}[resume]
  \item We make an attempt to compress the data by replacing \emph{each data point} with the same mean vector -- in particular, we replace every data point with a vector whose entries are the average values for the 11 features over the entire dataset.  We calculate the reconstruction error from this ``compression'' technique, namely $$\sum_{i = 1}^N ||\overline{x} - x_i||_2^2,$$ where $\overline{x}$ is the mean vector and $x_1, \ldots, x_N$ are the data points.  Therefore, the reconstruction error is literally the sum of the squares of the ``zero-mean'' translation of the vector.  In other words, it's equal to the sum of the variances of each of its components \emph{times the number of data points!}  Indeed, in our experiment, the calculated ``reconstruction error'' was entirely ridiculous:

    \begin{minted}{python}
      >>> import hw2prob2 as p2
      ===== Reconstruction Error for "compression" with mean =====
      Reconstruction Error:  3452440.8587691817
    \end{minted}

    And indeed, we observed

    \begin{minted}{python}
      >>> sum(np.var(p2.td, 0)) * p2.td.shape[0]
      3452440.8587691407
    \end{minted}

    where \texttt{p2.td} is the matrix containing the training data set.  We conclude that this is not an acceptable compression mechanism.

    \item We find the PCA components using the \texttt{sklearn.decomposition.PCA} object; these are printed here ($12 \times 12$ matrix of floats):

      \begin{minted}{python}
        ===== PCA Components on Training Data =====
[[ -2.49555304e-01  -9.67010257e-01  -2.04970588e-03  -3.74649694e-04
   -4.95834300e-02  -3.35297591e-04  -2.14323419e-04  -3.66294071e-05
    1.22591943e-02   5.10546814e-06  -8.72493863e-05]
 [ -9.67501505e-01   2.50996525e-01   1.08026825e-02   4.77425220e-04
   -2.78095883e-02   2.61789564e-04   1.76045725e-03   1.45298435e-05
   -6.93498958e-03  -1.03769376e-04   9.76976640e-05]
 [ -3.88255448e-02  -4.20450679e-02   8.83530928e-03  -2.55661273e-03
    9.94986338e-01   4.04629470e-04   9.68410912e-04   4.29367298e-04
   -8.11425755e-02  -7.54786367e-03   5.81009464e-05]
 [ -7.45985773e-03   1.04657059e-02  -6.46147729e-02   1.08682839e-03
    8.18664508e-02   1.81131190e-03   1.49869525e-02  -1.15079666e-03
    9.94282474e-01   8.96086236e-03  -7.02177895e-03]
 [  9.79800003e-03  -3.61606734e-03   9.93640231e-01   1.97483818e-04
   -3.84563771e-03   4.09525628e-02  -9.41289565e-03   3.99165684e-04
    6.57871934e-02  -8.03781148e-02  -8.46556859e-04]
 [  2.74464664e-04  -6.85472710e-04   7.74792237e-02   2.78926732e-01
    7.14524090e-03  -5.13537470e-02  -1.34629907e-01   3.24938267e-03
   -2.37348958e-03   9.46072920e-01  -1.76823823e-02]
 [ -6.59688321e-04  -1.58316818e-04  -4.73905219e-02   5.11700121e-01
    6.32930888e-04   7.89330592e-01  -3.01613497e-01   4.50860918e-04
    8.74543078e-04  -1.46695090e-01   1.95540725e-02]
 [  6.56676928e-04  -4.01938029e-04   3.09992713e-03   8.12619411e-01
    1.66007528e-04  -4.80156888e-01   2.34342155e-01   2.98081119e-04
   -1.29401550e-03  -2.32668826e-01  -6.00282567e-03]
 [ -1.72397111e-03   9.25216248e-04  -6.29725624e-03  -1.57421115e-03
    1.16109751e-03  -3.75785853e-01  -9.13927448e-01  -8.12540702e-04
    1.50904658e-02  -1.49955607e-01  -2.75542604e-02]
 [  5.14338166e-06  -2.20028659e-05   2.52637992e-03  -2.37307593e-04
    6.58111413e-04  -2.95562383e-02  -2.01779602e-02   4.30345972e-03
    7.40341232e-03   1.40618494e-02   9.99220366e-01]
 [  6.87821910e-06  -4.26291715e-06  -7.22328723e-04  -1.37730252e-03
   -3.58326920e-04  -2.38487863e-04  -1.31618438e-04   9.99984150e-01
    1.14146996e-03  -3.07542925e-03  -4.27990369e-03]]
      \end{minted}

      For what it's worth, these results were obtained from the lines

      \begin{minted}{python}
        # Create and fit a PCA model, report the components
        pca = PCA()
        model = pca.fit(td)
        print('===== PCA Components on Training Data =====')
        print(model.components_)        
      \end{minted}

      \item If we compress the data using only four PCA dimensions, the reconstruction error will be equal to the sum of the squares of the remaining singular values.  If we let $X \in \R^{N \times d}$ be a matrix (with zero mean on each column -- $N$ data points of $d$ features), and if $X_k$ is the copy of $X$ developed from using $k$ dimensions in PCA compression, then the reconstruction error is $$||X - X_kV_k^T||_F^2 = \sum_{i = k + 1}^d \sigma_i^2,$$ where $\sigma_1 \ge \sigma_2 \ge \cdots \ge \sigma_d$ are the singular values of $X$ and $V_k\in\R^{d \times k}$ is the reconstruction matrix (i.e., its columns are the first $k$ right-singular vectors of $X = U \Sigma V^T$).  Using this formulation, we calculate that the reconstruction error should be

        \begin{minted}{python}
          ===== Reconstruction Error under 4 Dimensions =====
          Reconst. Error    : 1282.916240597161
        \end{minted}

        We verify this calculation by looking at the leftovers if we throw away all but the first 4 columns of the PCA-transformed image of $X$:

        \begin{minted}{python}
          >>> T1 = p2.model.transform(p2.td)
          >>> sum(x**2 for x in T1[:,4:].flatten())
          1282.9162405971617
        \end{minted}

        \item  We train a linear regressor on the data using a training dataset that is compressed by increasingly many PCA dimensions; the resulting MSE on the training and test data sets from these models is printed below and reproduced in figure \ref{fig:MSE_v_dims}.

          \begin{minted}{python}
            ==============================================
            ====== PCA Regression, Num. Dimensions =======
            ==============================================
            | Num. Dims | Train MSE | Test MSE |
            |         0 |    0.7468 |   0.7882 |
            |         1 |    0.7305 |   0.7666 |
            |         2 |    0.7005 |   0.7622 |
            |         3 |    0.6937 |   0.7646 |
            |         4 |    0.5751 |   0.6201 |
            |         5 |    0.5749 |   0.6195 |
            |         6 |    0.5703 |   0.6166 |
            |         7 |    0.5667 |   0.6088 |
            |         8 |    0.5658 |   0.6084 |
            |         9 |    0.5426 |   0.5842 |
            |        10 |    0.5426 |   0.5839 |
            |        11 |    0.5335 |   0.5740 |
          \end{minted}

          \begin{figure}[H]
            \centering \includegraphics[width=5in]{figs/mse_v_pca_dim}
            \caption{MSE vs PCA Dimensions}
            \label{fig:MSE_v_dims}
          \end{figure}

          This result is somewhat surprising!  You might expect that the first few dimensions knock out most of the prediction error and that the remaining dimensions are relatively irrelevant, but this is not seen at all!  For some reason, the 4th of the PCA dimensions appears to be the most significant in predicting the quality of the wines.

          While surprising, this result may be accounted for by considering that the ordering of the PCA components has to do with the variance of the input data along different dimensions, while the MSE has to do with how strongly the features in the data are correlated to the output.  For example, if I added a random feature to my data distributed according to $\mathcal{N}(0, 10^6)$, it would easily become the top singular vector (and the ``strongest'' PCA component) because of its high variance, but it's clearly useless in predicting the quality of each wine.  Moreover, it's quite likely that some feature that was not measured by the scientists who compiled this dataset (say, square of the age in millenia of whoever bottled the sample) may have small variance, but could be highly correlated to the output variable (unlikely in this example!).

          Therefore, we conclude that PCA is helpful for compressing data on its own, but is not necessarily helpful in predicting a function of that data, as the relative variance of the features (or their linear combinations) has nothing to do with how some output quantity depends on the features.  Indeed, when we perform least squares, precisely the problem we are solving is ``which linear combination of the features best predicts the output,'' and the PCA analysis is (in a certain sense) independent of this result.

\end{enumerate}

\pagebreak

\section*{Appendix -- Code}
\texttt{hw2prob1.py}
\inputminted[linenos]{python}{../code/hw2prob1.py}
\pagebreak
\texttt{hw2prob2.py}
\inputminted[linenos]{python}{../code/hw2prob2.py}
\pagebreak
\texttt{logregimports.py}
\inputminted[linenos]{python}{../code/logregimports.py}

\end{document}
