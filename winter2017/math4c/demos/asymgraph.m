function rc = asymgraph(x, y, z)

  graphics_toolkit('gnuplot');
  setenv('GNUTERM', 'qt');

  hold off;
  close;
  plot(x, y, 'b', 'linewidth', 3);
  hold on;
  plot(x, z, 'r', 'linestyle', '--', 'linewidth', 2);
  rc = 1;
