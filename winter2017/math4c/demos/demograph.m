function demograph(x, y)

  graphics_toolkit('gnuplot');
  setenv('GNUTERM', 'qt');

  hold off;
  figure();
  plot(x, y, 'b', 'linewidth', 3);
