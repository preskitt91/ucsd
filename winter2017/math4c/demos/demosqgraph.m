function demosqgraph(x, y)

  graphics_toolkit('gnuplot');
  setenv('GNUTERM', 'qt');

  hold off;
  figure();
  plot(x, y, 'b', 'linewidth', 3);
  axis([-3,3,-10,10]);
