%\documentclass{assignment}
\documentclass{exam}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, enumitem}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\newcommand{\dom}{\ensuremath{\mathrm{dom}}}
\newcommand{\ran}{\ensuremath{\mathrm{range}}}

\pagestyle{headandfoot}
\firstpageheadrule
\firstpageheader{Math 4C}{Practice Midterm 1}{Winter 2017}
\runningheadrule
\runningheader{Math 4C}{Practice Midterm 1}{Winter 2017}
%% \coursetitle{}
%% \courselabel{}
%% \exercisesheet{}{}

%% \university{}
%% \semester{}
%% \date{}


\title{}
\author{}
\date{}

\begin{document}
\newlength{\probskip}
\setlength{\probskip}{0.6cm}
\addtolength{\baselineskip}{3pt}
% \maketitle

%% \begin{abstract}
%% \end{abstract}

%% \begin{problemlist}
%% \end{problemlist}

%\textbf{\large University of California, San Diego \\ Math 4C, Winter 2017 \\ Instructor:  Brian Preskitt \\ Practice Midterm 1 }
%\vspace{1em}

\begin{questions}
  \question
  Find all $x$ satisfying the equation $|x - 1| + |x + 1| = 4$.

  \textbf{Solution} By looking at where the two absolute value expressions ``flip'' (namely at $1$ and $-1$), we consider three cases:
  \begin{itemize}
  \item $x \in (-\infty, -1]$.  In this case, the equation becomes $-(x - 1) - (x + 1) = 4$, or $-2x = 4$, yielding $x = -2$.
  \item $x \in [-1, 1]$.  In this case, the equation becomes $-(x - 1) + (x + 1) = 4$, or $2 = 4$, which has \emph{no solutions}.
    \item $x \in [1, \infty)$.  In this case, the equation becomes $(x - 1) + (x + 1) = 4$, or $2x = 4$, yielding $x = 2$.
  \end{itemize}
  In total, our solutions are $x = -2, 2$.

  \question
  Find the equation of the line perpendicular to the line $y = 5 - 4x$ and containing the point $(5, 5)$.

  \textbf{Solution}  Any line perpendicular to $y = 5 - 4x$ has a slope of $m = -(1 / (-4)) = 1/4$; since we now have the slope of, and a point on, the line we're looking for, we may write it down as 
  \begin{align*}
    (y - 5) &= (1/4)(x - 5) \\
    y &= \frac{1}{4}x - \frac{5}{4} + 5 \\
    y &= \frac{1}{4} x + \frac{15}{4}.
  \end{align*}

  \question Let $f(x) = \dfrac{8x - 2}{x - 4}$.
  \begin{enumerate}[label=(\alph*)]
  \item Find the domain of $f$.
  \item Find the range of $f$. (Hint: Try part \ref{part} first!)
  \item Find a formula for $f^{-1}$. \label{part}
  \item Find the domain and range of $f^{-1}$.
  \end{enumerate}

  \textbf{Solution}
  \begin{enumerate}[label=(\alph*)]
  \item The domain of $f$ is wherever the denominator is non-zero; naturally, $x - 4 = 0 \iff x = 4$, so the domain is $\R \setminus \{4\}$ or $(-\infty, 4) \cup (4, \infty)$.
  \item By consulting our inverse function, we see that the range of $f$ is $\R \setminus \{8\} = (-\infty, 8) \cup (8, \infty)$.
  \item To get $f^{-1}$, we solve for $x$ in terms of $y$:
    \begin{align*}
      \dfrac{8x - 2}{x - 4} &= y \\
      8x - 2 &= y(x - 4) \\
      x(8 - y) &= 2 - 4y \\
      x &= \dfrac{2 - 4y}{8 - y} = f^{-1}(y)
    \end{align*}
  \end{enumerate}
  \item The domain of $f^{-1}$ is $\dom(f^{-1}) = \ran(f) = \R \setminus \{8\}$ and its range is $\ran(f^{-1}) = \dom(f) = \R \setminus \{4\}$.

  \question \label{line}
  For questions \ref{line} through \ref{linelast}, let $L$ be the line containing the point $(3, 14)$ and having slope $-2$.
  Find an equation for $L$ (and simplify to the form $y = mx + b$)

  \textbf{Solution}
  We may immediately solve for the line with
  \begin{align*}
    (y - 14) &= (-2)(x - 3) \\
    y &= -2x + 20
  \end{align*}

  \question
  Show that the point $(7 + t, 6 - 2t)$ is on $L$ for any real number $t$.

  \textbf{Solution}  We need only plug in this point to the equation for $L$ and see if it's true!  Indeed: $$-2(7 + t) + 20 = -14 - 2t + 20 = 6 - 2t,$$ so $(7 + t, 6 - 2t)$ is on the line.

  \question \label{linelast}
  Find a number $c$ such that the point $(-1, c)$ is on the line perpendicular to $L$ and intersecting $L$ at $(3, 14)$.

  \textbf{Solution}
  First, let's find an equation for this perpendicular line.  It's perpendicular to $L$, so its slope is $1/2$.  Second, it has the point $(3, 14)$, so the line has the equation 
  \begin{align*}
    (y - 14) &= 1/2(x - 3) \\
    y - 14 &= 1/2 x - 3/2 \\
    y &= \frac{x + 25}{2}
  \end{align*}
  To find $c$, we need only plug $-1$ in for $x$: $$\frac{-1 + 25}{2} = 12,$$ so $(-1, 12)$ is on this line and $c = 12$.

  \question
  Find two real numbers whose sum is $16$ and whose product is $36$.

  \textbf{Solution}
  We are looking for numbers $a$ and $b$ such that $$a + b = 16, \ \text{and} \ ab = 36.$$  The first equation gives $b = (16 - a)$, and substituting into the second gives us 
  \begin{align*}
    a(16 - a) &= 36 \\
    16a - a^2 &= 36 \\
    a^2 - 16a + 36 &= 0 \\
    a &= \dfrac{16 \pm \sqrt{256 - 144}}{2} \\
    a &= 8 \pm \sqrt{28} = 8 \pm 2\sqrt{7}.
  \end{align*}
  We begin by trying $a = 8 - 2 \sqrt{7}$ (though $8 + 2\sqrt{7}$ would be an equally good choice), which gives $b = 16 - a = 8 + 2\sqrt{7}$.  We then check $$ab = (8 - 2\sqrt{7})(8 + 2 \sqrt{7}) = 64 - 4 \cdot 7 = 36,$$ so this completes the problem!

  \question
  Let $f(x) = -2x^2 - 12x + 12$.
  \begin{enumerate}[label=(\alph*)]
  \item What is the maximum value of $f$?
  \item What is the vertex of $f$?
  \item Find all solutions to $f(x) = -2$.
  \end{enumerate}

  \textbf{Solution}
  \begin{enumerate}[label=(\alph*)]
  \item We find the maximum by completing the square:
    \begin{align*}
      f(x) &= -2(x^2 + 6x - 6) \\
      &= -2((x + 3)^2 - 9 - 6) \\
      &= -2(x + 3)^2 + 30,
    \end{align*}
    so that the maximum value of $f$ must be $30$.
  \item We use the last result from the previous calculation to say that the vertex is at the point $(-3, 30)$ (and we verify that $f(-3) = -2 \cdot 9 - 12 \cdot (-3) + 12 = 30$).
  \item To solve $f(x) = -2$, we look at 
    \begin{align*}
      -2(x + 3)^2 + 30 &= -2 \\
      -2(x + 3)^2 &= -32 \\
      (x + 3)^2 &= 16 \\
      x + 3 &= \pm 4 \\
      x &= -7, 1.
    \end{align*}
  \end{enumerate}

  \question
  Find \emph{all} solutions to the equation $$\dfrac{x^2}{3} - 2x + 2y^2 + 24y = -75.$$

  \textbf{Solution}
  We complete the square to reduce the complexity of the equation:
  \begin{align*}
    1/3(x^2 - 6x) + 2(y^2 + 12y) &= -75 \\
    1/3((x - 3)^2 - 9) + 2((y + 6)^2 - 36) &= -75 \\
    1/3(x - 3)^2 - 3 + 2(y + 6)^2 - 72 &= -75 \\
    1/3(x - 3)^2 + 2(y + 6)^2 &= 0.
  \end{align*}
  This last equation admits only one solution -- $(x, y) = (3, -6)$.  (You might think of this as a circle of ``radius zero'').

  \question
  Let $f(x) = \sqrt{|x + 5| - 1}$.
  \begin{enumerate}[label=(\alph*)]
  \item What is the domain of $f$?
  \item Let $g$ be $f$ with its domain restricted to be $(-127, -10]$.  Find a formula for $g^{-1}$.
  \item What is the domain of $g^{-1}$?
  \end{enumerate}

  \textbf{Solution}
  \begin{enumerate}[label=(\alph*)]
  \item The domain of $f$ is wherever $|x + 5| - 1 \ge 0$.  We solve this by rearranging to $$|x + 5| \ge 1 \iff x + 5 \ge 1 \ \text{or} \ x + 5 \le -1,$$ which yields $x \ge -4$ or $x \le -6$.  Putting these together, we see the domain is $(-\infty, -6] \cup [-4, \infty)$.
    \item Since $x \le -10$ on the domain of $g$, we have that $$g(x) = \sqrt{|x + 5| - 1} = \sqrt{-x - 5 - 1} = \sqrt{-x - 6}.$$  Therefore, solving for $x$ in terms of $y$ gives 
      \begin{align*}
        y &= \sqrt{-x - 6} \\
        y^2 &= -x - 6 \\
        x &= -6 - y^2 = g^{-1}(y).
      \end{align*}
    \item The domain of $g^{-1}$ is the range of $g$.  $g$ is decreasing on its domain, and since we have that $g(-127) = 11$ and $g(-10) = 2$, we can conclude that $\ran(g) = [2, 11)$.
  \end{enumerate}

    \question
    The circle $(x - 1)^2 + (y - 1)^2 = 25$ and the line $y = 1/2 x + 3$ intersect at two points.  What are they?

    \textbf{Solution}
    We can substitute the equation for the line into the equation for the circle.  Namely, we rearrange to get $x = 2y - 6$, yielding
    \begin{align*}
      ((2y - 6) - 1)^2 + (y - 1)^2 &= 25 \\
      (2y - 7)^2 + (y - 1)^2 &= 25 \\
      4y^2 - 28y + 49 + y^2 - 2y + 1 &= 25 \\
      5y^2 - 30y + 25 &= 0 \\
      y &= \dfrac{30 \pm \sqrt{900 - 500}}{10} \\
      y &= 3 \pm 2 = 1, 5;
    \end{align*}
    these results give us $x = 2(1) - 6 = -4$ and $x = 2(5) - 6 = 4$.  Therefore, the two points of intersection are $(-4, 1)$ and $(4, 5)$.

    \question
    Let $f(x) = 3x^2 - 6x - 9$.
    \begin{enumerate}[label=(\alph*)]
    \item By completing the square, rewrite $f(x)$ in the form $f(x) = a(x - h)^2 + k$, where $a, h, k \in \R$.
    \item Find all solutions to the equation $f(x) = 0$.
    \item Using these results, draw a sketch of the parabola on the domain $[-3, 5]$.
    \end{enumerate}

    \textbf{Solution}
    \begin{enumerate}[label=(\alph*)]
    \item We observe $$f(x) = 3x^2 - 6x - 9 = 3(x^2 - 2x - 3) = 3((x - 1)^2 - 1 - 3) = 3(x - 1)^2 - 12.$$
    \item Using this result, we see $$f(x) = 0 \iff 3(x - 1)^2 = 12 \iff x - 1 = \pm 2 \iff x = -1, 3.$$
      \item (Sketch a parabola with its vertex -- bottom -- at $(1, -12)$ and intersecting the $x$-axis at $(-1, 0)$ and $(3, 0)$.)
    \end{enumerate}

    \question
    Suppose $f$ is a function with domain $(-1, 6]$ and range $[3, 4)$.  Define $g$ and $h$ by $$g(x) = f(2x - 2) \ \text{and} \ h(x) = -2f(x) + 2.$$
        \begin{enumerate}[label=(\alph*)]
        \item What is the domain of $g$?
        \item What is the range of $g$?
        \item What is the domain of $h$?
        \item What is the range of $h$?
        \item Devise a function $l$ such that the domain of $l$ is equal to the domain of $g$ and the range of $l$ is equal to the range of $h$?
        \end{enumerate}

        \textbf{Solution}
        \begin{enumerate}[label=(\alph*)]
        \item The domain of $g$ is where $2x - 2 \in \dom(f)$, namely $$-1 < 2x - 2 \le 6 \iff 1 < 2x \le 8 \iff 1/2 < x \le 4,$$ so $\dom(g) = (1/2, 4]$.
    \item Since $g$ is not transformed vertically, $\ran(g) = \ran(f) = [3, 4)$.
    \item Since $h$ is not transformed horizontally, $\dom(h) = \dom(f) = (-1, 6]$.
    \item By looking at the range of $f$, we have, for any $x \in \dom(f)$, $$3 \le f(x) < 4 \implies -6 \ge -2 f(x) > -8 \implies -4 \ge -2 f(x) + 2 > -6,$$ giving that $\ran(h) = (-6, -4]$.
    \item To come up with a function $l$ with $\dom(l) = \dom(g)$ and $\ran(l) = \ran(h)$, we might apply both the horizontal and vertical transformations to $f$ simultaneously.  Namely, we can take $$l(x) = -2 f(2x - 2) + 2.$$
        \end{enumerate}

        \question
        Answer each question.  No justification required.
        \begin{enumerate}[label=(\alph*)]
        \item Let $f$ be a one-to-one function with $10$ is in the domain of $f$.  What is $f^{-1}(f(10))$?
        \item Give an example of a parabola with its vertex at $(-4, 7)$.
        \item Give an example of a quadratic equation with no solutions.
        \item Give an example of a function that is one-to-one.
        \item Give an example of a function that is not one-to-one.
        \end{enumerate}

        \textbf{Solution}
        \begin{enumerate}[label=(\alph*)]
        \item $f^{-1}(f(10))$ exists because $10 \in \dom(f)$, so $f^{-1}(f(10)) = 10$.
        \item We can just go backwards with our ``vertex formula'' and set $$g(x) = 10(x + 4)^2 + 7.$$ (note that any number -- except $0$ -- could be used instead of the $10$).
        \item To have no solutions, we need to have $b^2 - 4ac < 0$; for example, we might take $b = 1, a = 1, c = 1$, so that $b^2 - 4ac = -3$.  This shows that $x^2 + x + 1 = 0$ is a quadratic equation with no solutions.
        \item An example of a one-to-one function is $f(x) = 3x - 2$ -- any non-constant linear function is one-to-one.
          \item A function that is not one-to-one is $f(x) = |x| - 1$, because $f(-5) = f(5) = 4$.
        \end{enumerate}

        \question
        Let $L$ be the line containing the points $(-3, 2)$ and $(2, 17)$.  Find an equation for the line parallel to $L$ and containing the point $(-2, 4)$.

        \textbf{Solution}
        The slope of $L$ is $m = \frac{17 - 2}{2 - (-3)} = \frac{15}{5} = 3.$  The parallel line will be given by 
        \begin{align*}
          (y - 4) &= 3(x - (-2)) \\
          y - 4 &= 3x + 6 \\
          y &= 3x + 10
        \end{align*}
        
        \question
        The equation $4x^2 + 16x + 56 = 24y - 4y^2$ defines a circle.  Find its center and radius.  (***MISTAKE*** Replace ``56'' with ``48'' in the above equation)

        \textbf{Solution}
        To do this, we rearrange and complete the square:
        \begin{align*}
          4(x^2 + 4x) + 4(y^2 - 6y) &= -48 \\
          4((x + 2)^2 - 4) + 4((y - 3)^2 - 9) &= -48 \\
          4(x + 2)^2 - 16 + 4(y - 3)^2 - 36 &= -48 \\
          4(x + 2)^2 + 4(y - 3)^2 &= 4 \\
          (x + 2)^2 + (y - 3)^2 &= 1, \\
        \end{align*}
        so this is a circle with center $(-2, 3)$ and radius $1$.

        \question
        Describe the solutions to $$\left|\dfrac{x + 7}{2x - 2}\right| \le 3$$ as a union of one or more intervals.

        \textbf{Solution}
        Let's solve the equation $\left|\dfrac{x + 7}{2x - 2}\right| = 3$ first to ``divide'' the real line into two or more intervals where the inequality ``switches.''  Then we'll test these intervals to see which ones satisfy the inequality.  To solve, we have the positive and negative cases.  First, the positive case:
        \begin{align*}
          \dfrac{x + 7}{2x - 2} &= 3 \\
          x + 7 &= 6x - 6 \\
          x &= 13 / 5
        \end{align*}
        Then the negative case:
        \begin{align*}
          \frac{x + 7}{2x - 2} &= -3 \\
          x + 7 &= 6 - 6x \\
          7x &= -1 \\
          x &= -1/7
        \end{align*}
        Considering also that $x = 1$ is where the inequality is undefined, we therefore have the following intervals as candidates: $(-\infty, -1/7], [-1/7, 1), (1, 13/5], [13/5, \infty)$.  We test these intervals with the points $-1, 0, 2,$ and $3$, which give $$\left|\dfrac{-1 + 7}{2(-1) - 2}\right| = 3/2 \le 3, \left|\dfrac{0 + 7}{0 - 2}\right| = 7/2 > 3, \left|\dfrac{2 + 7}{2\cdot2 - 2}\right| = 9/2 > 3, \ \text{and} \ \left|\dfrac{3 + 7}{2 \cdot 3 - 2}\right| = 5/2 \le 3,$$ meaning that the inequality is satisfied on $(-\infty, -1/7] \cup [13/5, \infty)$.
    
\end{questions}


\end{document}

