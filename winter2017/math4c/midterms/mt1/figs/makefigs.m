a = 2;
b = 6;
h = -2;
o = 3;

font = 26;

f = @(x) cos((x - a) / (b - a) * 2 * pi) * h + o;
t = linspace(a, b);
x = t / 2;
y = f(2 * x) - 4;

graphics_toolkit('gnuplot');
setenv('GNUTERM', 'qt');

plot(x, y, 'linewidth', 3);
grid on
set(gca, 'fontsize', font, 'fontweight', 'bold');
axis([min(0, min(x) - 1), max(x) + 1, min(0, min(y) - 1), max(y) + 1]);
