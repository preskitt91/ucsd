%\documentclass{assignment}
\documentclass[addpoints]{exam}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, enumitem, float, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\newcommand{\dom}{\ensuremath{\mathrm{dom}}}

\pagestyle{headandfoot}
\firstpageheadrule
\firstpageheader{Math 4C}{Midterm 1, Version A}{Winter 2017}
\runningheadrule
\runningheader{Math 4C}{Midterm 1, Version A}{Winter 2017}
\renewcommand{\questionshook}{\setlength{\itemsep}{0.5cm}}
\bracketedpoints
\printanswers
%\pointsinmargin

\title{}
\author{}
\date{}

\begin{document}
\newlength{\probskip}
\setlength{\probskip}{0.6cm}
\addtolength{\baselineskip}{3pt}

\begin{questions}

  \question[12]
  Express the solutions to the inequality $|6x - 8| < 3$ as an interval or union of intervals.

  \begin{solution}
    From the definition of absolute value, we have that $$|6x - 8| < 3 \iff -3 < 6x - 8 < 3,$$ which becomes $$5 < 6x < 11 \iff 5/6 < x < 11/6.$$  Expressing this in interval notation, we have $$|6x - 8| < 3 \iff x \in (5/6, 11/6).$$
  \end{solution}
  
  \question[12]
  Let $f(x) = 3x^2 + 12x + 5$ and $g(x) = 2x - 2$.  Find a formula for $f \circ g(x) = f(g(x))$.

  \begin{solution}
    To find $f \circ g$, we plug $g$ into $f$!  In other words, 
    \begin{align*}
      f \circ g (x) &= f(g(x)) \\
      &= f(2x - 2) \\
      &= 3(2x - 2)^2 + 12(2x - 2) + 5 \\
      &= 3(4x^2 - 8x + 4) + 24x - 24 + 5 \\
      &= 12x^2 - 7,
    \end{align*}
    so $f \circ g(x) =  12x^2 - 7.$
  \end{solution}

  \question[12]
  Find the vertex of the parabola given by $f(x) = -x^2 + 2x + 8$ and state whether the vertex is a maximum or a minimum point.

  \begin{solution}
    To find the vertex, we begin by completing the square: $$-x^2 + 2x + 8 = -(x^2 - 2x - 8) = -((x - 1)^2 - 1 - 8) = -(x - 1)^2 + 9.$$  The vertex is always where the ``squared term'' is equal to zero, so we conclude that the vertex appears at $(1, 9)$.  Because the coefficient of $x^2$ is negative, the parabola opens downward, so the vertex represents a \textbf{maximum point}.
  \end{solution}
  
  \question[12]
  The equation $6y - 10x = 47 - x^2 - y^2$ defines a circle.  Find its center and radius.

  \begin{solution}
    We will rearrange the equation by completing the square in order to find the center and radius:
    \begin{align*}
      6y - 10x &= 47 - x^2 - y^2 \\
      x^2 - 10x  + y^2 + 6y &= 47 \\
      (x - 5)^2 - 25 + (y + 3)^2 - 9 &= 47 \\
      (x - 5)^2 + (y + 3)^2 &= 81.
    \end{align*}
    By recalling the pattern of $(x - x_0)^2 + (y - y_0)^2 = r^2$ for a circle of radius $r$ and center $(x_0, y_0)$, we conclude that this equation defines a circle of radius $9$ centered at the point $(5, -3)$.
  \end{solution}

  \question[12]
  Find two real numbers whose difference is $1$ and whose product is $4$.

  \begin{solution}
    We restate the problem mathematically -- namely, we are looking for two numbers $a, b \in \R$ such that $a - b = 1$ and $ab = 4$.  The first equation gives us that $a = (b + 1)$; by substituting this into the second equation, we have $$(b + 1)b = 4 \iff b^2 + b = 4 \iff b^2 + b - 4 = 0,$$ which gives us $b = -\frac{1}{2} \pm \frac{\sqrt{17}}{2}$ (say, by the quadratic formula).  We choose to try $-1/2 + \frac{\sqrt{17}}{2}$ (although either would be an equally valid choice) and conclude that $a = b + 1 = 1/2 + \sqrt{17}/2$.

    We check that $a - b = 1$ and (by expanding the product or by using the ``difference of squares'' identity) $$ab = (1/2 + \sqrt{17}/2) \cdot (-1/2 + \sqrt{17}/2) = 17/4 - 1/4 = 16/4 = 4,$$ so we're done, with $(a, b) = (1/2 + \sqrt{17}/2, -1/2 + \sqrt{17}/2)$ (the other solution would be $(a, b) = (1/2 - \sqrt{17}/2, -1/2 - \sqrt{17}/2)$).
  \end{solution}

  \question[12]
  Find \emph{all} solutions to the equation $4x^2 + 16x + y^2 - 14y = -65.$

  \begin{solution}
    Because this equation has quadratic terms, we begin by completing the square:
    \begin{align*}
      4x^2 + 16x + y^2 - 14y &= -65 \\
      4(x^2 + 4x) + y^2 - 14y &= -65 \\
      4((x + 2)^2 - 4) + (y - 7)^2 - 49 &= -65 \\
      4(x + 2)^2 - 16 + (y - 7)^2 - 49 &= -65 \\
      4(x + 2)^2 + (y - 7)^2 &= 0,
    \end{align*}
    which can only be true if both squared terms are $0$, in particular if $(x, y) = (-2, 7)$.
  \end{solution}

  \question
  Let $f(x) = \dfrac{-3x + 3}{2x + 7}$.
  \begin{parts}
  \part[3] Find the domain of $f$.
  \part[3] Find the range of $f$ (Hint: Try part (\ref{en:inverse}) first!)
  \part[6] \label{en:inverse} Find a formula for $f^{-1}$.
  \end{parts}

  \begin{solution}
    \begin{parts}
      \part The only problem here is potentially dividing by zero, so we have that $\dom(f) = \R \setminus \{-7/2\}$.
      \part By looking at our formula from part (\ref{en:inverse}), we see that the range is $\R \setminus \{-3 / 2\}$.
      \part We solve the equation $y = f(x)$ for $x$:
      \begin{align*}
        y &= \dfrac{-3x + 3}{2x + 7} \\
        y(2x + 7) &= -3x + 3 \\
        x(3 + 2y) &= 3 - 7y \\
        x &= \dfrac{3 - 7y}{3 + 2y},
      \end{align*}
      so $f^{-1}(y) = \dfrac{3 - 7y}{3 + 2y}$.
    \end{parts}
  \end{solution}

  \question[12]
  \begin{multicols}{2}
    Let $f$ be the function whose graph is depicted here, and let $g(x) = 2 f(x + 3)$.  Sketch the graph of $g$ over its entire domain.  Please label and scale the axes, and label the $(x, y)$ coordinates of three points on the curve.

    \begin{figure}[H] \includegraphics[width=6cm]{figs/graph1.png} \end{figure}
  \end{multicols}

  \begin{solution}
  The transformation given will translate the graph to the left by 3 units and stretch it vertically by a factor of 2.  The graph of the result is shown here:
  \begin{figure}[H] \centering \includegraphics[width=6cm]{figs/graph1soln.png} \end{figure}
  \end{solution}

  %% \question
  %% The circle $(x + 5)^2 + (y - 2)^2 = 169$ and the line $y = -\frac{1}{5}x + 14$ intersect at two points.  What are they?

  %% \question
  %% Describe the solutions to $$\left|\dfrac{3x + 1}{3x - 1}\right| \ge 4$$ as an interval or a  union of intervals.
    
    \question[12]
    Let $L$ be the line containing the points $(-1, 1)$ and $(1, 11)$.  Find the equation of a line parallel to $L$ having a $y$ intercept of $-4$.

    \begin{solution}
      The slope of $L$ is $\frac{11 - 1}{1 - (-1)} = \frac{10}{2} = 5$, so the slope of a parallel line is also $5$.  With a $y$-intercept of $-4$, this gives us $$y = 5x - 4.$$
    \end{solution}

  \question
  Let $L$ be the line containing the point $(2, -8)$ and having slope $-3$.
  \begin{parts}
  \part[9] Find an equation for $L$ (and simplify to the form $y = mx + b$)
  \part[9] Find an equation for a line perpendicular to $L$ which has a $y$-intercept of 7.
  \end{parts}

  \begin{solution}
    \begin{parts}
      \part Using the point and the slope given, we find 
      \begin{align*}
        (y - (-8)) &= -3(x - 2) \\
        y + 8 &= -3x + 6 \\
        y &= -3x - 2,
      \end{align*}
      so the equation is $y = -3x - 2$.
      \part A perpendicular line will have a slope of $-\frac{1}{-3} = 1/3$; using the $y$-intercept given, the equation becomes $y = \frac{1}{3} x + 7$.
    \end{parts}
  \end{solution}

\end{questions}
\vspace{\fill}
\emph{\large This exam has \numquestions\ questions and \numpoints\ points.}


\end{document}

