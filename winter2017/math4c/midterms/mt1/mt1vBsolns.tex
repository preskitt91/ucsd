%\documentclass{assignment}
\documentclass[addpoints]{exam}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{bbm, enumerate, enumitem, float, multicol}
\usepackage{graphics, parskip}
\usepackage{listings}
\usepackage{appendix}
\usepackage{afterpage}
\usepackage[margin=1in]{geometry}
\lstset{numbers=left}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{exercise}{Exercise}
\newtheorem*{surfacecor}{Corollary 1}
\newtheorem{conjecture}{Conjecture} 
\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\R}{\ensuremath{\mathbb{R}}}  % The real numbers.
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Rn}{\ensuremath{\R^n}}
\newcommand{\Cn}{\ensuremath{\C^n}}
\newcommand{\Rmxn}{\ensuremath{\R^{m \times n}}}
\newcommand{\Cmxn}{\ensuremath{\C^{m \times n}}}
\newcommand{\Rnxn}{\ensuremath{\R^{n \times n}}}
\newcommand{\Cnxn}{\ensuremath{\C^{n \times n}}}
\newcommand{\bigfrac}[2]{\ensuremath{\frac{\displaystyle #1}{\displaystyle #2}}}
\newcommand{\ee}{\mathrm{e}}
\newcommand{\ii}{\mathrm{i}}
\newcommand{\diag}{\ensuremath{\mathrm{diag}}}
\newcommand{\Col}{\ensuremath{\mathrm{Col}}}
\newcommand{\Span}{\ensuremath{\mathrm{span}}}
\newcommand{\Nul}{\ensuremath{\mathrm{Nul}}}
\newcommand{\rank}{\ensuremath{\mathrm{rank}}}
\newcommand{\dom}{\ensuremath{\mathrm{dom}}}

\pagestyle{headandfoot}
\firstpageheadrule
\firstpageheader{Math 4C}{Midterm 1, Version B}{Winter 2017}
\runningheadrule
\runningheader{Math 4C}{Midterm 1, Version B}{Winter 2017}
\renewcommand{\questionshook}{\setlength{\itemsep}{0.5cm}}
\bracketedpoints
\printanswers
%\pointsinmargin

\title{}
\author{}
\date{}

\begin{document}
\newlength{\probskip}
\setlength{\probskip}{0.6cm}
\addtolength{\baselineskip}{3pt}

\begin{questions}
  \question[12]%5
  Let $L$ be the line containing the points $(-2, 0)$ and $(0, -8)$.  Find the equation of a line parallel to $L$ having a $y$ intercept of $6$.

  \begin{solution}
      The slope of $L$ is $\frac{(-8) - 0}{0 - (-2)} = \frac{-8}{2} = -4$, so the slope of a parallel line is also $-4$.  With a $y$-intercept of $6$, this gives us $$y = -4x + 6.$$
    \end{solution}

  \question[12]%8
  \begin{multicols}{2}
    Let $f$ be the function whose graph is depicted here, and let $g(x) = f(2x) - 4$.  Sketch the graph of $g$ over its entire domain.  Please label and scale the axes, and label the $(x, y)$ coordinates of three points on the curve.

    \begin{figure}[H] \includegraphics[width=6cm]{figs/graph2.png} \end{figure}
  \end{multicols}

  \begin{solution}
    The transformation given will translate the graph down by $4$ units and shrink it horizontally by a factor of $1/2$.  The graph of the result is shown here:
    \begin{figure}[H] \centering \includegraphics[width=6cm]{figs/graph2soln.png} \end{figure}
  \end{solution}

  \question[12]%6
  Find \emph{all} solutions to the equation $x^2 + 4x + 3y^2 - 12y = -16.$

  \begin{solution}
    Because this equation has quadratic terms, we begin by completing the square:
    \begin{align*}
      x^2 + 4x + 3y^2 - 12y &= -16 \\
      x^2 + 4x + 3(y^2 - 4y) &= -16 \\
      (x + 2)^2 - 4 + 3((y - 2)^2 - 4) &= -16 \\
      (x + 2)^2 - 4 + 3(y - 2)^2 - 12 &= -16 \\
      (x + 2)^2 + 3(y - 2)^2 &= 0,
    \end{align*}
    which can only be true if both squared terms are $0$, in particular if $(x, y) = (-2, 2)$.
  \end{solution}
  
  \question[12]%3
  The equation $x^2 + 8x = 48 + 12y - y^2$ defines a circle.  Find its center and radius.

  \begin{solution}
    We will rearrange the equation by completing the square in order to find the center and radius:
    \begin{align*}
      x^2 + 8x &= 48 + 12y - y^2 \\
      x^2 + 8x + y^2 - 12y &= 48 \\
      (x + 4)^2 - 16 + (y - 6)^2 - 36 &= 48 \\
      (x + 4)^2 + (y - 6)^2 &= 100.
    \end{align*}
    By recalling the pattern of $(x - x_0)^2 + (y - y_0)^2 = r^2$ for a circle of radius $r$ and center $(x_0, y_0)$, we conclude that this equation defines a circle of radius $10$ centered at the point $(-4, 6)$.
  \end{solution}  

  \question[12]%4
  Find two real numbers whose product is $9$ and whose sum is $8$.

  \begin{solution}
    We restate the problem mathematically -- namely, we are looking for two numbers $a, b \in \R$ such that $a + b = 8$ and $ab = 9$.  The first equation gives us that $a = (8 - b)$; by substituting this into the second equation, we have $$(8 - b)b = 9 \iff 8b - b^2 = 9 \iff b^2 - 8b + 9 = 0,$$ which gives us $b = 4 \pm \sqrt{7}$ (say, by the quadratic formula).  We choose to try $4 + \sqrt{7}$ (although either would be an equally valid choice) and conclude that $a = 8 - b = 4 - \sqrt{7}$.

    We check that $a + b = 8$ and (by expanding the product or by using the ``difference of squares'' identity) $$ab = (4 - \sqrt{7}) \cdot (4 + \sqrt{7}) = 16 - 7 = 9,$$ so we're done, with $(a, b) = (4 - \sqrt{7}, 4 + \sqrt{7})$ (the other solution would be $(a, b) = (4 + \sqrt{7}, 4 - \sqrt{7})$).
  \end{solution}  

  \question[12]%9
  Express the solutions to the inequality $|13 - 3x| \le 5$ as an interval or union of intervals.

  \begin{solution}
    From the definition of absolute value, we have that $$|13 - 3x| \le 5 \iff -5 \le 13 - 3x \le 5,$$ which becomes $$-18 \le -3x \le -8 \iff 6 \ge x \ge 8 / 3.$$  Expressing this in interval notation, we have $$|13 - 3x| \le 5 \iff x \in [\frac{8}{3}, 6].$$
  \end{solution}

  \question[12]%1
  Let $f(x) = x^2 - 8x - 1$ and $g(x) = -x + 5$.  Find a formula for $f \circ g(x) = f(g(x))$.

  \begin{solution}
    To find $f \circ g$, we plug $g$ into $f$!  In other words, 
    \begin{align*}
      f \circ g (x) &= f(g(x)) \\
      &= f(-x + 5) \\
      &= (-x + 5)^2 - 8(-x + 5) - 1 \\
      &= x^2 - 10x + 25 + 8x - 40 - 1 \\
      &= x^2 - 2x - 16,
    \end{align*}
    so $f \circ g(x) = x^2 - 2x - 16.$
  \end{solution}

  \question%7
  Let $f(x) = \dfrac{x - 4}{15 - 5x}$.
  \begin{parts}
  \part[3] Find the domain of $f$.
  \part[3] Find the range of $f$ (Hint: Try part (\ref{en:inverse}) first!)
  \part[6] \label{en:inverse} Find a formula for $f^{-1}$.
  \end{parts}

    \begin{solution}
    \begin{parts}
      \part The only problem here is potentially dividing by zero, so we have that $\dom(f) = \R \setminus \{3\}$.
      \part By looking at our formula from part (\ref{en:inverse}), we see that the range is $\R \setminus \{-1 / 5\}$.
      \part We solve the equation $y = f(x)$ for $x$:
      \begin{align*}
        y &= \dfrac{x - 4}{15 - 5x} \\
        y(15 - 5x) &= x - 4 \\
        x(1 + 5y) &= 15y + 4 \\
        x &= \dfrac{15y + 4}{1 + 5y},
      \end{align*}
      so $f^{-1}(y) = \dfrac{15y + y}{1 + 5y}$.
    \end{parts}
  \end{solution}

  \question[12]%2
  Find the vertex of the parabola given by $f(x) = 4x^2 - 8x$ and state whether the vertex is a maximum or a minimum point.  

  \begin{solution}
    To find the vertex, we begin by completing the square: $$4x^2 - 8x = 4(x^2 - 2x) = 4((x - 1)^2 - 1) = 4(x - 1)^2 - 4.$$  The vertex is always where the ``squared term'' is equal to zero, so we conclude that the vertex appears at $(1, -4)$.  Because the coefficient of $x^2$ is positive, the parabola opens upward, so the vertex represents a \textbf{minimum point}.
  \end{solution}

  \question%10
  Let $L$ be the line containing the point $(4, 5)$ and having slope $6$.
  \begin{parts}
  \part[9] Find an equation for $L$ (and simplify to the form $y = mx + b$)
  \part[9] Find an equation for a line perpendicular to $L$ which has a $y$-intercept of 0.
  \end{parts}

  \begin{solution}
    \begin{parts}
      \part Using the point and the slope given, we find 
      \begin{align*}
        (y - 5) &= 6(x - 4) \\
        y - 5 &= 6x - 24 \\
        y &= 6x - 19,
      \end{align*}
      so the equation is $y = 6x - 19$.
      \part A perpendicular line will have a slope of $-\frac{1}{6} = -1/6$; using the $y$-intercept given, the equation becomes $y = -\frac{1}{6} x$.
    \end{parts}
  \end{solution}

\end{questions}
\vspace{\fill}
\emph{\large This exam has \numquestions\ questions and \numpoints\ points.}


\end{document}
